package Examen;

import Preguntas.Pregunta;
import Respuestas.Respuesta;
import org.javatuples.Pair;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Examen {

    private List<Pregunta> preguntas;

    private List<Pregunta> preguntasObligatorias;

    private Integer cantidadPreguntasObligatoriasRequeridas;

    private Integer calificacionMinima;


    public Examen(List<Pregunta> preguntas, List<Pregunta> preguntasObligatorias, Integer cantidadPreguntasObligatoriasRequeridas, Integer calificacionMinima) {
        try {
            if (preguntas.containsAll(preguntasObligatorias) || cantidadPreguntasObligatoriasRequeridas > preguntasObligatorias.size()) {
                this.preguntasObligatorias = preguntasObligatorias;
                this.cantidadPreguntasObligatoriasRequeridas = cantidadPreguntasObligatoriasRequeridas;
                this.preguntas = preguntas;
                this.calificacionMinima = calificacionMinima;

            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Una o ulgunas preguntas no estan en el examen o la cantidad a responder no es correcta");
        }
    }

    public Integer responderPreguntas(List<Pair<Pregunta,Respuesta>> respuestas) {
        var preguntasRespondidas = respuestas.stream().map(r -> r.getValue0()).collect(Collectors.toList());
        try {
            if (preguntas.containsAll(preguntasRespondidas)) {
                return respuestas.stream().mapToInt(r -> r.getValue0().resolverPregunta(r.getValue1())).sum();
            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Alguna pregunta enviada no esta en el examen");
            return 0;
        }
        finally {
            System.gc();
        }
    }

    public Integer getCalificacionMinima() {
        return calificacionMinima;
    }

    public List<Pregunta> getPreguntasObligatorias() {
        return preguntasObligatorias;
    }

    public Integer getCantidadPreguntasObligatoriasRequeridas() {
        return cantidadPreguntasObligatoriasRequeridas;
    }


}
