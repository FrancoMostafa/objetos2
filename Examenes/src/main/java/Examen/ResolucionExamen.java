package Examen;

import Preguntas.Pregunta;
import Respuestas.Respuesta;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResolucionExamen {

    private Examen examen;

    private List<Pair<Pregunta,Respuesta>> respuestas;

    public ResolucionExamen(Examen examen, List<Pair<Pregunta, Respuesta>> respuestas) {
        this.examen = examen;
        this.respuestas = respuestas;
    }

    public Examen getExamen() {
        return examen;
    }

    public List<Pair<Pregunta,Respuesta>> getRespuestas() {
        return respuestas;
    }

    public Integer puntajeObtenido() {
        return examen.responderPreguntas(respuestas);
    }

    public Boolean estaAprobado() {
        return this.puntajeObtenido() >= examen.getCalificacionMinima() &&
                this.respondioPreguntasObligatorias();
    }

    public Boolean respondioPreguntasObligatorias() {
        var preguntasObligatorias = examen.getPreguntasObligatorias();
        var preguntasRespondidas = respuestas.stream().map(r -> r.getValue0()).collect(Collectors.toList());
        var preguntasObligatoriasRespondidas = preguntasObligatorias.stream().filter(p -> preguntasRespondidas.contains(p)).collect(Collectors.toList());
        return preguntasObligatoriasRespondidas.size() >= examen.getCantidadPreguntasObligatoriasRequeridas();
    }

}
