package Aspirante;

import Busquedas.Busqueda;
import Empresa.Empresa;
import Examen.*;
import Preguntas.Pregunta;
import Respuestas.Respuesta;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Aspirante {

    private String nombreYApellido;

    private List<ResolucionExamen> resolucionesRealizadas;

    private Busqueda busquedaALaQueApunta;

    public Aspirante(String nombreYApellido) {
        this.nombreYApellido = nombreYApellido;
        this.resolucionesRealizadas = new ArrayList<>();
    }

    public void aspirarABusqueda(Busqueda busqueda) {
        try {
            if (Empresa.getEmpresa().getBusquedas().contains(busqueda) || !Empresa.getEmpresa().getAspirantes().contains(this)) {
                busqueda.agregarAspirante(this);
                this.busquedaALaQueApunta = busqueda;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("La busqueda no esta en la empresa o el aspirante no esta en la empresa");
        }
    }

    public Boolean califica() {
        return busquedaALaQueApunta.califica(this);
    }

    public Integer puntajeTotal() {
        return resolucionesRealizadas.stream().mapToInt(r -> r.puntajeObtenido()).sum();
    }

    public List<ResolucionExamen> getResolucionesRealizadas() {
        return resolucionesRealizadas;
    }

    public List<Examen> examenesRealizados() {
        return resolucionesRealizadas.stream().map(r -> r.getExamen()).collect(Collectors.toList());
    }

    public void realizarExamen(Examen examen, List<Pair<Pregunta,Respuesta>> respuestas) {
        try {
            if (this.examenesRealizados().contains(examen) || !this.lasPreguntasNoEstanRepetidas(respuestas.stream().map(Pair::getValue0).collect(Collectors.toList()))) {
                throw new Exception();
            } else {
                ResolucionExamen resolucion = new ResolucionExamen(examen, respuestas);
                resolucionesRealizadas.add(resolucion);
            }
        }
        catch (Exception e) {
            System.out.println("Este examen ya fue realizado o una pregunta enviada esta repetida");
        }
    }

    public Boolean lasPreguntasNoEstanRepetidas(List<Pregunta> preguntas) {
        return preguntas.stream().allMatch(r -> Collections.frequency(preguntas,r) == 1);
    }

    public List<ResolucionExamen> resolucionesAprobadas() {
        return resolucionesRealizadas.stream().filter(r -> r.estaAprobado()).collect(Collectors.toList());
    }

    public List<Examen> examenesAprobados() {
        return this.resolucionesAprobadas().stream().map(r -> r.getExamen()).collect(Collectors.toList());
    }

    public Integer cantidadDeExamenesAprobados() {
        return this.examenesAprobados().size();
    }

    public List<Examen> examenesQueLeFaltan() {
        return busquedaALaQueApunta.getExamenesRequeridos().stream().filter(e -> !examenesRealizados().contains(e))
                .collect(Collectors.toList());
    }

}
