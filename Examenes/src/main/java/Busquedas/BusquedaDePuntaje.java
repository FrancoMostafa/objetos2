package Busquedas;

import Aspirante.Aspirante;
import Examen.Examen;

import java.util.List;

public class BusquedaDePuntaje extends Busqueda {

    private Integer puntajeRequerido;

    public BusquedaDePuntaje(List<Examen> examenesRequeridos, Integer puntajeRequerido) {
        super(examenesRequeridos);
        this.puntajeRequerido = puntajeRequerido;
    }

    protected Boolean condicion(Aspirante aspirante) {
        return aspirante.puntajeTotal() >= puntajeRequerido;
    }

}
