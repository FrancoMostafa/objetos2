package Busquedas;

import Aspirante.Aspirante;
import Examen.*;
import Preguntas.Pregunta;
import Respuestas.Respuesta;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Busqueda {

    protected List<Aspirante> aspirantes;

    protected List<Examen> examenesRequeridos;

    public Busqueda(List<Examen> examenesRequeridos) {
        this.examenesRequeridos = examenesRequeridos;
        this.aspirantes = new ArrayList<>();
    }

    public Boolean califica(Aspirante aspirante) {
        return entregoLosExamenesRequeridos(aspirante)
                &&
                condicion(aspirante);
    }

    public Boolean entregoLosExamenesRequeridos(Aspirante aspirante) {
        return aspirante.examenesRealizados().containsAll(examenesRequeridos);
    }

    protected abstract Boolean condicion(Aspirante aspirante);

    public List<Examen> getExamenesRequeridos() {
        return examenesRequeridos;
    }

    public void agregarAspirante(Aspirante aspirante) {
        aspirantes.add(aspirante);
    }

    public List<Aspirante> aspirantesQueNoSePuedenEvaluar() {
       return aspirantes.stream().filter(a -> examenesRequeridos.containsAll(a.examenesRealizados())).collect(Collectors.toList());
    }

    public List<Aspirante> rankingAspirantesCalificados() {
        List <Aspirante> listAspirante = aspirantes.stream().filter(a -> a.califica()).collect(Collectors.toList());
        return listAspirante.stream()
                .sorted(Comparator.comparing(Aspirante::puntajeTotal).reversed())
                .collect(Collectors.toList());
    }

    public List<Pair<Pregunta,Respuesta>> respuestasAUnExamenDeAspirantes(Examen examen) {
        try {
            if(this.getExamenesRequeridos().contains(examen)) {
                List<Pair<Pregunta,Respuesta>> newList = new ArrayList<>();
                var resoluciones = resolucionesDeUnExamenDeAspirantes(examen).stream().map(r -> r.getRespuestas()).collect(Collectors.toList());
                resoluciones.forEach(newList::addAll);
                return newList;
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("El examen no esta en la busqueda");
            return new ArrayList<>();
        }
    }

    public List<ResolucionExamen> resolucionesDeUnExamenDeAspirantes(Examen examen) {
        try {
            if (this.getExamenesRequeridos().contains(examen)) {
                List<ResolucionExamen> newList = new ArrayList<>();
                aspirantes.forEach(a -> newList.addAll(a.getResolucionesRealizadas()));
                return newList.stream().filter(r -> r.getExamen() == examen).collect(Collectors.toList());
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("El examen no esta en la busqueda");
            return new ArrayList<>();
        }
    }


}
