package Preguntas;

import Respuestas.*;

import java.util.List;

public class PreguntaMultipleChoice extends Pregunta {

    private List<Respuesta> respuestas;

    private Respuesta respuestaCorrecta;

    public PreguntaMultipleChoice(String descripcion, Integer puntaje, List<Respuesta> respuestas, Respuesta respuestaCorrecta) {
        super(descripcion,puntaje);
        this.respuestas = respuestas;
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public Integer resolverPregunta(Respuesta respuesta) {
        if (respuesta.getValor() == respuestaCorrecta.getValor()) {
            return puntaje;
        } else {
            return 0;
        }
    }

}
