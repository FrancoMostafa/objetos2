package Preguntas;

import Respuestas.Respuesta;
import org.javatuples.Pair;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PreguntaNumerica extends Pregunta {

    private Pair<Integer,Integer> rangoDeAproximacion;

    private Integer puntajePorAproximacion;

    private Respuesta respuesta;

    public PreguntaNumerica(String descripcion, Integer puntaje, Pair<Integer,Integer> rangoDeAproximacion, Integer puntajePorAproximacion, Respuesta respuesta) {
        super(descripcion,puntaje);
        this.rangoDeAproximacion = rangoDeAproximacion;
        this.puntajePorAproximacion = puntajePorAproximacion;
        this.respuesta = respuesta;
    }

    public Integer resolverPregunta(Respuesta respuesta) {
        List<Integer> values = IntStream.range(rangoDeAproximacion.getValue0(), rangoDeAproximacion.getValue1()).boxed().collect(Collectors.toList());
        Integer laRespuesta = Integer.parseInt(respuesta.getValor());
        if (values.contains(laRespuesta)) {
            return puntajePorAproximacion;
        }
        else if (respuesta.getValor().equals(this.respuesta.getValor())){
            return puntaje;
        }
        else {
            return 0;
        }
    }

}
