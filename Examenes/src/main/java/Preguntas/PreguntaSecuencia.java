package Preguntas;

import Respuestas.Respuesta;

import java.util.List;

public class PreguntaSecuencia extends Pregunta {

    private Respuesta secuenciaCorrecta;

    public PreguntaSecuencia(String descripcion, Integer puntaje, Respuesta secuenciaCorrecta) {
        super(descripcion,puntaje);
        List<Integer> secuenciaInt = stringToIntegerList(secuenciaCorrecta.getValor());
        if (secuenciaInt.size() != 5) {
            this.secuenciaCorrecta = null;
            System.out.println("La secuencia debe ser de 5 digitos");
        }
        this.secuenciaCorrecta = secuenciaCorrecta;
    }

    public Integer resolverPregunta(Respuesta respuesta) {
        if (this.esLaSecuenciaComun(respuesta) || this.esLaSecuenciaInversa(respuesta)) {
            return puntaje;
        }
        else {
            return 0;
        }
    }

    public Boolean esLaSecuenciaComun(Respuesta respuesta) {
        List<Integer> respuestaListInt = stringToIntegerList(respuesta.getValor());
        List<Integer> respuestaCorrectaListInt = stringToIntegerList(secuenciaCorrecta.getValor());
        return respuestaListInt.get(0).equals(respuestaCorrectaListInt.get(0)) &&
                respuestaListInt.get(1).equals(respuestaCorrectaListInt.get(1)) &&
                respuestaListInt.get(2).equals(respuestaCorrectaListInt.get(2)) &&
                respuestaListInt.get(3).equals(respuestaCorrectaListInt.get(3)) &&
                respuestaListInt.get(4).equals(respuestaCorrectaListInt.get(4));
    }

    public Boolean esLaSecuenciaInversa(Respuesta respuesta) {
        List<Integer> respuestaListInt = stringToIntegerList(respuesta.getValor());
        List<Integer> respuestaCorrectaListInt = stringToIntegerList(secuenciaCorrecta.getValor());
        return respuestaListInt.get(0).equals(respuestaCorrectaListInt.get(4)) &&
                respuestaListInt.get(1).equals(respuestaCorrectaListInt.get(3)) &&
                respuestaListInt.get(2).equals(respuestaCorrectaListInt.get(2)) &&
                respuestaListInt.get(3).equals(respuestaCorrectaListInt.get(1)) &&
                respuestaListInt.get(4).equals(respuestaCorrectaListInt.get(0));
    }

    public List<Integer> stringToIntegerList(String str) {
        return stringToIntegerList(str);
    }

}
