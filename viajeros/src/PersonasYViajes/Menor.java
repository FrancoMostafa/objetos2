package PersonasYViajes;

import java.util.List;

public class Menor extends Persona {

    private Persona madre;

    public Menor (String unNombre, String unApellido, Persona madre) {
        super(unNombre, unApellido);
        this.madre = madre;
    }

    @Override
    public List<String> residioEnLosPaisesElAnio(Integer unAnio) {
        return madre.residioEnLosPaisesElAnio(unAnio);
    }

    @Override
    public String toString() {
        return "Menor{" +
                "madre=" + madre +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", viajesRealizados=" + viajesRealizados +
                '}';
    }
}
