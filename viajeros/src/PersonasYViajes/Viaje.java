package PersonasYViajes;

public class Viaje {

    public String pais;
    public Integer anio;

    public Viaje(String unPais, Integer unAnio) {
        this.pais = unPais;
        this.anio = unAnio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public boolean esDelAnio(Integer unAnio) {return this.getAnio().equals(unAnio);}

    @Override
    public String toString() {
        return "Viaje{" +
                "pais='" + pais + '\'' +
                ", anio=" + anio +
                '}';
    }
}
