package PersonasYViajes;

import java.util.ArrayList;
import java.util.List;

public class Establecido extends Persona {

    private String paisDeResidencia;
    private Integer anioActual;

    public Establecido(String elNombre,String elApellido, String pais) {
        super(elNombre,elApellido);
        this.paisDeResidencia = pais;
    }

    @Override
    public List<String> residioEnLosPaisesElAnio(Integer unAnio) {
        List<String> paises = new ArrayList<>();
        paises.add(paisDeResidencia);
        List<String> paisesDeViajes = (List<String>) viajesRealizados.stream().filter(viaje -> viaje.esDelAnio(unAnio))
                .map(viaje -> viaje.getPais());
        paisesDeViajes.forEach(pais -> paises.add(pais));
        return paises;
    }

    @Override
    public String toString() {
        return "Establecido{" +
                "paisDeResidencia='" + paisDeResidencia + '\'' +
                ", anioActual=" + anioActual +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", viajesRealizados=" + viajesRealizados +
                '}';
    }
}
