package PersonasYViajes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Persona {

        public String nombre;
        public String apellido;
        public ArrayList<Viaje> viajesRealizados;

    public Persona(String elNombre,String elApellido) {
        nombre = elNombre;
        apellido = elApellido;
        viajesRealizados = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<Viaje> getViajesRealizados() {
        return viajesRealizados;
    }

    public List<String> residioEnLosPaisesElAnio(Integer unAnio) { return Collections.emptyList(); }

    public List<String> enQuePaisesEstuvoElAnio(Integer elAnio) {
        return viajesRealizados.stream().filter(viaje -> viaje.getAnio().equals(elAnio))
        .map(viaje -> viaje.getPais()).collect(Collectors.toList());
    }

    public Boolean coincidioCon(Persona otraPersona, Integer elAnio) {
        List<String> paises1 = null;
        paises1 = this.enQuePaisesEstuvoElAnio(elAnio);
        List<String> paises2 = null;
        paises2 = otraPersona.enQuePaisesEstuvoElAnio(elAnio);
        List<String> finalPaises = paises2;
        return !paises1.stream().filter(pais -> finalPaises.contains(pais)).collect(Collectors.toList()).isEmpty();
    }

    public void agregarViaje(Viaje unViaje) {
        viajesRealizados.add(unViaje);
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", viajesRealizados=" + viajesRealizados +
                '}';
    }
}
