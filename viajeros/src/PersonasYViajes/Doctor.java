package PersonasYViajes;

import java.util.ArrayList;
import java.util.List;

public class Doctor extends Persona {

    private String paisEnElQueVive;
    private String paisDeDoctorado;
    private Integer inicioDoctorado;
    private Integer finDoctorado;

    public Doctor(String elNombre, String elApellido, String paisEnElQueVive,
                  String paisDeDoctorado, Integer inicioDoctorado, Integer finDoctorado) {
        super(elNombre, elApellido);
        this.paisEnElQueVive = paisEnElQueVive;
        this.paisDeDoctorado = paisDeDoctorado;
        this.inicioDoctorado = inicioDoctorado;
        this.finDoctorado = finDoctorado;
        Viaje viajeDeDoctorado = new Viaje(this.getPaisDeDoctorado(),this.getAnioDeDoctorado());
        viajesRealizados.add(viajeDeDoctorado);
    }

    public String getPaisDeDoctorado() {return this.paisDeDoctorado;}

    public Integer getAnioDeDoctorado() {return this.inicioDoctorado;}

    @Override
    public List<String> residioEnLosPaisesElAnio(Integer unAnio) {
        List<String> paises = new ArrayList<>();
        if (unAnio >= inicioDoctorado && unAnio <= finDoctorado && !this.getInicioDoctorado().equals(unAnio)) {
            paises.add(paisDeDoctorado);
        }
        else if (this.getInicioDoctorado().equals(unAnio)) {
            paises.add(paisEnElQueVive);
            paises.add(paisDeDoctorado);
        }
        else {
            paises.add(paisEnElQueVive);
        }
        List<String> paisesDeViajes = (List<String>) viajesRealizados.stream().filter(viaje -> viaje.esDelAnio(unAnio))
                .map(viaje -> viaje.getPais());
        paisesDeViajes.forEach(pais -> paises.add(pais));
    return paises;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "paisEnElQueVive='" + paisEnElQueVive + '\'' +
                ", paisDeDoctorado='" + paisDeDoctorado + '\'' +
                ", inicioDoctorado=" + inicioDoctorado +
                ", finDoctorado=" + finDoctorado +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", viajesRealizados=" + viajesRealizados +
                '}';
    }

    public Integer getInicioDoctorado() {
        return inicioDoctorado;
    }
}