package PersonasYViajes;

import java.util.ArrayList;
import java.util.List;

public class Migrante extends Persona {

    private String paisDeNacimiento;
    private String paisDeMudanza;
    private Integer anioDeMudanza;

    public Migrante (String elNombre,String elApellido, String paisDeNacimiento,
                     String paisDeMudanza, Integer anioDeMudanza) {
        super(elNombre, elApellido);
        this.paisDeNacimiento = paisDeNacimiento;
        this.paisDeMudanza = paisDeMudanza;
        this.anioDeMudanza = anioDeMudanza;
        Viaje viajeDeMudanza = new Viaje(this.getPaisDeMudanza(),this.getAnioDeMudanza());
        viajesRealizados.add(viajeDeMudanza);
    }

    public String getPaisDeMudanza() {return this.paisDeMudanza;}

    public Integer getAnioDeMudanza() {return this.anioDeMudanza;}

    @Override
    public List<String> residioEnLosPaisesElAnio(Integer unAnio) {
        List <String> paises = new ArrayList<>();
        if (unAnio < anioDeMudanza) {
            paises.add(paisDeNacimiento);
        }
        else if (unAnio > anioDeMudanza) {
            paises.add(paisDeMudanza);
        }
        else {
            paises.add(paisDeNacimiento);
            paises.add(paisDeMudanza);
        }
        List<String> paisesDeViajes = (List<String>) viajesRealizados.stream().filter(viaje -> viaje.esDelAnio(unAnio))
                .map(viaje -> viaje.getPais());
        paisesDeViajes.forEach(pais -> paises.add(pais));
        return paises;
    }

    @Override
    public String toString() {
        return "Migrante{" +
                "paisDeNacimiento='" + paisDeNacimiento + '\'' +
                ", paisDeMudanza='" + paisDeMudanza + '\'' +
                ", anioDeMudanza=" + anioDeMudanza +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", viajesRealizados=" + viajesRealizados +
                '}';
    }
}