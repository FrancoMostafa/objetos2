import PersonasYViajes.Doctor;
import PersonasYViajes.Establecido;
import PersonasYViajes.Migrante;
import PersonasYViajes.Viaje;

public class Main {

    public static void main(String[] args) {
        Viaje viaje1 = new Viaje("España", 2010);
        Viaje viaje2 = new Viaje("Francia", 2014);
        Migrante pepe = new Migrante("Pepe","Roque","Francia",
                "Italia",2005);
        Establecido marcos = new Establecido("Marcos","Perez","Argentina");
        Doctor jorge = new Doctor("Jorge","Perez","Canada"
        ,"Estados Unidos",2000,2003);
        pepe.agregarViaje(viaje1);
        marcos.agregarViaje(viaje2);
        jorge.agregarViaje(viaje2);
        System.out.println((jorge.residioEnLosPaisesElAnio(2000)));
    }

}
