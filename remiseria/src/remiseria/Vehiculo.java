package remiseria;

public interface Vehiculo {

    public Integer getCapacidad();

    public Integer getMaximaVelocidad();

    public String getColor();

    public Integer getPeso();

}
