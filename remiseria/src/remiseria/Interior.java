package remiseria;

public interface Interior {

    public Integer getCapacidad();

    public Integer getPeso();

}
