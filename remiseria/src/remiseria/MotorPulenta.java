package remiseria;

public class MotorPulenta implements Motor {

    public Integer getVelocidadMaxima() {
        return 130;
    }

    public Integer getPeso() {
        return 800;
    }

}
