package remiseria;

import java.util.List;

public class Viaje {

    private Integer kilometros;
    private Integer tiempoMaximo;
    private Integer cantidadDePasajeros;
    private List<String> coloresIncompatibles;
    private Vehiculo vehiculo;

    public Viaje(Integer kilometros, Integer tiempoMaximo, Integer cantidadDePasajeros, List<String> coloresIncompatibles) {
        this.kilometros = kilometros;
        this.tiempoMaximo = tiempoMaximo;
        this.cantidadDePasajeros = cantidadDePasajeros;
        this.coloresIncompatibles = coloresIncompatibles;
        this.vehiculo = null;
    }



    public Boolean puedeRealizarloElVehiculo(Vehiculo vehiculo) {
        return vehiculo.getMaximaVelocidad() > (this.velocidadRequerida() + 10)
                &&
                vehiculo.getCapacidad() >= this.cantidadDePasajeros
                &&
                !coloresIncompatibles.contains(vehiculo.getColor());
    }

    public Integer velocidadRequerida() {
        return this.kilometros / this.tiempoMaximo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Integer getKilometros() {
        return kilometros;
    }

    public Integer lugaresSobrantes() {
        return this.getVehiculo().getCapacidad() - cantidadDePasajeros;
    }

    public Integer pagaDelVehiculo(Remiseria remiseria) {
        if (kilometros * remiseria.getSueldoDeVehiculosPorKm() < remiseria.getSueldoDeVehiculosMinimo()) {
            return remiseria.getSueldoDeVehiculosMinimo();
        }
        else {
            return kilometros * remiseria.getSueldoDeVehiculosPorKm();
        }
    }

}
