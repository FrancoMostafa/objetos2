package remiseria;

public class ChevroletCorsa implements Vehiculo {

    private String color;

    public ChevroletCorsa(String color) {
        this.color = color;
    }

    public Integer getCapacidad() {
        return 4;
    }

    public Integer getMaximaVelocidad() {
        return 150;
    }

    public String getColor() {
        return color;
    }

    public Integer getPeso() {
        return 1300;
    }

}
