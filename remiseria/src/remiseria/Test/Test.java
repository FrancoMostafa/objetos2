package remiseria.Test;

import junit.framework.TestCase;
import remiseria.*;

import java.util.ArrayList;
import java.util.List;

public class Test extends TestCase {

    public void testAgregarAFlota() {
        Remiseria laRemiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        laRemiseria.agregarALaFlota(unAuto);
        Integer vehiculos = laRemiseria.getFlotaDeVehiculos().size();
        assertEquals(Integer.valueOf(1),vehiculos);
    }

    public void testQuitarDeLaFlota() {
        Remiseria laRemiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        laRemiseria.agregarALaFlota(unAuto);
        laRemiseria.quitarDeFlota(unAuto);
        Integer vehiculos = laRemiseria.getFlotaDeVehiculos().size();
        assertEquals(Integer.valueOf(0),vehiculos);
    }

    public void testCapacidadTotalYendoA() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        Auto otroAuto = new Auto(2,80,"Blanco",60);
        remiseria.agregarALaFlota(unAuto);
        remiseria.agregarALaFlota(otroAuto);
        assertEquals(Integer.valueOf(4),remiseria.capacidadTotalYendoA(100));
    }

    public void testColorDelAutoMasRapido() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        Auto otroAuto = new Auto(2,80,"Rojo",60);
        remiseria.agregarALaFlota(unAuto);
        remiseria.agregarALaFlota(otroAuto);
        assertEquals(String.valueOf("Blanco"),remiseria.colorDelAutoMasRapido());
    }

    public void testDosRemiseriasPuntoB() {
        //
        ChevroletCorsa cachito = new ChevroletCorsa("Rojo");
        ChevroletCorsa corsaNegro = new ChevroletCorsa("Negro");
        ChevroletCorsa corsaVerde = new ChevroletCorsa("Negro");
        Standard standard1 = new Standard(true);
        Auto auto1 = new Auto(5,160,"Beige",1200);
        Remiseria remiseria1 = new Remiseria(30,50);
        remiseria1.agregarALaFlota(cachito);
        remiseria1.agregarALaFlota(corsaNegro);
        remiseria1.agregarALaFlota(corsaVerde);
        remiseria1.agregarALaFlota(standard1);
        remiseria1.agregarALaFlota(auto1);
        //
        Standard standard2 = new Standard(true);
        Standard standard3 = new Standard(false);
        Standard standard4 = new Standard(false);
        MotorBataton motorBataton = new MotorBataton();
        InteriorPopular interiorPopular = new InteriorPopular();
        Trafic trafic = Trafic.instance(motorBataton,interiorPopular);
        Remiseria remiseria2 = new Remiseria(40,60);
        remiseria2.agregarALaFlota(cachito);
        remiseria2.agregarALaFlota(standard2);
        remiseria2.agregarALaFlota(standard3);
        remiseria2.agregarALaFlota(standard4);
        remiseria2.agregarALaFlota(trafic);
        assertEquals(Integer.valueOf(6450),remiseria1.pesoTotalFlota());
        assertEquals(Integer.valueOf(10050),remiseria2.pesoTotalFlota());
        assertEquals(Boolean.valueOf(true),remiseria1.esRecomendable());
        assertEquals(Boolean.valueOf(true),remiseria2.esRecomendable());
        assertEquals(Integer.valueOf(17),remiseria1.capacidadTotalYendoA(140));
        assertEquals(Integer.valueOf(4),remiseria2.capacidadTotalYendoA(140));
        assertEquals(String.valueOf("Beige"),remiseria1.colorDelAutoMasRapido());
        assertEquals(String.valueOf("Rojo"),remiseria2.colorDelAutoMasRapido());
    }

    public void testCuantosViajesRealizoUnVehiculo() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        remiseria.agregarALaFlota(unAuto);
        var coloresIncompatibles1 = new ArrayList<String>();
        coloresIncompatibles1.add("Blanco");
        var coloresIncompatibles2 = new ArrayList<String>();
        coloresIncompatibles2.add("Rojo");
        Viaje viaje1 = new Viaje(50,10,4,coloresIncompatibles1);
        Viaje viaje2 = new Viaje(50,10,3,coloresIncompatibles2);
        remiseria.registrarViaje(viaje1,unAuto);
        remiseria.registrarViaje(viaje2,unAuto);
        assertEquals(Integer.valueOf(1),remiseria.cuantosViajesRealizoElVehiculo(unAuto));
    }

    public void testCuantosViajesAKm() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        remiseria.agregarALaFlota(unAuto);
        var coloresIncompatibles1 = new ArrayList<String>();
        coloresIncompatibles1.add("Blanco");
        var coloresIncompatibles2 = new ArrayList<String>();
        coloresIncompatibles2.add("Rojo");
        Viaje viaje1 = new Viaje(30,10,4,coloresIncompatibles1);
        Viaje viaje2 = new Viaje(30,10,3,coloresIncompatibles2);
        remiseria.registrarViaje(viaje1,unAuto);
        remiseria.registrarViaje(viaje2,unAuto);
        assertEquals(Integer.valueOf(0),remiseria.viajesRealizadosDeMasDeKilometros(40));
    }

    public void testLugaresSobrantes() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        remiseria.agregarALaFlota(unAuto);
        var coloresIncompatibles1 = new ArrayList<String>();
        coloresIncompatibles1.add("Rojo");
        Viaje viaje1 = new Viaje(30,10,4,coloresIncompatibles1);
        Viaje viaje2 = new Viaje(30,10,3,coloresIncompatibles1);
        remiseria.registrarViaje(viaje1,unAuto);
        remiseria.registrarViaje(viaje2,unAuto);
        assertEquals(Integer.valueOf(1),remiseria.lugaresSobrantesEnViajes());
    }

    public void testPagaDeUnVehiculo() {
        Remiseria remiseria = new Remiseria(30,50);
        Auto unAuto = new Auto(4,100,"Blanco",80);
        remiseria.agregarALaFlota(unAuto);
        var coloresIncompatibles1 = new ArrayList<String>();
        coloresIncompatibles1.add("Blanco");
        var coloresIncompatibles2 = new ArrayList<String>();
        coloresIncompatibles2.add("Rojo");
        Viaje viaje1 = new Viaje(50,10,4,coloresIncompatibles1);
        Viaje viaje2 = new Viaje(50,10,3,coloresIncompatibles2);
        remiseria.registrarViaje(viaje1,unAuto);
        remiseria.registrarViaje(viaje2,unAuto);
        assertEquals(Integer.valueOf(2500),remiseria.pagoAUnVehiculo(unAuto));
    }

}
