package remiseria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Remiseria {

    private List<Vehiculo> flotaDeVehiculos;
    private List<Viaje> viajesRealizados;
    private Integer sueldoDeVehiculosMinimo;
    private Integer sueldoDeVehiculosPorKm;

    public Remiseria(Integer sueldoDeVehiculosMinimo, Integer sueldoDeVehiculosPorKm) {
        this.flotaDeVehiculos = new ArrayList<Vehiculo>();
        this.viajesRealizados = new ArrayList<Viaje>();
        this.sueldoDeVehiculosMinimo = sueldoDeVehiculosMinimo;
        this.sueldoDeVehiculosPorKm = sueldoDeVehiculosPorKm;
    }

    public List<Vehiculo> getFlotaDeVehiculos() {
        return flotaDeVehiculos;
    }

    public Integer getSueldoDeVehiculosMinimo() {
        return sueldoDeVehiculosMinimo;
    }

    public void setSueldoDeVehiculosMinimo(Integer sueldoDeVehiculosMinimo) {
        this.sueldoDeVehiculosMinimo = sueldoDeVehiculosMinimo;
    }

    public Integer getSueldoDeVehiculosPorKm() {
        return sueldoDeVehiculosPorKm;
    }

    public void setSueldoDeVehiculosPorKm(Integer sueldoDeVehiculosPorKm) {
        this.sueldoDeVehiculosPorKm = sueldoDeVehiculosPorKm;
    }

    public void agregarALaFlota(Vehiculo vehiculo) {
        flotaDeVehiculos.add(vehiculo);
    }

    public void quitarDeFlota(Vehiculo vehiculo) {
        flotaDeVehiculos.remove(vehiculo);
    }

    public Integer pesoTotalFlota() {
        return flotaDeVehiculos.stream().mapToInt(v -> v.getPeso()).sum();
    }

    public Boolean esRecomendable() {
        return flotaDeVehiculos.size() > 3
                &&
                flotaDeVehiculos.stream().anyMatch(v -> v.getMaximaVelocidad() >= 100);
    }

    public Integer capacidadTotalYendoA(Integer velocidad) {
        var vehiculos = flotaDeVehiculos.stream().filter(v -> v.getMaximaVelocidad() >= velocidad);
        return vehiculos.mapToInt(v -> v.getCapacidad()).sum();
    }

    // CONDICION: No hay un auto con la misma velocidad maxima.
    public String colorDelAutoMasRapido() {
        return flotaDeVehiculos.stream().max(Comparator.comparing(Vehiculo::getMaximaVelocidad)).get().getColor();
    }

    public List<Vehiculo> puedenRealizarElViaje(Viaje viaje) {
        return flotaDeVehiculos.stream()
                .filter(v -> viaje.puedeRealizarloElVehiculo(v)).collect(Collectors.toList());
    }

    public void registrarViaje(Viaje viaje, Vehiculo vehiculo) {
        if (viaje.puedeRealizarloElVehiculo(vehiculo)) {
            viajesRealizados.add(viaje);
            viaje.setVehiculo(vehiculo);
        }
    }

    public Integer cuantosViajesRealizoElVehiculo(Vehiculo vehiculo) {
        if (this.tieneElVehiculo(vehiculo)) {
            return viajesRealizados.stream().filter(v -> v.getVehiculo() == vehiculo)
                    .collect(Collectors.toList()).size();
        }
        else {
            System.out.println("La flota no contiene el vehiculo");
            return 0;
        }
    }

    public Boolean tieneElVehiculo(Vehiculo vehiculo) {
        return flotaDeVehiculos.contains(vehiculo);
    }

    public Integer viajesRealizadosDeMasDeKilometros(Integer kilometros) {
        return viajesRealizados.stream().filter
                (v -> v.getKilometros() > kilometros).collect(Collectors.toList()).size();
    }

    public Integer lugaresSobrantesEnViajes() {
        return viajesRealizados.stream().mapToInt(v -> v.lugaresSobrantes()).sum();
    }

    public Integer pagoAUnVehiculo(Vehiculo vehiculo) {
        if (this.tieneElVehiculo(vehiculo)) {
            var viajesDelVehiculo = viajesRealizados.stream().filter
                    (v -> v.getVehiculo() == vehiculo);
            return viajesDelVehiculo.mapToInt(v -> v.pagaDelVehiculo(this)).sum();
        }
        else {
            System.out.println("La flota no contiene el vehiculo");
            return 0;
        }
    }

}
