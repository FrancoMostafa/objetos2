package remiseria;

public interface Motor {

    public Integer getVelocidadMaxima();

    public Integer getPeso();

}
