package remiseria;

public class Auto implements Vehiculo{

    private Integer capacidad;
    private Integer maximaVelocidad;
    private String color;
    private Integer peso;

    public Auto(Integer capacidad, Integer maximaVelocidad, String color, Integer peso) {
        this.capacidad = capacidad;
        this.maximaVelocidad = maximaVelocidad;
        this.color = color;
        this.peso = peso;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public Integer getMaximaVelocidad() {
        return maximaVelocidad;
    }

    public String getColor() {
        return color;
    }

    public Integer getPeso() {
        return peso;
    }
}
