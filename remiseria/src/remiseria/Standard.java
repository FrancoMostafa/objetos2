package remiseria;

public class Standard implements Vehiculo {

    private Boolean tanqueAdicional;

    public Standard(Boolean tanqueAdicional) {
        this.tanqueAdicional = tanqueAdicional;
    }

    public Integer getCapacidad() {
        if (tanqueAdicional) {
            return 3;
        }
        else {
            return 4;
        }
    }

    public Integer getMaximaVelocidad() {
        if (tanqueAdicional) {
            return 120;
        }
        else {
            return 110;
        }
    }

    public String getColor() {
        return "Azul";
    }

    public Integer getPeso() {
        if (tanqueAdicional) {
            return 1350;
        }
        else {
            return 1200;
        }
    }

}
