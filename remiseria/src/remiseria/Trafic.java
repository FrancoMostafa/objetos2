package remiseria;

public class Trafic implements Vehiculo {

    private static Trafic trafic;
    private Motor motor;
    private Interior interior;

    private Trafic(Motor motor, Interior interior) {
        this.motor = motor;
        this.interior = interior;
    }

    public static Trafic instance(Motor motor, Interior interior) {
        if (trafic == null) {
            trafic = new Trafic(motor,interior);
        }
        return trafic;
    }

    public Integer getCapacidad() {
        return interior.getCapacidad();
    }

    public Integer getMaximaVelocidad() {
        return motor.getVelocidadMaxima();
    }

    public String getColor() {
        return "Blanco";
    }

    public Integer getPeso() {
        return 4000 + interior.getPeso();
    }

    public void setInterior(Interior interior) {
        this.interior = interior;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

}

