class CerdoResistente extends Cerdo {
    
    constructor(peso) {
        super(peso)
        this.necesitaVacuna = true
    }

    convieneVacunar() {
        return this.necesitaVacuna
    }

    vacunar() {
        super.vacunar()
        this.necesitaVacuna = false
    }

    comer(gramos) {
        super.comer(gramos)
        if (gramos >= 5000) {
            this.necesitaVacuna = true
        }
    }

}

class CerdoAlegre extends Cerdo {

    esFeliz() {
        return true
    }

}

class VacaZen extends Vaca {

    tieneSed() {
        return !this.seVacuno
    }

}

class VacaReflexiva extends Vaca {

    constructor(peso) {
        super(peso)
        this.vecesQueCamino = 0
    }

    llevarACaminar() {
        super.llevarACaminar()
        this.vecesQueCamino += 1
    }

    esFeliz() {
        return super.esFeliz() && this.vecesQueCamino >= 2
    }

}

class VacaFilosofica extends VacaReflexiva {

    esFeliz() {
        return super.esFeliz() && !this.tieneSed()
    }

}

class GallinaTuruleca extends Gallina {

    constructor() {
        super(4)
        this.huevosPuestosEnTotal = 0
    }

    haPuestoUnHuevo() {
        this.huevosPuestosEnTotal += 1
    }

    haPuestoDosHuevos() {
        this.huevosPuestosEnTotal += 2
    }

    haPuestoTresHuevos() {
        this.huevosPuestosEnTotal += 3
    }

    tieneSed() {
        return this.huevosPuestosEnTotal % 2 != 0
    }

}