class Dispositivo {

    constructor() {}

    atenderAnimalSiPuede(unAnimal) {
        if (this.puedeAtenderAlAnimal(unAnimal)) {
            this.atenderAnimal(unAnimal)
        }
        else {
            console.log("No se puede atender a este animal")
        }
    }

    puedeAtenderAlAnimal(_unAnimal) {console.log("No hay definicion de puedeAtenderAlAnimal(unAnimal) en Dispositivo")}

    atenderAnimal(_unAnimal) {console.log("No hay definicion de atenderAnimal(unAnimal) en Dispositivo")}

    necesitaRecarga() {console.log("No hay definicion de necesitaRecarga() en Dispositivo")}

    recargar() {console.log("No hay definicion de recargar() en Dispositivo")}

}

class ComedorNormal extends Dispositivo {

    constructor(cantidadDeRaciones, gramosDeRaciones, soporteDePeso) {
        super()
        this.cantidadDeRaciones = cantidadDeRaciones
        this.gramosDeRaciones = gramosDeRaciones
        this.soporteDePeso = soporteDePeso
    }

    atenderAnimal(unAnimal) {
        if (this.cantidadDeRaciones != 0) {
            unAnimal.comer(200)
            this.cantidadDeRaciones -= 1
        }
        else {
            console.log("No hay mas raciones para alimentar al animal")
        }
    }

    puedeAtenderAlAnimal(unAnimal) {
        return unAnimal.tieneHambre() && unAnimal.getPeso() < this.soporteDePeso
    }

    necesitaRecarga() {
        return this.cantidadDeRaciones < 10
    }

    recargar() {
        this.cantidadDeRaciones += 30
    }

}

class ComedorInteligente extends Dispositivo {

    constructor(cantidadDeComida, capacidadMaxima) {
        super()
        this.cantidadDeComida = cantidadDeComida
        this.capacidadMaxima = capacidadMaxima
    }

    puedeAtenderAlAnimal(unAnimal) {
        return unAnimal.tieneHambre()
    }

    atenderAnimal(unAnimal) {
        if (this.cantidadDeComida >= unAnimal.getPeso() / 100) {
            unAnimal.comer(unAnimal.getPeso() / 100)
            this.cantidadDeComida -= unAnimal.getPeso() / 100
        }
        else {
            console.log("No hay suficiente comida para alimentar al animal")
        }
    }

    necesitaRecarga() {
        return this.cantidadDeComida < 15000
    }

    recargar() {
        this.cantidadDeComida = this.capacidadMaxima
    }

}

class Bebedero extends Dispositivo {

    constructor() {
        super()
        this.cantidadDeAnimalesAtendidosRestantesParaRecarga = 20
    }

    puedeAtenderAlAnimal(unAnimal) {
        return unAnimal.tieneSed()
    }

    atenderAnimal(unAnimal) {
        unAnimal.beber()
        this.cantidadDeAnimalesAtendidosRestantesParaRecarga -= 1
    }

    necesitaRecarga() {
        return this.cantidadDeAnimalesAtendidosRestantesParaRecarga <= 0
    }

    recargar() {
        this.cantidadDeAnimalesAtendidosRestantesParaRecarga = 20
    }

}

class Vacunatorio extends Dispositivo {

    constructor(cantidadDeVacunas) {
        super()
        this.cantidadDeVacunas = cantidadDeVacunas
    }

    puedeAtenderAlAnimal(unAnimal) {
        return unAnimal.convieneVacunar()
    }

    atenderAnimal(unAnimal) {
        if (this.cantidadDeVacunas > 0) {
            unAnimal.vacunar()
            this.cantidadDeVacunas -= 1
        }
        else {
            console.log("No hay mas vacunas para vacunar al animal")
        }     
    }

}