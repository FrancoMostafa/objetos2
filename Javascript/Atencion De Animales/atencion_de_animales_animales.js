 class Animal {
    
    constructor(peso) {
        this.peso = peso
        this.seVacuno = false
    }

    getPeso() {
        return this.peso
    }

    vacunar() {
        this.seVacuno = true
    }

    convieneVacunar() {
        return !this.seVacuno
    }

    atenderseEnLaEstacion(unaEstacion) {
        if (unaEstacion.puedeAtenderAlAnimal(this)) {
            let dispositivos = unaEstacion.getDispositivos().filter(d => d.puedeAtenderAlAnimal(this))
            let dispositivo = dispositivos[Math.floor(Math.random() * dispositivos.length)]
            dispositivo.atenderAnimal(this)
            unaEstacion.getAnimalesAtendidos().add(this)
        }
        else {
            console.log("No hay un dispositivo en la estacion que pueda atender al animal")
        }
    }

    esFeliz() {
        return !this.convieneVacunar() && this.getPeso() < 10000 || this.getPeso() > 150000
    }

}

class Vaca extends Animal {

    constructor(peso) {
        super(peso)
        this.sed = false
    }

    comer(gramos) {
        this.peso += gramos / 3
        this.sed = true
    }

    beber() {
        this.sed = false
        this.peso -= 500
    }

    tieneSed() {
        return this.sed
    }

    tieneHambre() {
        return this.peso < 200000
    }

    llevarACaminar() {
        this.peso -= 3000
    }

}

class Cerdo extends Animal {

    constructor(peso) {
        super()
        this.peso = peso
        this.sed = false
        this.comioLaUltimaVez = 0
        this.comioSinBeber = 0
    }

    comer(gramos) {
        if (gramos > 200) {
            this.peso += gramos - 200 
        }
        this.comioLaUltimaVez = gramos
        this.comioSinBeber += 1
        if (this.comioSinBeber === 3) {
            this.sed = true
        } 
    }

    cuantoComioLaUltimaVez() {
        return this.comioLaUltimaVez
    }

    beber() {
        this.sed = false
        this.hambre = true
        this.peso -= 1
        this.comioSinBeber = 0
    }

    convieneVacunar() {
        return true
    }

    tieneHambre() {
        return this.comioLaUltimaVez < 1000
    }

    tieneSed() {
        return this.sed
    }

    llevarACaminar() {
        this.peso -= 3000
    }

}

class Gallina extends Animal {

    constructor() {
        super(4000)
        this.vecesQueComio = 0
    }

    getPeso() {
        return 4000
    }

    comer() {
        this.vecesQueComio += 1
    }

    vecesQueFueAComer() {
        return this.vecesQueComio
    }

    tieneHambre() {
        return true
    }

    tieneSed() {
        return false
    }

    convieneVacunar() {
        return false
    }

}