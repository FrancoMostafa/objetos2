class EstacionDeServicio {
    
    constructor(dispositivos) {
        this.dispositivos = dispositivos
        this.animalesAtendidos = new Set
    }

    getDispositivos() {
        return this.dispositivos
    }

    getAnimalesAtendidos() {
        return this.animalesAtendidos
    }

    recargarDispositivos() {
        let dispositivos = this.getDispositivos().filter(d => d.necesitaRecarga())
        dispositivos.forEach(d => d.recargar())
    }

    puedeAtenderAlAnimal(unAnimal) {
        return this.getDispositivos().some(d => d.puedeAtenderAlAnimal(unAnimal))
    }

    fueAtendidoElAnimal(unAnimal) {
        return this.animalesAtendidos.has(unAnimal)
    }

    animalesAtendidosQueNecesitanSerVacunados() {
        return [...this.animalesAtendidos].filter(a => a.convieneVacunar())
    }

    animalMasPesadoAtendido() {
        let animal = [...this.animalesAtendidos][0]
        for (var i = 0; i < this.animalesAtendidos.size; i++) {
            if (animal.getPeso() < [...this.animalesAtendidos][i].getPeso()) {
                animal = [...this.animalesAtendidos][i]
            }
        }
        return animal
    }

    pesoTotalDeAnimalesAtendidos() {
        let animales = [...this.animalesAtendidos].map(a => a.getPeso())
        var result = animales.reduce(function(a, b){ return a + b; })
        return result
    }

}