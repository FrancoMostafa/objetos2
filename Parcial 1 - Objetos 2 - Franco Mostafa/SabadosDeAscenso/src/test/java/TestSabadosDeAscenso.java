import Emisora.Emisora;
import Equipo.Equipo;
import Jugador.Jugador;
import Partido.Partido;
import Sistema.Sistema;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestSabadosDeAscenso {


    @DisplayName("Test Sabados De Ascenso")
    @Test
    void test() {
        // CREAR SISTEMA

        Sistema sistema = new Sistema();

        // CREAR EMISORAS Y REGISTRARSE EN EL SISTEMA

        Emisora elAscenso = new Emisora("EL ASCENSO");
        Emisora AM910 = new Emisora("AM 910");
        Emisora radioContinental = new Emisora("RADIO CONTINENTAL");
        elAscenso.registarseEnSistema(sistema);
        AM910.registarseEnSistema(sistema);
        radioContinental.registarseEnSistema(sistema);

        // CREAR EQUIPOS,PARTIDOS Y UN JUGADOR.

        Equipo laMadrid = new Equipo("La Madrid");
        Equipo deportivoRiestra = new Equipo("Deportivo Riestra");
        Equipo defensoresDeBelgrano = new Equipo("Defensores De Belgrano");
        Equipo atlas = new Equipo("Atlas");
        Jugador tevez = new Jugador("Carlos Tevez");
        Partido partido1 = new Partido(laMadrid,defensoresDeBelgrano);
        Partido partido2 = new Partido(deportivoRiestra,atlas);

        // NOTIFICAR

        System.out.println("-----Comienzo partido 1 (EL ASCENSO)-----");

        elAscenso.notificarComienzoDePartido(partido1);

        System.out.println("-----Comienzo partido 2 (AM910)-----");

        AM910.notificarComienzoDePartido(partido2);

        System.out.println("-----final primer tiempo partido 1 (RADIO CONTINENTAL)-----");

        radioContinental.notificarFinalDelPrimerTiempo(partido1);

        System.out.println("-----final primer tiempo partido 2 (EL ASCENSO)-----");

        elAscenso.notificarFinalDelPrimerTiempo(partido2);

        System.out.println("-----amarrilla a tevez (AM 910)-----");

        AM910.notificarTarjetaAmarilla(partido1,tevez,70,"Pequeña patada");

        System.out.println("-----roja a tevez (RADIO CONTINENTAL)-----");

        radioContinental.notificarTarjetaRoja(partido1,tevez, 80, "Plancha arriba");

        System.out.println("-----Gol de La Madrid (EL ASCENSO)-----");

        elAscenso.notificarGolDelEquipo(laMadrid,partido1,55, "De volea");

        System.out.println("-----Final partido 1 (AM 910)-----");

        AM910.notificarFinalDelPartido(partido1);

        System.out.println("-----Final partido 2 (RADIO CONTINENTAL)-----");

        radioContinental.notificarFinalDelPartido(partido2);

    }
}
