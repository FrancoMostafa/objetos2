package Emisora;

import Equipo.Equipo;
import Jugador.Jugador;
import Partido.Partido;
import Sistema.Sistema;

public class Emisora {

    private final String nombre;

    private Sistema sistema;

    public Emisora(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void registarseEnSistema(Sistema sistema) {
        if (this.sistema != sistema) {
            this.sistema = sistema;
            sistema.getEmisoras().add(this);
        }
    }

    public void notificar(String mensaje) {
        System.out.println(this.getNombre() + " INFORMA: " + mensaje);
    }

    public void notificarComienzoDePartido(Partido partido) {
        String mensaje = "comienza el partido: " + partido.descripcion();
        sistema.notificar(this,mensaje);
    }

    public void notificarFinalDelPrimerTiempo(Partido partido) {
        String mensaje = partido.descripcion() + ": final del primer tiempo";
        sistema.notificar(this,mensaje);
    }

    public void notificarFinalDelPartido(Partido partido) {
        String mensaje = partido.descripcion() + ": final del partido";
        sistema.notificar(this,mensaje);
    }

    public void notificarGolDelEquipo(Equipo equipo, Partido partido, Integer minuto, String descripcion) {
        try {
            if (partido.getPrimerEquipo() == equipo || partido.getSegundoEquipo() == equipo) {
                String mensaje = partido.descripcion() + ": Gol de " + equipo.getNombre() + " en el minuto " + minuto.toString() + ", " + descripcion;
                sistema.notificar(this,mensaje);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Este equipo no esta jugando este partido");
        }
    }

    public void notificarTarjetaAmarilla(Partido partido, Jugador jugador, Integer minuto, String descripcion) {
        String mensaje = partido.descripcion() + ": amarrilla para " + jugador.getNombreYApellido() + " en el minuto " + minuto.toString() + ", " + descripcion;;
        sistema.notificar(this,mensaje);
    }

    public void notificarTarjetaRoja(Partido partido, Jugador jugador, Integer minuto, String descripcion) {
        String mensaje = partido.descripcion() + ": roja para " + jugador.getNombreYApellido() + " en el minuto " + minuto.toString() + ", " + descripcion;;
        sistema.notificar(this,mensaje);
    }

}
