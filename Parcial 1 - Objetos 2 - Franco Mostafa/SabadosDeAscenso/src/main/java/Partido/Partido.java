package Partido;

import Equipo.Equipo;

public class Partido {

    private Equipo primerEquipo;

    private Equipo segundoEquipo;

    public Partido(Equipo primerEquipo, Equipo segundoEquipo) {
        this.primerEquipo = primerEquipo;
        this.segundoEquipo = segundoEquipo;
    }

    public Equipo getPrimerEquipo() {
        return primerEquipo;
    }

    public Equipo getSegundoEquipo() {
        return segundoEquipo;
    }

    public String descripcion() {
        return primerEquipo.getNombre() + " vs " + segundoEquipo.getNombre();
    }

}
