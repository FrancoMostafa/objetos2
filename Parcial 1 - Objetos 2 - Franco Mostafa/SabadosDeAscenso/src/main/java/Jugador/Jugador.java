package Jugador;

public class Jugador {

    private String nombreYApellido;

    public Jugador(String nombreYApellido) {
        this.nombreYApellido = nombreYApellido;
    }

    public String getNombreYApellido() {
        return nombreYApellido;
    }
}
