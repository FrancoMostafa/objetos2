package Sistema;

import Emisora.Emisora;
import java.util.ArrayList;
import java.util.List;

public class Sistema {

    private List<Emisora> emisoras;

    public Sistema() {
        this.emisoras = new ArrayList<>();
    }

    public List<Emisora> getEmisoras() {
        return emisoras;
    }

    public void notificar(Emisora emisora, String mensaje) {
        var emisorasANotificar = emisoras.stream().filter(e -> e != emisora);
        emisorasANotificar.forEach(e -> e.notificar(mensaje));
    }

}
