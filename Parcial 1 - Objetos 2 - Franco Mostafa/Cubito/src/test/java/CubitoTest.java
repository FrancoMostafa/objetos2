import model.Cubito.Cubito;
import model.Departamento.Departamento;
import model.Departamento.DepartamentoGerencial;
import model.Departamento.DepartamentoOperativo;
import model.Empleado.Empleado;
import model.Estado.EstadoContingencia;
import model.Estado.EstadoNormal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CubitoTest {

    @DisplayName("Test Cubito")
    @Test
    void testCubito() {

        // CREACION DE DEPARTAMENTOS

        Empleado tomas = new Empleado("Tomas");
        Empleado carlos = new Empleado("Carlos");
        Empleado silva = new Empleado("Silva");
        List<Empleado> empleadosDeMarketing = new ArrayList<>();
        empleadosDeMarketing.add(tomas);
        empleadosDeMarketing.add(carlos);
        empleadosDeMarketing.add(silva);
        DepartamentoOperativo marketing = new DepartamentoOperativo(empleadosDeMarketing,new EstadoNormal(),50);


        Empleado jose = new Empleado("Jose");
        Empleado pepe = new Empleado("Pepe");
        List<Empleado> empleadosDeVentas = new ArrayList<>();
        empleadosDeVentas.add(jose);
        empleadosDeVentas.add(pepe);
        DepartamentoOperativo ventas = new DepartamentoOperativo(empleadosDeVentas,new EstadoNormal(), 50);

        Empleado juan = new Empleado("Juan");
        Empleado lucia = new Empleado("Lucia");
        Empleado borre = new Empleado("Borre");
        Empleado cacho = new Empleado("Cacho");
        List<Empleado> empleadosDeTurismo = new ArrayList<>();
        empleadosDeTurismo.add(juan);
        empleadosDeTurismo.add(lucia);
        empleadosDeTurismo.add(borre);
        empleadosDeTurismo.add(cacho);
        List<Departamento> departamentosIntegradosDeTurismo = new ArrayList<>();
        departamentosIntegradosDeTurismo.add(ventas);
        DepartamentoGerencial turismo = new DepartamentoGerencial(empleadosDeTurismo,new EstadoContingencia(),50,departamentosIntegradosDeTurismo);

        // INSTANCIAR CUBITO

        List<Departamento> departamentosDeCubito = new ArrayList<>();
        departamentosDeCubito.add(marketing);
        departamentosDeCubito.add(turismo);
        Cubito cubito = Cubito.getInstance(departamentosDeCubito);

        // CONSULTA DE CANTIDAD DE EMPLEADOS TRABAJANDO

        Assertions.assertEquals(7,cubito.cantidadDeEmpleadosTrabajando());
    }

}
