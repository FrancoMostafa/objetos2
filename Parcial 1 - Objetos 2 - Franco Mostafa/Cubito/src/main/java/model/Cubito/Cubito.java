package model.Cubito;

import model.Departamento.Departamento;

import java.util.List;

public class Cubito {

    private static Cubito instance;

    private List<Departamento> departamentos;

    private Cubito(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public static Cubito getInstance(List<Departamento> departamentos) {
        if (instance == null) {
            instance = new Cubito(departamentos);
        }
        return instance;
    }

    public void agregarDepartamento(Departamento departamento) {
        departamentos.add(departamento);
    }

    public Integer cantidadDeEmpleadosTrabajando() {
        return departamentos.stream().mapToInt(Departamento::cantidadDeEmpleadosTrabajando).sum();
    }

}
