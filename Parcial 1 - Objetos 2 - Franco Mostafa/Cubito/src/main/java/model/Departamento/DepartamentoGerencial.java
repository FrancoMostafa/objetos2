package model.Departamento;

import model.Empleado.Empleado;
import model.Estado.Estado;

import java.util.List;

public class DepartamentoGerencial extends Departamento {

    private List<Departamento> departamentosIntegrados;

    public DepartamentoGerencial(List<Empleado> empleados, Estado estado, Integer porcentajeMinimoDeTrabajadores, List<Departamento> departamentosIntegrados) {
        super(empleados,estado,porcentajeMinimoDeTrabajadores);
        this.departamentosIntegrados = departamentosIntegrados;
    }

    public void agregarDepartamentoIntegrado(Departamento departamento) {
        departamentosIntegrados.add(departamento);
    }

    public Integer cantidadDeEmpleadosTrabajando() {
        return this.getEstado().cantidadDeEmpleadosTrabajando(this) +
                this.departamentosIntegrados.stream().mapToInt(Departamento::cantidadDeEmpleadosTrabajando).sum();
    }
}
