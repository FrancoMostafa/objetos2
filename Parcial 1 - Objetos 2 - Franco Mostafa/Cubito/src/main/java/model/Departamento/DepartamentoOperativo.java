package model.Departamento;

import model.Empleado.Empleado;
import model.Estado.Estado;

import java.util.List;

public class DepartamentoOperativo extends Departamento {

    public DepartamentoOperativo(List<Empleado> empleados, Estado estado, Integer porcentajeMinimoDeTrabajadores) {
        super(empleados, estado, porcentajeMinimoDeTrabajadores);
    }

    public Integer cantidadDeEmpleadosTrabajando() {
        return this.getEstado().cantidadDeEmpleadosTrabajando(this);
    }

}
