package model.Departamento;


import model.Empleado.Empleado;
import model.Estado.Estado;
import model.Estado.EstadoContingencia;
import model.Estado.EstadoNormal;

import java.util.List;

public abstract class Departamento {

    private List<Empleado> empleados;

    private Estado estado;

    private Integer porcentajeMinimoDeTrabajadores;

    public Departamento(List<Empleado> empleados, Estado estado, Integer porcentajeMinimoDeTrabajadores) {
        this.empleados = empleados;
        this.estado = estado;
        this.porcentajeMinimoDeTrabajadores = porcentajeMinimoDeTrabajadores;
    }

    public Integer getPorcentajeMinimoDeTrabajadores() {
        return porcentajeMinimoDeTrabajadores;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public Estado getEstado() {
        return estado;
    }

    public Integer getCantidadDeEmpleadosTotal() {
        return this.getEmpleados().size();
    }

    public void modificarEstadoANormal() {
        this.estado = new EstadoNormal();
    }

    public void modificarEstadoAContingencia() {
        this.estado = new EstadoContingencia();
    }

    public void agregarEmpleado(Empleado empleado) {
        empleados.add(empleado);
    }

    public abstract Integer cantidadDeEmpleadosTrabajando();

}
