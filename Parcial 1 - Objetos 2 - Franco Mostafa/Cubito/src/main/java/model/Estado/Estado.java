package model.Estado;

import model.Departamento.Departamento;

public interface Estado {

    Integer cantidadDeEmpleadosTrabajando(Departamento departamento);

}
