package model.Estado;

import model.Departamento.Departamento;

public class EstadoNormal implements Estado {

    public Integer cantidadDeEmpleadosTrabajando(Departamento departamento) {
        return departamento.getCantidadDeEmpleadosTotal();
    }

}
