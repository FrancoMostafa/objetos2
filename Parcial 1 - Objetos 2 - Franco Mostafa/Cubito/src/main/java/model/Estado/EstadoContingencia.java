package model.Estado;

import model.Departamento.Departamento;

public class EstadoContingencia implements Estado {

    public Integer cantidadDeEmpleadosTrabajando(Departamento departamento) {
        var valor = (departamento.getCantidadDeEmpleadosTotal() * departamento.getPorcentajeMinimoDeTrabajadores()) / 100;
        return Math.round(valor);
    }

}
