package MonstersOfRock.DiscosYBandas.Discos;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Disco {

    private String nombreDisco;

    private Integer anio;

    private List<CopiasDeDisco> copiasDeDiscos;

    public Disco(String nombreDisco, Integer anio, List<CopiasDeDisco> copiasDeDiscos) {
        this.nombreDisco = nombreDisco;
        this.anio = anio;
        try {
            var paises = copiasDeDiscos.stream().map(CopiasDeDisco::getPais).collect(Collectors.toList());
            if (paises.stream().anyMatch(p -> Collections.frequency(paises, p) > 1)) {
                throw new Exception();
            } else {
                this.copiasDeDiscos = copiasDeDiscos;
            }
        }
        catch (Exception e) {
            System.out.println("Hay un valor de pais repetido en el disco " + nombreDisco + ", intente nuevamente");
        }
    }

    public Integer getAnio() {
        return anio;
    }

    public List<CopiasDeDisco> getCopiasDeDiscos() {
        return copiasDeDiscos;
    }

    public Integer copiasVendidasEnTotal() {
        return copiasDeDiscos.stream().mapToInt(CopiasDeDisco::getNumeroDeCopiasVendidas).sum();
    }

}
