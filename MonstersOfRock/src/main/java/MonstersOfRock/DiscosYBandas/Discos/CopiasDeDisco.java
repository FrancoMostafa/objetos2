package MonstersOfRock.DiscosYBandas.Discos;

import MonstersOfRock.DiscosYBandas.Pais.Pais;

public class CopiasDeDisco {

    private Integer numeroDeCopiasVendidas;

    private Pais pais;

    public CopiasDeDisco(Integer numeroDeCopiasVendidas, Pais pais) {
        this.numeroDeCopiasVendidas = numeroDeCopiasVendidas;
        this.pais = pais;
    }

    public Integer getNumeroDeCopiasVendidas() {
        return numeroDeCopiasVendidas;
    }

    public Pais getPais() {
        return pais;
    }

}
