package MonstersOfRock.DiscosYBandas.Banda;

import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.GenerosMusicales.GeneroMusical;

import java.util.Comparator;
import java.util.List;

public class Banda {

    private String nombreBanda;

    private List<Disco> discos;

    private Integer cachet;

    private Pais pais;

    private GeneroMusical generoMusical;

    public Banda(String nombreBanda, List<Disco> discos, Integer cachet, Pais pais, GeneroMusical generoMusical) {
        this.nombreBanda = nombreBanda;
        this.discos = discos;
        this.cachet = cachet;
        this.pais = pais;
        this.generoMusical = generoMusical;
    }

    public Pais getPais() {
        return pais;
    }

    public GeneroMusical getGeneroMusical() {
        return generoMusical;
    }

    public List<Disco> getDiscos() {
        return discos;
    }

    public Integer getCachet() {
        return cachet;
    }

    public Disco ultimoDisco() {
        try {
            if (discos.size() >= 1) {
                return discos.stream().max(Comparator.comparing(Disco::getAnio)).get();
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La banda " + nombreBanda + " no tiene discos");
            return null;
        }
    }

    public void participarEnEvento(Evento evento, Disco discoAPresentar, Integer minutosQueToca) {
        try {
            if (this.puedeParticiparEnElEvento(evento, discoAPresentar, minutosQueToca)) {
                evento.integrarBanda(this, discoAPresentar, minutosQueToca);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La banda " + nombreBanda +  " no puede participar en el " + evento.getNombreEvento());
        }

    }

    public Boolean puedeParticiparEnElEvento(Evento evento, Disco discoAPresentar, Integer minutosQueToca) {
        return evento.puedeParticiparLaBanda(this,discoAPresentar,minutosQueToca);
    }

    public Integer copiasVendidasEnTotal() {
        return discos.stream().mapToInt(Disco::copiasVendidasEnTotal).sum();
    }

}
