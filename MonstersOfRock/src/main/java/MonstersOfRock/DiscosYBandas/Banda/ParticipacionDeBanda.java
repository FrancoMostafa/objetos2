package MonstersOfRock.DiscosYBandas.Banda;

import MonstersOfRock.DiscosYBandas.Discos.Disco;

public class ParticipacionDeBanda {

    private Banda banda;

    private Disco discoAPresentar;

    private Integer minutosQueToca;

    public ParticipacionDeBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca) {
        this.banda = banda;
        this.discoAPresentar = discoAPresentar;
        this.minutosQueToca = minutosQueToca;
    }

    public Banda getBanda() {
        return banda;
    }

    public Integer getMinutosQueToca() {
        return minutosQueToca;
    }

}
