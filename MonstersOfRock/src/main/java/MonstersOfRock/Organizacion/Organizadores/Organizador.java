package MonstersOfRock.Organizacion.Organizadores;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.Sede;

public abstract class Organizador {

    protected String nombreOrganizador;

    public Organizador(String nombreOrganizador) {
        this.nombreOrganizador = nombreOrganizador;
    }

    public void organizarEvento(Evento evento) {
        try {
            if(this.puedeRealizarElEvento(evento)) {
                evento.setOrganizador(this);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La organizadora " + nombreOrganizador + " no puede realizar el " + evento.getNombreEvento());
        }
    }

    public abstract Boolean puedeRealizarElEvento(Evento evento);

    public abstract Boolean puedeParticiparLaBanda(Banda banda, Disco discoAPresentar);

}
