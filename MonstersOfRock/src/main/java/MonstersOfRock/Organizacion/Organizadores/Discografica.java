package MonstersOfRock.Organizacion.Organizadores;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.Evento.Evento;

import javax.security.auth.login.CredentialNotFoundException;
import java.util.List;

public class Discografica extends Organizador {

    private List<Disco> discosProducidos;

    public Discografica(String nombreOrganizador, List<Disco> discosProducidos) {
        super(nombreOrganizador);
        this.discosProducidos = discosProducidos;
    }

    @Override
    public Boolean puedeRealizarElEvento(Evento evento) {
        return true;
    }

    public Boolean puedeParticiparLaBanda(Banda banda, Disco discoAPresentar) {
        return this.discosProducidos.contains(banda.ultimoDisco()) ||
                this.discosProducidos.contains(discoAPresentar);
    }

}
