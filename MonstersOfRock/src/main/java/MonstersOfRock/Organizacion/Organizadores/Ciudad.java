package MonstersOfRock.Organizacion.Organizadores;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.Evento.Evento;

public class Ciudad extends Organizador {

    private Pais pais;

    public Ciudad(String nombreOrganizador, Pais pais) {
        super(nombreOrganizador);
        this.pais = pais;
    }

    public Boolean puedeRealizarElEvento(Evento evento) {
        return evento.getSede().getPais().equals(pais);
    }

    public Boolean puedeParticiparLaBanda(Banda banda, Disco discoAPresentar) {
        return this.getPais().equals(banda.getPais());
    }

    public Pais getPais() {
        return pais;
    }

}
