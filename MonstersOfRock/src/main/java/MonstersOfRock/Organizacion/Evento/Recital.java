package MonstersOfRock.Organizacion.Evento;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.Organizadores.Ciudad;
import MonstersOfRock.Organizacion.Organizadores.Discografica;
import MonstersOfRock.Organizacion.Sede.Estadio;
import MonstersOfRock.Organizacion.Sede.Sede;

public class Recital extends Evento {

    private Integer importePublicidad;

    public Recital(String nombreEvento, Sede sede, Integer financiamientoInicial, Integer precioDeEntrada, Integer importePublicidad) {
        super(nombreEvento, sede, financiamientoInicial, precioDeEntrada);
        this.importePublicidad = importePublicidad;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    @Override
    public Boolean elEventoPermiteParticiparALaBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca) {
        return bandaPrincipal == null || banda.getGeneroMusical().equals(bandaPrincipal.getBanda().getGeneroMusical()) && this.bandasParticipantes.size() <= 3 && laBandaNoRompeCondicionDePrincipal(banda);
    }

    @Override
    public Boolean laBandaNoRompeCondicionDePrincipal(Banda banda) {
            return bandaPrincipal == null || banda.copiasVendidasEnTotal() * 3 < bandaPrincipal.getBanda().copiasVendidasEnTotal();
    }

    @Override
    public Double costoBasico() {
        if (bandasParticipantes.size() == 0) {
            return 0.00 + + sede.costoDeAlquilerParaEvento(this) / 2 + importePublicidad;
        }
        else {
            return (double) bandaPrincipal.getBanda().getCachet() + sede.costoDeAlquilerParaEvento(this) / 2 + importePublicidad;
        }
    }

    @Override
    public Double importeExtra() {
        return (double) 250000;
    }

    @Override
    public Boolean laSedeEsCorrecta(Sede sede) {
        return true;
    }

}
