package MonstersOfRock.Organizacion.Evento;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Banda.ParticipacionDeBanda;
import MonstersOfRock.DiscosYBandas.Discos.CopiasDeDisco;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.GenerosMusicales.GeneroMusical;
import MonstersOfRock.Organizacion.Organizadores.Ciudad;
import MonstersOfRock.Organizacion.Organizadores.Discografica;
import MonstersOfRock.Organizacion.Organizadores.Organizador;
import MonstersOfRock.Organizacion.Sede.Sede;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Evento {

    protected String nombreEvento;

    protected Organizador organizador;

    protected Sede sede;

    protected ParticipacionDeBanda bandaPrincipal;

    protected List<ParticipacionDeBanda> bandasParticipantes;

    protected Integer financiamientoInicial;

    protected Integer precioDeEntrada;

    public Evento(String nombreEvento, Sede sede, Integer financiamientoInicial, Integer precioDeEntrada) {
        this.nombreEvento = nombreEvento;
        this.organizador = null;
        this.sede = sede;
        if(sede != null) {
            sede.agregarEvento(this);
        }
        this.bandaPrincipal = null;
        this.bandasParticipantes = new ArrayList<>();
        this.financiamientoInicial = financiamientoInicial;
        this.precioDeEntrada = precioDeEntrada;
    }

    public List<ParticipacionDeBanda> getBandasParticipantes() {
        return bandasParticipantes;
    }

    public abstract void setSede(Sede sede);

    public Sede getSede() {
        return sede;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setOrganizador(Organizador organizador) {
        this.organizador = organizador;
    }

    public void setBandaPrincipal(ParticipacionDeBanda banda) {
        this.bandaPrincipal = banda;
    }

    public void integrarBanda(Banda banda, Disco discoQuePresenta, Integer minutosQueToca) {
        if(!bandasParticipantes.stream().map(ParticipacionDeBanda::getBanda).collect(Collectors.toList()).contains(banda)) {
            var participacionBanda = new ParticipacionDeBanda(banda, discoQuePresenta, minutosQueToca);
            if (bandasParticipantes.size() == 0) {
                this.setBandaPrincipal(participacionBanda);
            }
            bandasParticipantes.add(participacionBanda);
        }
    }

    public Boolean puedeParticiparLaBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca) {
        try {
            if (organizador.getClass().equals(Ciudad.class) || organizador.getClass().equals(Discografica.class)) {
                return organizador.puedeParticiparLaBanda(banda, discoAPresentar) && this.elEventoPermiteParticiparALaBanda(banda, discoAPresentar, minutosQueToca) && this.laBandaNoRompeCondicionDePrincipal(banda) && !banda.getDiscos().contains(discoAPresentar);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("El evento " + nombreEvento + " todavia no tiene organizador");
            return false;
        }
    }

    public abstract Boolean elEventoPermiteParticiparALaBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca);

    public abstract Boolean laBandaNoRompeCondicionDePrincipal(Banda banda);

    public List<GeneroMusical> generosMusicalesQueIncluyeElEvento() {
        return bandasParticipantes.stream().map(b -> b.getBanda().getGeneroMusical()).distinct().collect(Collectors.toList());
    }

    public Integer duracionDelEvento() {
        return bandasParticipantes.stream().mapToInt(ParticipacionDeBanda::getMinutosQueToca).sum();
    }

    public Boolean esEconomicamenteFactible() {
        return this.ingresoAsegurado() > this.costoBasico();
    }

    public abstract Double costoBasico();

    public Double ingresoAsegurado() {
        return financiamientoInicial + this.importeExtra();
    }

    public abstract Double importeExtra();

    public Boolean sePuedeHacerEnLaSede(Sede sede) {
        try {
            if (organizador == null) {
                throw new Exception();
            } else {
                return organizador.puedeRealizarElEvento(this) && this.laSedeEsCorrecta(sede);
            }
        }
        catch (Exception e) {
            System.out.println("El evento " + nombreEvento + " todavia no tiene organizador, por ende, no se puede determinar si la sede es correcta");
            return false;
        }
    }

    public abstract Boolean laSedeEsCorrecta(Sede sede);

    public Integer indiceDeExitoPotencial() {
        try {
            if (sede != null) {
                var sum1 = this.copiasVendidasEnElPaisDelEvento().stream().mapToInt(CopiasDeDisco::getNumeroDeCopiasVendidas).sum() + this.bandasParticipantes.size();
                var sum2 = (sum1 * (this.bandasDelPaisDelEvento().size() * 5)) / 100;
                return sum1 + sum2;
            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("No se puede sacar el indice de exito potencial de " + nombreEvento + ", su sede esta en valor null");
            return 0;
        }
    }

    public List<CopiasDeDisco> copiasVendidasEnElPaisDelEvento() {
        //
        List<CopiasDeDisco> todasLasCopias = new ArrayList<>();
        List<CopiasDeDisco> copiasDelPaisDelEvento;
        List<Disco> todosLosDiscos = new ArrayList<>();
        List<Banda> bandasDelPaisDelEvento;
        //
        var listasDeDiscos = bandasParticipantes.stream().map(b -> b.getBanda().getDiscos()).collect(Collectors.toList());
        listasDeDiscos.forEach(todosLosDiscos::addAll);
        var listaDeCopias = todosLosDiscos.stream().map(Disco::getCopiasDeDiscos).collect(Collectors.toList());
        listaDeCopias.forEach(todasLasCopias::addAll);
        return todasLasCopias.stream().filter(c -> c.getPais().equals(sede.getPais())).collect(Collectors.toList());
    }

    public List<Banda> bandasDelPaisDelEvento() {
        var todasLasBandas = bandasParticipantes.stream().map(ParticipacionDeBanda::getBanda).collect(Collectors.toList());
        return todasLasBandas.stream().filter(b -> b.getPais().equals(sede.getPais())).collect(Collectors.toList());
    }

    public List<Banda> bandasExtranjeras() {
        try {
            if (sede != null) {
                var todasLasBandas = bandasParticipantes.stream().map(ParticipacionDeBanda::getBanda).collect(Collectors.toList());
                return todasLasBandas.stream().filter(b -> !b.getPais().equals(sede.getPais())).collect(Collectors.toList());
            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("No se puede obtener las bandas extranjeras de " + nombreEvento + ", su sede esta en valor null");
            return new ArrayList<>();
        }
    }

}
