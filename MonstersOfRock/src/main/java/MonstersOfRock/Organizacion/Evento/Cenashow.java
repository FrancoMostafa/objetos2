package MonstersOfRock.Organizacion.Evento;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.Sede.Anfiteatro;
import MonstersOfRock.Organizacion.Sede.Sede;

public class Cenashow extends Evento {

    public Cenashow(String nombreEvento, Sede sede, Integer financiamientoInicial, Integer precioDeEntrada) {
        super(nombreEvento, null, financiamientoInicial, precioDeEntrada);
        this.setSede(sede);
    }

    public void setSede(Sede sede) {
        try {
            if(this.laSedeEsCorrecta(sede)) {
                this.sede = sede;
                sede.agregarEvento(this);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La sede de un Cenashow solo puede ser un Anfiteatro, setear nuevamente la sede de " + nombreEvento);
        }
    }

    @Override
    public Boolean elEventoPermiteParticiparALaBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca) {
        return this.bandaPrincipal == null;
    }

    @Override
    public Boolean laBandaNoRompeCondicionDePrincipal(Banda banda) {
        return true;
    }

    @Override
    public Double costoBasico() {
        if (bandasParticipantes.size() == 0) {
            return 80000.00;
        }
        else {
            return (double) (bandaPrincipal.getBanda().getCachet() * 80 / 100) + 80000;
        }
    }

    @Override
    public Double importeExtra() {
        if (sede == null) {
            return 0.00;
        }
        return (double) (precioDeEntrada * this.getSede().getCapacidad()) / 50;
    }

    @Override
    public Boolean laSedeEsCorrecta(Sede sede) {
        return sede.getClass().equals(Anfiteatro.class);
    }


}
