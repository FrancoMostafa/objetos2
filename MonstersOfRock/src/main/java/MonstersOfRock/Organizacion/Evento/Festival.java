package MonstersOfRock.Organizacion.Evento;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.EmpresaAuspiciante.EmpresaAuspiciante;
import MonstersOfRock.Organizacion.GenerosMusicales.GeneroMusical;
import MonstersOfRock.Organizacion.Sede.Anfiteatro;
import MonstersOfRock.Organizacion.Sede.Estadio;
import MonstersOfRock.Organizacion.Sede.Sede;

import javax.print.attribute.standard.JobName;
import java.util.List;

public class Festival extends Evento {

    private List<GeneroMusical> generosMusicales;

    private List<EmpresaAuspiciante> empresasAuspiciantes;

    public Festival(String nombreEvento, Sede sede, Integer financiamientoInicial, Integer precioDeEntrada, List<GeneroMusical> generosMusicales, List<EmpresaAuspiciante> empresasAuspiciantes) {
        super(nombreEvento, null, financiamientoInicial, precioDeEntrada);
        this.generosMusicales = generosMusicales;
        this.bandaPrincipal = null;
        this.empresasAuspiciantes = empresasAuspiciantes;
        this.setSede(sede);
    }

    public void setSede(Sede sede) {
        try {
            if(this.sePuedeHacerEnLaSede(sede)) {
                this.sede = sede;
                sede.agregarEvento(this);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La sede de un Festval solo puede ser un Estadio, setear nuevamente la sede de " + nombreEvento);
        }
    }

    public Boolean laBandaTocaUnGeneroQueEstaEnElEvento(Banda banda) {
        return generosMusicales.contains(banda.getGeneroMusical());
    }

    public Boolean noSeExcedeElLimiteDeTiempo(Integer minutosQueToca) {
        return this.duracionDelEvento() + minutosQueToca <= 720;
    }

    @Override
    public Boolean elEventoPermiteParticiparALaBanda(Banda banda, Disco discoAPresentar, Integer minutosQueToca) {
        return this.laBandaTocaUnGeneroQueEstaEnElEvento(banda) && this.noSeExcedeElLimiteDeTiempo(minutosQueToca)
                && this.laBandaNoRompeCondicionDePrincipal(banda);
    }

    @Override
    public Boolean laBandaNoRompeCondicionDePrincipal(Banda banda) {
        return bandaPrincipal == null || banda.copiasVendidasEnTotal() < bandaPrincipal.getBanda().copiasVendidasEnTotal();
    }

    @Override
    public Double costoBasico() {
        if (bandasParticipantes.size() == 0) {
            return 0.00;
        }
        else if (bandasParticipantes.size() == 1) {
            return (double) bandaPrincipal.getBanda().getCachet();
        }
        else {
            var bandasSecundarias = bandasParticipantes.stream().filter(b -> b.getBanda() != bandaPrincipal.getBanda());
            return (double) bandaPrincipal.getBanda().getCachet() + bandasSecundarias.mapToInt(b -> b.getBanda().getCachet()).sum() / 2;
        }
    }

    @Override
    public Double importeExtra() {
        if(empresasAuspiciantes.size() == 0) {
            return 0.00;
        }
        else {
            return (double) empresasAuspiciantes.stream().mapToInt(EmpresaAuspiciante::getAporte).sum();
        }
    }

    @Override
    public Boolean laSedeEsCorrecta(Sede sede) {
        return sede.getClass().equals(Estadio.class);
    }

}
