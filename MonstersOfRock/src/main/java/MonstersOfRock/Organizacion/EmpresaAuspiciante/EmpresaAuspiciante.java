package MonstersOfRock.Organizacion.EmpresaAuspiciante;

public class EmpresaAuspiciante {

    private String nombreEmpresa;

    private Integer aporte;

    public EmpresaAuspiciante(String nombreEmpresa, Integer aporte) {
        this.nombreEmpresa = nombreEmpresa;
        this.aporte = aporte;
    }

    public Integer getAporte() {
        return aporte;
    }

}
