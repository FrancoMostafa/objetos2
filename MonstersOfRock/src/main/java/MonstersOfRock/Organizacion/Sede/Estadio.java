package MonstersOfRock.Organizacion.Sede;

import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoPorFraccion;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoPorHora;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.TipoDeCosto;

public class Estadio extends Sede {

    public Estadio(String nombreSede, Pais pais, Integer capacidad, TipoDeCosto tipoDeCosto, Double precioAlquiler) {
        super(nombreSede, pais, capacidad, tipoDeCosto, precioAlquiler);
    }

    @Override
    public void setTipoDeCostoYPrecio(TipoDeCosto tipoDeCosto, Double precioAlquiler) {
        try {
            if (tipoDeCosto.getClass().equals(CostoPorHora.class) || tipoDeCosto.getClass().equals(CostoPorFraccion.class)) {
                this.tipoDeCosto = tipoDeCosto;
                this.precioAlquiler = precioAlquiler;
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Los estadios solo cobran por hora o por fraccion, setear nuevamente el tipo de costo de " + nombreSede);
        }
    }

}
