package MonstersOfRock.Organizacion.Sede;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.GenerosMusicales.GeneroMusical;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoFijo;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoPorFraccion;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoPorHora;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.TipoDeCosto;

import java.util.List;

public class Anfiteatro extends Sede {

    private List<GeneroMusical> generosEnPromocion;

    public Anfiteatro(String nombreSede, Pais pais, Integer capacidad, TipoDeCosto tipoDeCosto, Double precioAlquiler, List<GeneroMusical> generosEnPromocion) {
        super(nombreSede, pais, capacidad, tipoDeCosto, precioAlquiler);
        this.generosEnPromocion = generosEnPromocion;
    }

    public void setTipoDeCostoYPrecio(TipoDeCosto tipoDeCosto, Double precioAlquiler) {
        try {
            if (tipoDeCosto.getClass().equals(CostoFijo.class)) {
                this.tipoDeCosto = tipoDeCosto;
                this.precioAlquiler = precioAlquiler;
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Los Anfiteatros solo cobran por precio fijo, setear nuevamente el tipo de costo de " + nombreSede);
        }
    }

    @Override
    public Double costoDeAlquilerParaEvento(Evento evento) {
        if (evento.generosMusicalesQueIncluyeElEvento().stream().anyMatch(g -> generosEnPromocion.contains(g))) {
            return super.costoDeAlquilerParaEvento(evento) - ((super.costoDeAlquilerParaEvento(evento) * 20) / 100);
        }
        else {
            return super.costoDeAlquilerParaEvento(evento);
        }
    }

}
