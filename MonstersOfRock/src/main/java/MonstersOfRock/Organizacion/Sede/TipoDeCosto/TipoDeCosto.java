package MonstersOfRock.Organizacion.Sede.TipoDeCosto;

import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.Sede;

public abstract class TipoDeCosto {

    public TipoDeCosto() {}

    public abstract Double precioDeAlquilerParaEvento(Evento evento, Sede sede);

}
