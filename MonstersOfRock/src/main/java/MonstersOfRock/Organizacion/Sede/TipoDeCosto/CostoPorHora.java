package MonstersOfRock.Organizacion.Sede.TipoDeCosto;

import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.Sede;

public class CostoPorHora extends TipoDeCosto {

    public CostoPorHora() {
        super();
    }

    @Override
    public Double precioDeAlquilerParaEvento(Evento evento, Sede sede) {
        return evento.duracionDelEvento() / 60 * sede.getPrecioAlquiler();
    }

}
