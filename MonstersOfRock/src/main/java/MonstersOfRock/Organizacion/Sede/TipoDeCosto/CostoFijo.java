package MonstersOfRock.Organizacion.Sede.TipoDeCosto;

import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.Sede;

public class CostoFijo extends TipoDeCosto {

    public CostoFijo() {
        super();
    }

    @Override
    public Double precioDeAlquilerParaEvento(Evento evento, Sede sede) {
        return sede.getPrecioAlquiler();
    }

}
