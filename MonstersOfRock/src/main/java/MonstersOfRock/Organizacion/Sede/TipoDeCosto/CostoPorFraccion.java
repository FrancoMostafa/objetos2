package MonstersOfRock.Organizacion.Sede.TipoDeCosto;

import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.Sede;

public class CostoPorFraccion extends TipoDeCosto {

    private Double tiempoPorFraccion;

    public CostoPorFraccion(Double tiempoPorFraccion) {
        super();
        this.tiempoPorFraccion = tiempoPorFraccion;
    }

    @Override
    public Double precioDeAlquilerParaEvento(Evento evento, Sede sede) {
        return Math.abs(evento.duracionDelEvento() / tiempoPorFraccion * sede.getPrecioAlquiler());
    }

}
