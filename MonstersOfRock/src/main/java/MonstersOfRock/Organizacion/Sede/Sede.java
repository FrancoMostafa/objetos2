package MonstersOfRock.Organizacion.Sede;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.Evento.Evento;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.TipoDeCosto;

import java.io.DataInput;
import java.util.ArrayList;
import java.util.List;

public abstract class Sede {

    protected String nombreSede;

    protected Pais pais;

    protected Integer capacidad;

    protected List<Evento> eventosQueSeLlevanACaboAqui;

    protected TipoDeCosto tipoDeCosto;

    protected Double precioAlquiler;

    public Sede(String nombreSede, Pais pais, Integer capacidad, TipoDeCosto tipoDeCosto, Double precioAlquiler) {
        this.nombreSede = nombreSede;
        this.pais = pais;
        this.capacidad = capacidad;
        this.eventosQueSeLlevanACaboAqui = new ArrayList<>();
        this.setTipoDeCostoYPrecio(tipoDeCosto,precioAlquiler);
    }

    public Pais getPais() {
        return pais;
    }

    public Double getPrecioAlquiler() {
        return precioAlquiler;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public List<Evento> eventosQueSeLlevanACaboAqui() {
        return eventosQueSeLlevanACaboAqui;
    }

    public abstract void setTipoDeCostoYPrecio(TipoDeCosto tipoDeCosto, Double precioAlquiler);

    public void agregarEvento(Evento evento) {
        if(!eventosQueSeLlevanACaboAqui.contains(evento)) {
            eventosQueSeLlevanACaboAqui.add(evento);
        }
    }

    public Double costoDeAlquilerParaEvento(Evento evento) {
        return this.tipoDeCosto.precioDeAlquilerParaEvento(evento,this);
    }

}
