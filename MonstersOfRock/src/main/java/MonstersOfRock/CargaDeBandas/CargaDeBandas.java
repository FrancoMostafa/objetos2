package MonstersOfRock.CargaDeBandas;

import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Banda.ParticipacionDeBanda;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.Organizacion.Evento.Evento;

import java.util.Dictionary;

public class CargaDeBandas {

    private static CargaDeBandas instance;

    private CargaDeBandas() {}

    public static CargaDeBandas instance() {
        if(instance == null) {
            instance = new CargaDeBandas();
        }
        return instance;
    }

    public void cargarBandaAEvento(Banda banda, Disco discoAPresentar, Integer minutosQueToca, Evento evento) {
        banda.participarEnEvento(evento,discoAPresentar,minutosQueToca);
    }

}
