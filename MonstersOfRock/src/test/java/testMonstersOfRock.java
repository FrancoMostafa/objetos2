import MonstersOfRock.CargaDeBandas.CargaDeBandas;
import MonstersOfRock.DiscosYBandas.Banda.Banda;
import MonstersOfRock.DiscosYBandas.Discos.CopiasDeDisco;
import MonstersOfRock.DiscosYBandas.Discos.Disco;
import MonstersOfRock.DiscosYBandas.Pais.Pais;
import MonstersOfRock.Organizacion.EmpresaAuspiciante.EmpresaAuspiciante;
import MonstersOfRock.Organizacion.Evento.Cenashow;
import MonstersOfRock.Organizacion.Evento.Festival;
import MonstersOfRock.Organizacion.Evento.Recital;
import MonstersOfRock.Organizacion.GenerosMusicales.GeneroMusical;
import MonstersOfRock.Organizacion.Organizadores.Ciudad;
import MonstersOfRock.Organizacion.Organizadores.Discografica;
import MonstersOfRock.Organizacion.Sede.Anfiteatro;
import MonstersOfRock.Organizacion.Sede.Estadio;
import MonstersOfRock.Organizacion.Sede.Sede;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoFijo;
import MonstersOfRock.Organizacion.Sede.TipoDeCosto.CostoPorFraccion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class testMonstersOfRock {
    @DisplayName("Test Monsters Of Rock")
    @Test
    void testMonstersOfRock() {

        // EJERCICIOS PUNTO 2

        // PUNTO 1

        List<CopiasDeDisco> copiasDeDisco1 = new ArrayList<>();
        copiasDeDisco1.add(new CopiasDeDisco(10000, Pais.ARGENTINA));
        copiasDeDisco1.add(new CopiasDeDisco(5000, Pais.BRASIL));
        Disco disco1 = new Disco("Oktubre", 1970, copiasDeDisco1);
        List<Disco> discosDeBanda1 = new ArrayList<>();
        discosDeBanda1.add(disco1);
        List<CopiasDeDisco> copiasDeDisco2 = new ArrayList<>();
        copiasDeDisco2.add(new CopiasDeDisco(9000, Pais.ARGENTINA));
        Disco disco2 = new Disco("BestAlbum", 1971, copiasDeDisco2);
        discosDeBanda1.add(disco2);
        Banda banda1 = new Banda("Los rockeros", discosDeBanda1, 90000, Pais.ARGENTINA, GeneroMusical.ROCKALTERNATIVO);
        Assertions.assertEquals(banda1.copiasVendidasEnTotal(), 24000);

        // PUNTO 2

        Assertions.assertEquals(disco2, banda1.ultimoDisco());

        // PUNTO 3

        Anfiteatro anfiteatro1 = new Anfiteatro("Anfiteatro 1", Pais.ARGENTINA, 70000, new CostoFijo(),4000.00, new ArrayList<>());
        Cenashow cenashow1 = new Cenashow("Cenashow 1", anfiteatro1, 1000000, 500);
        Cenashow cenashow2 = new Cenashow("Cenashow 2", anfiteatro1, 1000000, 500);
        Assertions.assertEquals(anfiteatro1.eventosQueSeLlevanACaboAqui().size(), 2);

        // PUNTO 4

        List<CopiasDeDisco> copiasDeDisco3 = new ArrayList<>();
        copiasDeDisco3.add(new CopiasDeDisco(30000, Pais.ARGENTINA));
        Disco disco3 = new Disco("Locos del rock", 2000, copiasDeDisco3);
        discosDeBanda1.add(disco2);
        Ciudad ciudad1 = new Ciudad("La Plata", Pais.ARGENTINA);
        List<Disco> discosProducidosPorDiscografica1 = new ArrayList<>();
        discosProducidosPorDiscografica1.add(disco1);
        discosProducidosPorDiscografica1.add(disco2);
        Discografica discografica1 = new Discografica("Discografica 1", discosProducidosPorDiscografica1);
        ciudad1.organizarEvento(cenashow1);
        discografica1.organizarEvento(cenashow2);
        Assertions.assertEquals(banda1.puedeParticiparEnElEvento(cenashow1, disco3, 200), true);
        Assertions.assertEquals(banda1.puedeParticiparEnElEvento(cenashow2, disco3, 200), true);

        // PUNTO 5

        CargaDeBandas cargaDeBandas = CargaDeBandas.instance();
        cargaDeBandas.cargarBandaAEvento(banda1, disco3, 200, cenashow1);
        Assertions.assertEquals(cenashow1.getBandasParticipantes().size(), 1);

        // PUNTO 6

        List<CopiasDeDisco> copiasDeDiscos4 = new ArrayList<>();
        copiasDeDiscos4.add(new CopiasDeDisco(90, Pais.ARGENTINA));
        Disco disco4 = new Disco("Disco 4", 2010, copiasDeDiscos4);
        List<Disco> discosDeBanda2 = new ArrayList<>();
        discosDeBanda2.add(disco4);
        List<CopiasDeDisco> copiasDeDiscos5 = new ArrayList<>();
        copiasDeDiscos5.add(new CopiasDeDisco(1900, Pais.BRASIL));
        Disco disco5 = new Disco("Disco 5", 2011, copiasDeDiscos5);
        Banda banda2 = new Banda("Los Mosqueteros", discosDeBanda2, 100000, Pais.ARGENTINA, GeneroMusical.HEAVYMETAL);
        Recital recital1 = new Recital("Recital 1", anfiteatro1, 2000000, 300,3000);
        ciudad1.organizarEvento(recital1);
        cargaDeBandas.cargarBandaAEvento(banda1, disco3, 200, recital1);
        cargaDeBandas.cargarBandaAEvento(banda2, disco5, 200, recital1);
        Assertions.assertTrue(recital1.generosMusicalesQueIncluyeElEvento().contains(GeneroMusical.ROCKALTERNATIVO));
        Assertions.assertFalse(recital1.generosMusicalesQueIncluyeElEvento().contains(GeneroMusical.HEAVYMETAL));

        // PUNTO 7
        Sede estadio1 = new Estadio("Estadio 1", Pais.ARGENTINA,100000,new CostoPorFraccion(200.00),1000.00);
        Assertions.assertEquals(anfiteatro1.costoDeAlquilerParaEvento(recital1),4000.00);
        Assertions.assertEquals(estadio1.costoDeAlquilerParaEvento(recital1),1000.00);

        // PUNTO 8
        Assertions.assertEquals(cenashow1.esEconomicamenteFactible(), true);
        Assertions.assertEquals(cenashow2.esEconomicamenteFactible(), true);
        Assertions.assertEquals(recital1.esEconomicamenteFactible(), true);

        // PUNTO 9
        Assertions.assertEquals(cenashow1.sePuedeHacerEnLaSede(anfiteatro1),true);
        Assertions.assertEquals(cenashow1.sePuedeHacerEnLaSede(estadio1),false);
        Assertions.assertEquals(recital1.sePuedeHacerEnLaSede(anfiteatro1),true);
        Assertions.assertEquals(recital1.sePuedeHacerEnLaSede(estadio1),true);

        // PUNTO 10
        Assertions.assertEquals(cenashow1.indiceDeExitoPotencial(),29401);
        Assertions.assertEquals(cenashow2.indiceDeExitoPotencial(),0);
        Assertions.assertEquals(recital1.indiceDeExitoPotencial(),29401);

        // PUNTO 11
        Assertions.assertFalse(recital1.bandasExtranjeras().contains(banda1));
    }
}