package Test;

import Imperio.*;
import junit.framework.Assert;
import junit.framework.TestCase;

public class Test extends TestCase {

    public void testCantidadDeCiudadanos() {
        Imperio imperio = new Imperio();
        Aldeano aldeano1 = new Aldeano("Carlos",203,true);
        Aldeano aldeano2 = new Aldeano("Jorge",204,false);
        Cargo capitan = new Cargo("Capitan");
        Gobernante gobernante1 = new Gobernante("Sergio",205,capitan);
        Milicia milicia1 = new Milicia("Cacho",206);
        imperio.agregarAldeano(aldeano1);
        imperio.agregarAldeano(aldeano2);
        imperio.agregarGobernante(gobernante1);
        imperio.agregarMilicia(milicia1);
        assertEquals(Integer.valueOf(4),imperio.ciudadanosTotales());
    }

    public void testPoderDeAtaque1() {
        Imperio imperio = new Imperio();
        Aldeano aldeano1 = new Aldeano("Carlos",203,true);
        Aldeano aldeano2 = new Aldeano("Jorge",204,false);
        Cargo capitan = new Cargo("Capitan");
        Gobernante gobernante1 = new Gobernante("Sergio",205,capitan);
        Milicia milicia1 = new Milicia("Pepe",206);
        imperio.agregarAldeano(aldeano1);
        imperio.agregarAldeano(aldeano2);
        imperio.agregarGobernante(gobernante1);
        imperio.agregarMilicia(milicia1);
        assertEquals(Double.valueOf(50.00),imperio.poderDeAtaque());
    }

    public void testPoderDeAtaque2() {
        Imperio imperio = new Imperio();
        Aldeano aldeano1 = new Aldeano("Carlos",203,true);
        Aldeano aldeano2 = new Aldeano("Jorge",204,false);
        Cargo capitan = new Cargo("Capitan");
        Gobernante gobernante1 = new Gobernante("Sergio",205,capitan);
        Milicia milicia1 = new Milicia("Pepe",206);
        imperio.agregarAldeano(aldeano1);
        imperio.agregarAldeano(aldeano2);
        imperio.agregarGobernante(gobernante1);
        imperio.agregarMilicia(milicia1);
        Espada espada = new Espada();
        Armadura armadura = new Armadura();
        Escudo escudo = new Escudo();
        milicia1.agregarArmamento(espada);
        milicia1.agregarArmamento(escudo);
        milicia1.agregarArmamento(armadura);
        assertEquals(Double.valueOf(80.00),imperio.poderDeAtaque());
    }

    public void testPoderDeDefensa1() {
        Imperio imperio = new Imperio();
        Aldeano aldeano1 = new Aldeano("Carlos",203,true);
        Aldeano aldeano2 = new Aldeano("Jorge",204,false);
        Cargo capitan = new Cargo("Capitan");
        Gobernante gobernante1 = new Gobernante("Sergio",205,capitan);
        Milicia milicia1 = new Milicia("Cacho",206);
        imperio.agregarAldeano(aldeano1);
        imperio.agregarAldeano(aldeano2);
        imperio.agregarGobernante(gobernante1);
        imperio.agregarMilicia(milicia1);
        assertEquals(Double.valueOf(79.83000000000001),imperio.poderDeDefensa());
    }

    public void testPoderDeDefensa2() {
        Imperio imperio = new Imperio();
        Aldeano aldeano1 = new Aldeano("Carlos",203,true);
        Aldeano aldeano2 = new Aldeano("Jorge",204,false);
        Cargo capitan = new Cargo("Capitan");
        Gobernante gobernante1 = new Gobernante("Sergio",205,capitan);
        Milicia milicia1 = new Milicia("Pepe",206);
        imperio.agregarAldeano(aldeano1);
        imperio.agregarAldeano(aldeano2);
        imperio.agregarGobernante(gobernante1);
        imperio.agregarMilicia(milicia1);
        Espada espada = new Espada();
        Armadura armadura = new Armadura();
        Escudo escudo = new Escudo();
        milicia1.agregarArmamento(espada);
        milicia1.agregarArmamento(escudo);
        milicia1.agregarArmamento(armadura);
        assertEquals(Double.valueOf(109.74000000000001),imperio.poderDeDefensa());
    }

}
