package Imperio;

public interface Ciudadano {

    public Integer getLegajo();

    boolean noEstaElLegajoEnElImperio(Imperio imperio);

}
