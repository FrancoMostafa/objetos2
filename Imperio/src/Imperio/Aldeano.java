package Imperio;

import java.util.Collection;
import java.util.stream.Collectors;

public class Aldeano implements Ciudadano,Combatiente {

    private String nombre;
    private Integer legajo;
    private Boolean estaCansado;

    public Aldeano(String nombre, Integer legajo, Boolean estaCansado) {
        this.nombre = nombre;
        this.legajo = legajo;
        this.estaCansado = estaCansado;
    }

    public Integer getLegajo() {
        return legajo;
    }

    public Double poderDeDanio() {
        return SiluetaGuerrera.recibirDanio(this);
    }

    public Double danio() {
        if (estaCansado) {
            return 0.1;
        }
        else {
            return 0.2;
        }
    }

    public boolean noEstaElLegajoEnElImperio(Imperio imperio) {
        return !imperio.legajosTotales().contains(this.getLegajo());
    }
}
