package Imperio;

public class Gobernante implements Ciudadano {

    private String nombre;
    private Integer legajo;
    private Cargo cargo;

    public Gobernante(String nombre, Integer legajo, Cargo cargo) {
        this.nombre = nombre;
        this.legajo = legajo;
        this.cargo = cargo;
    }

    public Integer getLegajo() {
        return legajo;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public boolean noEstaElLegajoEnElImperio(Imperio imperio) {
        return !imperio.legajosTotales().contains(this.getLegajo());
    }

}
