package Imperio;

import javax.swing.plaf.synth.SynthDesktopIconUI;

public class SiluetaGuerrera {

    private static SiluetaGuerrera siluetaGuerrera;

    public static SiluetaGuerrera instance() {
        if (siluetaGuerrera == null) {
            siluetaGuerrera = new SiluetaGuerrera();
        }
        return siluetaGuerrera;
    }

    public static Double recibirDanio(Combatiente combatiente) {
        return 100 * combatiente.danio();
    }
}
