package Imperio;

public interface Combatiente {

    public Double poderDeDanio();

    public Double danio();

}
