package Imperio;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Imperio {

    private List<Aldeano> aldeanos;
    private List<Milicia> milicias;
    private List<Gobernante> gobernantes;
    private Estado estado;

    public Imperio() {
        this.aldeanos = new ArrayList<>();
        this.milicias = new ArrayList<>();
        this.gobernantes = new ArrayList<>();
        this.estado = new Pacifico();
    }

    public Integer ciudadanosTotales() {
        return this.aldeanos.size() + this.milicias.size() + this.gobernantes.size();
    }

    public List<Integer> legajosAldeanos() {
        return aldeanos.stream().map(a -> a.getLegajo()).collect(Collectors.toList());
    }

    public List<Integer> legajosMilicias() {
        return milicias.stream().map(m -> m.getLegajo()).collect(Collectors.toList());
    }

    public List<Integer> legajosGobernantes() {
        return gobernantes.stream().map(g -> g.getLegajo()).collect(Collectors.toList());
    }

    public List<Integer> legajosTotales() {
        List<Integer> lista = new ArrayList<>();
        lista.addAll(this.legajosAldeanos());
        lista.addAll(this.legajosMilicias());
        lista.addAll(this.legajosGobernantes());
        return lista;
    }

    public void agregarAldeano(Aldeano aldeano) {
        if (aldeano.noEstaElLegajoEnElImperio(this)) {
            aldeanos.add(aldeano);
        } else {
            System.out.println("El legajo" + aldeano.getLegajo() + "ya existe en el imperio");
        }
    }

    public void agregarMilicia(Milicia milicia) {
        if (milicia.noEstaElLegajoEnElImperio(this)) {
            milicias.add(milicia);
        } else {
            System.out.println("El legajo" + milicia.getLegajo() + "ya existe en el imperio");
        }
    }

    public void agregarGobernante(Gobernante gobernante) {
        if (gobernante.noEstaElLegajoEnElImperio(this)) {
            gobernantes.add(gobernante);
        } else {
            System.out.println("El legajo" + gobernante.getLegajo() + "ya existe en el imperio");
        }
    }

    public void entrarEnEstadoDeAlerta() {
        this.estado = new Alerta();
        this.estado.informarEstado();
    }

    public void entrarEnEstadoPacifico() {
        this.estado = new Pacifico();
        this.estado.informarEstado();
    }

    public Double poderDeAtaque() {
        return milicias.stream().mapToDouble(m -> m.poderDeDanio()).sum();
    }

    public Double poderDeDefensa() {
        return this.poderDeAtaque()
                +
                aldeanos.stream().mapToDouble(a -> a.poderDeDanio()).sum();
    }

}
