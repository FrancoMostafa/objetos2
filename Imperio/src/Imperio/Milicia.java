package Imperio;

import java.util.ArrayList;
import java.util.List;

public class Milicia implements Ciudadano,Combatiente {

    private String nombre;
    private Integer legajo;
    private List<Armamento> armamento;

    public Milicia(String nombre, Integer legajo) {
        this.nombre = nombre;
        this.legajo = legajo;
        this.armamento = new ArrayList<>();
    }

    public Integer getLegajo() {
        return legajo;
    }

    public Double poderDeDanio() {
        return SiluetaGuerrera.recibirDanio(this);
    }

    public Double danio() {
        return 0.5 + (this.getArmamento().size() * 0.1);
    }

    public void agregarArmamento(Armamento armamento) {
        this.armamento.add(armamento);
    }

    public List<Armamento> getArmamento() {
        return armamento;
    }

    public boolean noEstaElLegajoEnElImperio(Imperio imperio) {
        return !imperio.legajosTotales().contains(this.getLegajo());
    }

}
