package Preguntas;

import Respuestas.Respuesta;
import org.javatuples.Pair;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PreguntaNumerica extends Pregunta {

    private Pair<Integer,Integer> rangoDeAproximacion;

    private Integer puntajePorAproximacion;

    private Integer respuestaCorrecta;

    public PreguntaNumerica(String descripcion, Integer puntaje, Pair<Integer,Integer> rangoDeAproximacion, Integer puntajePorAproximacion, Integer respuesta) {
        super(descripcion,puntaje);
        this.rangoDeAproximacion = rangoDeAproximacion;
        this.puntajePorAproximacion = puntajePorAproximacion;
        this.respuestaCorrecta = respuesta;
    }

    public Integer getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public Pair<Integer, Integer> getRangoDeAproximacion() {
        return rangoDeAproximacion;
    }

    public Integer getPuntajePorAproximacion() {
        return puntajePorAproximacion;
    }

}
