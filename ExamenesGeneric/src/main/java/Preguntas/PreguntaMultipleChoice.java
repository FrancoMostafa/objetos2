package Preguntas;

import Respuestas.*;

import java.util.List;

public class PreguntaMultipleChoice extends Pregunta {

    private final Integer respuestaCorrecta;

    public PreguntaMultipleChoice(String descripcion, Integer puntaje, Integer respuestaCorrecta) {
        super(descripcion,puntaje);
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public Integer getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

}
