package Preguntas;

import Respuestas.Respuesta;

import java.util.List;

public class PreguntaSecuencia extends Pregunta {

    private List<Integer> secuenciaCorrecta;

    public PreguntaSecuencia(String descripcion, Integer puntaje, List<Integer> secuenciaCorrecta) {
        super(descripcion,puntaje);
        if (secuenciaCorrecta.size() != 5) {
            this.secuenciaCorrecta = null;
            System.out.println("La secuencia debe ser de 5 digitos");
        }
        this.secuenciaCorrecta = secuenciaCorrecta;
    }

    public List<Integer> getSecuenciaCorrecta() {
        return secuenciaCorrecta;
    }

}
