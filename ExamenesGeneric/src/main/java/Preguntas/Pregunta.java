package Preguntas;

import Respuestas.*;

public abstract class Pregunta {

    protected String descripcion;

    protected Integer puntaje;

    public Pregunta(String descripcion, Integer puntaje) {
        this.descripcion = descripcion;
        this.puntaje = puntaje;
    }

    public Integer getPuntaje() {
        return puntaje;
    }

}
