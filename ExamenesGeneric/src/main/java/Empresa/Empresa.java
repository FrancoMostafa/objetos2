package Empresa;

import Aspirante.Aspirante;
import Busquedas.Busqueda;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Empresa {

    private static Empresa empresa;

    private List<Aspirante> aspirantes;

    private List<Busqueda> busquedas;

    private Empresa(List<Busqueda> busquedas) {
        this.aspirantes = new ArrayList<>();
        this.busquedas = busquedas;
    }

    public static Empresa instance(List<Busqueda> busquedas) {
        if (empresa == null) {
            empresa = new Empresa(busquedas);
        }
        return empresa;
    }

    public void agregarBusqueda(Busqueda busqueda) {
        try {
            if (this.getBusquedas().contains(busqueda)) {
                throw new Exception();
            } else {
                busquedas.add(busqueda);
            }
        }
        catch (Exception e) {
            System.out.println("La busqueda ya esta en la empresa");
        }
    }

    public List<Busqueda> getBusquedas() {
        return busquedas;
    }

    public static Empresa getEmpresa() {
        return empresa;
    }

    public List<Aspirante> getAspirantes() {
        return aspirantes;
    }

    public void registrarAspirante(Aspirante aspirante) {
        try {
            if (this.getAspirantes().contains(aspirante)) {
                throw new Exception();
            } else {
                aspirantes.add(aspirante);
            }
        }
        catch (Exception e) {
            System.out.println("El aspirante ya esta en la empresa");
        }
    }

    public List<Aspirante> aspirantesAptos() {
        return aspirantes.stream().filter(a -> a.califica()).collect(Collectors.toList());
    }


}
