package Busquedas;

import Aspirante.Aspirante;
import Examen.Examen;

import java.util.List;

public class BusquedaDeExamenesAprobados extends Busqueda {

    private final Integer cantidadExamenesAprobadosNecesarios;

    public BusquedaDeExamenesAprobados(List<Examen> examenesRequeridos, Integer cantidadExamenesAprobadosNecesarios) {
        super(examenesRequeridos);
        this.cantidadExamenesAprobadosNecesarios = cantidadExamenesAprobadosNecesarios;
    }

    protected Boolean condicion(Aspirante aspirante) {
        return aspirante.cantidadDeExamenesAprobados() >= cantidadExamenesAprobadosNecesarios;
    }

}
