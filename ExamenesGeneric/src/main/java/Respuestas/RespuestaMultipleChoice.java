package Respuestas;

import Preguntas.Pregunta;
import Preguntas.PreguntaMultipleChoice;

public class RespuestaMultipleChoice extends Respuesta<PreguntaMultipleChoice> {

    private Integer valor;

    public RespuestaMultipleChoice(Integer valor, PreguntaMultipleChoice pregunta) {
        super(pregunta);
        this.valor = valor;
    }

    @Override
    public Integer resolverPregunta(PreguntaMultipleChoice pregunta) {
        if (valor.equals(pregunta.getRespuestaCorrecta())) {
            return pregunta.getPuntaje();
        } else {
            return 0;
        }
    }

}
