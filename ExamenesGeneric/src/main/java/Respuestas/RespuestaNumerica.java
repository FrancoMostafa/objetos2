package Respuestas;

import Preguntas.Pregunta;
import Preguntas.PreguntaNumerica;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RespuestaNumerica extends Respuesta<PreguntaNumerica> {

    private Integer valor;

    public RespuestaNumerica(Integer valor, PreguntaNumerica pregunta) {
        super(pregunta);
        this.valor = valor;
    }

    @Override
    public Integer resolverPregunta(PreguntaNumerica pregunta) {
        List<Integer> values = IntStream.range(pregunta.getRangoDeAproximacion().getValue0(), pregunta.getRangoDeAproximacion().getValue1()).boxed().collect(Collectors.toList());
        if (values.contains(valor)) {
            return pregunta.getPuntajePorAproximacion();
        }
        else if (valor.equals(pregunta.getRespuestaCorrecta())){
            return pregunta.getPuntaje();
        }
        else {
            return 0;
        }
    }

}

