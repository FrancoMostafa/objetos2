package Respuestas;

import Preguntas.Pregunta;

public abstract class Respuesta<T extends Pregunta> {

    protected T pregunta;

    public Respuesta(T pregunta) {
        this.pregunta = pregunta;
    }

    public abstract Integer resolverPregunta(T pregunta);

}

