package Respuestas;

import Preguntas.PreguntaSecuencia;

import java.util.List;

public class RespuestaSecuencia extends Respuesta<PreguntaSecuencia> {

    private List<Integer> valor;

    public RespuestaSecuencia(List<Integer> valor, PreguntaSecuencia pregunta) {
        super(pregunta);
        this.valor = valor;
    }

    public Integer resolverPregunta(PreguntaSecuencia pregunta) {
        if (this.esLaSecuenciaComun(pregunta.getSecuenciaCorrecta()) || this.esLaSecuenciaInversa(pregunta.getSecuenciaCorrecta())) {
            return pregunta.getPuntaje();
        }
        else {
            return 0;
        }
    }

    public Boolean esLaSecuenciaComun(List<Integer> secuencia) {
        return secuencia.get(0).equals(valor.get(0)) &&
                secuencia.get(1).equals(valor.get(1)) &&
                secuencia.get(2).equals(valor.get(2)) &&
                secuencia.get(3).equals(valor.get(3)) &&
                secuencia.get(4).equals(valor.get(4));
    }

    public Boolean esLaSecuenciaInversa(List<Integer> secuencia) {
        return secuencia.get(0).equals(valor.get(4)) &&
                secuencia.get(1).equals(valor.get(3)) &&
                secuencia.get(2).equals(valor.get(2)) &&
                secuencia.get(3).equals(valor.get(1)) &&
                secuencia.get(4).equals(valor.get(0));
    }

}
