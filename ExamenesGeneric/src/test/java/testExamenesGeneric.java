import Aspirante.Aspirante;
import Busquedas.Busqueda;
import Busquedas.BusquedaDeExamenesAprobados;
import Busquedas.BusquedaDePuntaje;
import Empresa.Empresa;
import Examen.Examen;
import Preguntas.Pregunta;
import Preguntas.PreguntaNumerica;
import Respuestas.Respuesta;
import Respuestas.RespuestaNumerica;
import org.javatuples.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class testExamenesGeneric {

    @DisplayName("Test Registro Aspirante")
    @Test
    void testRegistroAspirante() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante aspirante = new Aspirante("Carlos Perez");
        empresa.registrarAspirante(aspirante);
        Assertions.assertEquals(true, empresa.getAspirantes().contains(aspirante));
    }

    @DisplayName("Test Registro Resolucion Examen")
    @Test
    void testRegistroResolucionExamen() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante aspirante = new Aspirante("Carlos Perez");
        empresa.registrarAspirante(aspirante);
        PreguntaNumerica pregunta = new PreguntaNumerica
                ("Anio 2do mundial en mexico", 5, new Pair(1980, 1990), 2, 1986);
        RespuestaNumerica respuesta = new RespuestaNumerica(1986,pregunta);
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta = new ArrayList<>();
        respuestaYPregunta.add(new Pair(pregunta, respuesta));
        List<Pregunta> preguntasExamen = new ArrayList<>();
        preguntasExamen.add(pregunta);
        Examen examen = new Examen(preguntasExamen, new ArrayList<>(), 0, 2);
        aspirante.realizarExamen(examen, respuestaYPregunta);
        Assertions.assertEquals(true, aspirante.getResolucionesRealizadas().size() == 1);
    }

    @DisplayName("Test No se puede evaluar")
    @Test
    void testAspirantesQueNoSePuedenEvaluar() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante carlos = new Aspirante("Carlos Perez");
        Aspirante marcos = new Aspirante("Marcos Perez");
        Aspirante cacho = new Aspirante("Cacho Perez");
        empresa.registrarAspirante(carlos);
        empresa.registrarAspirante(marcos);
        empresa.registrarAspirante(cacho);
        PreguntaNumerica pregunta = new PreguntaNumerica
                ("Anio 2do mundial en mexico", 5, new Pair(1980, 1990), 2, 1986);
        RespuestaNumerica respuesta = new RespuestaNumerica(1986,pregunta);
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta = new ArrayList<>();
        respuestaYPregunta.add(new Pair(pregunta, respuesta));
        List<Pregunta> preguntasExamen = new ArrayList<>();
        preguntasExamen.add(pregunta);
        Examen examen = new Examen(preguntasExamen, new ArrayList<>(), 0, 2);
        carlos.realizarExamen(examen, respuestaYPregunta);
        List<Examen> examenDeBusqueda = new ArrayList<>();
        examenDeBusqueda.add(examen);
        BusquedaDeExamenesAprobados busquedaDeExamenesAprobados = new BusquedaDeExamenesAprobados(examenDeBusqueda, 1);
        Empresa.getEmpresa().agregarBusqueda(busquedaDeExamenesAprobados);
        marcos.aspirarABusqueda(busquedaDeExamenesAprobados);
        cacho.aspirarABusqueda(busquedaDeExamenesAprobados);
        carlos.aspirarABusqueda(busquedaDeExamenesAprobados);
        Assertions.assertTrue(busquedaDeExamenesAprobados.aspirantesQueNoSePuedenEvaluar().contains(marcos));
        Assertions.assertTrue(busquedaDeExamenesAprobados.aspirantesQueNoSePuedenEvaluar().contains(cacho));
    }

    @DisplayName("Test examenes que le faltan")
    @Test
    void testExamenesQueLeFaltan() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante carlos = new Aspirante("Carlos Perez");
        Aspirante marcos = new Aspirante("Marcos Perez");
        Aspirante cacho = new Aspirante("Cacho Perez");
        empresa.registrarAspirante(carlos);
        empresa.registrarAspirante(marcos);
        empresa.registrarAspirante(cacho);
        PreguntaNumerica pregunta = new PreguntaNumerica
                ("Anio 2do mundial en mexico", 5, new Pair(1980, 1990), 2, 1986);
        RespuestaNumerica respuesta = new RespuestaNumerica(1986,pregunta);
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta = new ArrayList<>();
        respuestaYPregunta.add(new Pair(pregunta, respuesta));
        List<Pregunta> preguntasExamen = new ArrayList<>();
        preguntasExamen.add(pregunta);
        Examen examen = new Examen(preguntasExamen, new ArrayList<>(), 0, 2);
        carlos.realizarExamen(examen, respuestaYPregunta);
        List<Examen> examenDeBusqueda = new ArrayList<>();
        examenDeBusqueda.add(examen);
        BusquedaDeExamenesAprobados busquedaDeExamenesAprobados = new BusquedaDeExamenesAprobados(examenDeBusqueda, 1);
        Empresa.getEmpresa().agregarBusqueda(busquedaDeExamenesAprobados);
        marcos.aspirarABusqueda(busquedaDeExamenesAprobados);
        cacho.aspirarABusqueda(busquedaDeExamenesAprobados);
        carlos.aspirarABusqueda(busquedaDeExamenesAprobados);
        Assertions.assertTrue(marcos.examenesQueLeFaltan().contains(examen));
        Assertions.assertTrue(cacho.examenesQueLeFaltan().contains(examen));
        Assertions.assertEquals(marcos.examenesQueLeFaltan().size(), 1);
        Assertions.assertEquals(cacho.examenesQueLeFaltan().size(), 1);
    }

    @DisplayName("Test calificados")
    @Test
    void testCalificados() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante carlos = new Aspirante("Carlos Perez");
        Aspirante marcos = new Aspirante("Marcos Perez");
        empresa.registrarAspirante(carlos);
        empresa.registrarAspirante(marcos);
        PreguntaNumerica pregunta = new PreguntaNumerica
                ("Anio 2do mundial en mexico", 5, new Pair(1980, 1990), 2, 1986);
        RespuestaNumerica respuesta = new RespuestaNumerica(1986,pregunta);
        RespuestaNumerica respuesta2 = new RespuestaNumerica(1990,pregunta);
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta = new ArrayList<>();
        respuestaYPregunta.add(new Pair(pregunta, respuesta));
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta2 = new ArrayList<>();
        respuestaYPregunta2.add(new Pair(pregunta, respuesta2));
        List<Pregunta> preguntasExamen = new ArrayList<>();
        preguntasExamen.add(pregunta);
        Examen examen = new Examen(preguntasExamen, new ArrayList<>(), 0, 0);
        carlos.realizarExamen(examen, respuestaYPregunta);
        marcos.realizarExamen(examen, respuestaYPregunta2);
        List<Examen> examenDeBusqueda = new ArrayList<>();
        examenDeBusqueda.add(examen);
        BusquedaDeExamenesAprobados busquedaDeExamenesAprobados = new BusquedaDeExamenesAprobados(examenDeBusqueda, 1);
        Empresa.getEmpresa().agregarBusqueda(busquedaDeExamenesAprobados);
        carlos.aspirarABusqueda(busquedaDeExamenesAprobados);
        marcos.aspirarABusqueda(busquedaDeExamenesAprobados);
        Assertions.assertSame(busquedaDeExamenesAprobados.rankingAspirantesCalificados().get(0), carlos);
        Assertions.assertSame(busquedaDeExamenesAprobados.rankingAspirantesCalificados().get(1), marcos);
    }

    @DisplayName("Test respuestas a un examen")
    @Test
    void testRespuestasAUnExamen() {
        Empresa empresa = Empresa.instance(new ArrayList<>());
        Aspirante carlos = new Aspirante("Carlos Perez");
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta = new ArrayList<>();
        Aspirante marcos = new Aspirante("Marcos Perez");
        empresa.registrarAspirante(carlos);
        empresa.registrarAspirante(marcos);
        PreguntaNumerica pregunta = new PreguntaNumerica
                ("Anio 2do mundial en mexico", 5, new Pair(1980, 1990), 2, 1986);
        RespuestaNumerica respuesta = new RespuestaNumerica(1986,pregunta);
        RespuestaNumerica respuesta2 = new RespuestaNumerica(1990,pregunta);
         respuestaYPregunta.add(new Pair(pregunta, respuesta));
        List<Pair<Pregunta, Respuesta>> respuestaYPregunta2 = new ArrayList<>();
        respuestaYPregunta2.add(new Pair(pregunta, respuesta2));
        List<Pregunta> preguntasExamen = new ArrayList<>();
        preguntasExamen.add(pregunta);
        Examen examen = new Examen(preguntasExamen, new ArrayList<>(), 0, 0);
        carlos.realizarExamen(examen, respuestaYPregunta);
        marcos.realizarExamen(examen, respuestaYPregunta2);
        List<Examen> examenDeBusqueda = new ArrayList<>();
        examenDeBusqueda.add(examen);
        BusquedaDeExamenesAprobados busquedaDeExamenesAprobados = new BusquedaDeExamenesAprobados(examenDeBusqueda, 1);
        Empresa.getEmpresa().agregarBusqueda(busquedaDeExamenesAprobados);
        carlos.aspirarABusqueda(busquedaDeExamenesAprobados);
        marcos.aspirarABusqueda(busquedaDeExamenesAprobados);
        Assertions.assertEquals(busquedaDeExamenesAprobados.respuestasAUnExamenDeAspirantes(examen).size(), 2);
    }
}