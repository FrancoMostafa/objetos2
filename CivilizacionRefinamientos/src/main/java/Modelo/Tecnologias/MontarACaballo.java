package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class MontarACaballo implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.MONTAR_A_CABALLO;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.RUEDA);
        lista.add(NombreTecnologia.GANADERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 20;}

}