package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class Ganaderia implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.GANADERIA;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        return new ArrayList<>();
    }

    public Integer culturaQueAporta() { return 10;}

}
