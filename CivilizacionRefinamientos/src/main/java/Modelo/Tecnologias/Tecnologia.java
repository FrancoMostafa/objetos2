package Modelo.Tecnologias;

import java.util.List;

public interface Tecnologia {

    public NombreTecnologia nombreTecnologia();

    public List<NombreTecnologia> tecnologiasRequeridas();

    public Integer culturaQueAporta();

}
