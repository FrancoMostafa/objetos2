package Modelo.Tecnologias;

public enum NombreTecnologia {
    GANADERIA,
    ARQUERIA,
    ALFARERIA,
    MINERIA,
    RUEDA,
    MONTAR_A_CABALLO,
    ESCRITURA,
    TRABAJO_DE_BRONCE,
    MATEMATICA,
    MONEDA;
}
