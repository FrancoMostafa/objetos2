package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class Rueda implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.RUEDA;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.GANADERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 20;}

}