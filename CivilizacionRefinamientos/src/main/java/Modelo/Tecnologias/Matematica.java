package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class Matematica implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.MATEMATICA;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.RUEDA);
        lista.add(NombreTecnologia.GANADERIA);
        lista.add(NombreTecnologia.ARQUERIA);
        lista.add(NombreTecnologia.TRABAJO_DE_BRONCE);
        lista.add(NombreTecnologia.MINERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 40;}

}