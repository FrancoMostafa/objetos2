package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class Escritura implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.ESCRITURA;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.ALFARERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 20;}

}