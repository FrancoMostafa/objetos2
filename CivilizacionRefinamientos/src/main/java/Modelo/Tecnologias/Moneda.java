package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class Moneda implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.MONEDA;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.MATEMATICA);
        lista.add(NombreTecnologia.RUEDA);
        lista.add(NombreTecnologia.GANADERIA);
        lista.add(NombreTecnologia.ARQUERIA);
        lista.add(NombreTecnologia.TRABAJO_DE_BRONCE);
        lista.add(NombreTecnologia.MINERIA);
        lista.add(NombreTecnologia.ALFARERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 30;}

}