package Modelo.Tecnologias;

import java.util.ArrayList;
import java.util.List;

public class TrabajoDeBronce implements Tecnologia {

    public NombreTecnologia nombreTecnologia() {
        return NombreTecnologia.TRABAJO_DE_BRONCE;
    }

    public List<NombreTecnologia> tecnologiasRequeridas() {
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.MINERIA);
        return lista;
    }

    public Integer culturaQueAporta() { return 20;}

}