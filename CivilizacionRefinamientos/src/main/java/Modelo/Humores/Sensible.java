package Modelo.Humores;

import Modelo.Ciudad.*;
import Modelo.Edificios_Unidades.EdificioCultural;
import Modelo.Edificios_Unidades.EdificioEconomico;
import Modelo.Edificios_Unidades.EdificioMilitar;

public class Sensible extends Humor {

    public Sensible() {
        super();
    }

    public Integer disconformidadPorHabitantes(Ciudad ciudad) {
        var valor = 0;
        if (ciudad.getHabitantes() < 200000) {
            valor = Math.round(ciudad.getHabitantes() / 20000);
        }
        else if (ciudad.getHabitantes() > 200000) {
            valor = 10 + Math.round((ciudad.getHabitantes() - 200000) / 40000);
        }
        return valor - ciudad.cantidadDeEdificiosCulturales();
    }

    public Integer disconformidadPorUnidadesMilitares(Ciudad ciudad) {
        return 0;
    }

    public NombreHumor nombreHumor() {
        return NombreHumor.SENSIBLE;
    }

    public void informarHumor() {
        System.out.println("Humor del imperio en Sensible");
    }

}
