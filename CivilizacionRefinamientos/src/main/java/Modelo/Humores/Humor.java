package Modelo.Humores;

import Modelo.Ciudad.*;

public abstract class Humor {

    public Integer disconformidad(Ciudad ciudad) {
        return this.disconformidadPorHabitantes(ciudad) +
                this.disconformidadPorUnidadesMilitares(ciudad);
    }

    protected abstract Integer disconformidadPorHabitantes(Ciudad ciudad);

    protected abstract Integer disconformidadPorUnidadesMilitares(Ciudad ciudad);

    public abstract NombreHumor nombreHumor();

    public abstract void informarHumor();

}
