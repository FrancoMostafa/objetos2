package Modelo.Humores;

import Modelo.Ciudad.*;

public class Pacifista extends Humor {

    public Pacifista() {
        super();
    }

    public Integer disconformidadPorHabitantes(Ciudad ciudad) {
        return Math.round(ciudad.getHabitantes() / 40000);
    }

    public Integer disconformidadPorUnidadesMilitares(Ciudad ciudad) {
        return ciudad.cantidadDeUnidadesMilitares();
    }

    public NombreHumor nombreHumor() {
        return NombreHumor.PACIFISTA;
    }

    public void informarHumor() {
        System.out.println("Humor del imperio en Pacifista");
    }

}
