package Modelo.Edificios_Unidades;

import Modelo.Ciudad.Ciudad;
import Modelo.Humores.NombreHumor;

public class EdificioEconomico implements Edificio {

    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;
    private Integer pepinesPorTurno;
    private Ciudad ciudad;

    public EdificioEconomico(Integer costoDeMatenimiento, Integer costoDeConstruccion, Integer pepinesPorTurno) {
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMatenimiento;
        this.pepinesPorTurno = pepinesPorTurno;
        this.ciudad = null;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Integer cultura() {
        if (pepinesPorTurno > 500) {
            return 3;
        }
        else {
            return 2;
        }
    }

    public Integer tranquilidad() {
        if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PACIFISTA)) {
            return this.tranquilidadEnHumorPacifista();
        }
        else if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PERSEGUIDA)) {
            return this.tranquilidadEnHumorPerseguida();
        }
        else {
            return this.tranquilidadEnHumorSensible();
        }
    }

    public Integer tranquilidadEnHumorPacifista() {
        return 12;
    }

    public Integer tranquilidadEnHumorPerseguida() {
        if (this.getPepinesPorTurno() <= 500) {
            return 10;
        }
        else {
            return 15;
        }
    }

    public Integer tranquilidadEnHumorSensible() {
        return 6;
    }

    public Integer getPepinesPorTurno() {
        return pepinesPorTurno;
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

    public void boostear() {
        this.pepinesPorTurno = pepinesPorTurno*2;
    }
}
