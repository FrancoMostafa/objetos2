package Modelo.Edificios_Unidades;

import Modelo.Ciudad.Ciudad;

public interface Edificio {

    public Integer cultura();

    public Integer tranquilidad();

    public Integer tranquilidadEnHumorPacifista();

    public Integer tranquilidadEnHumorPerseguida();

    public Integer tranquilidadEnHumorSensible();

    public void boostear();

    public Integer getCostoDeMantenimiento();

    public Integer getCostoDeConstruccion();

    public Ciudad getCiudad();

    public void setCiudad(Ciudad ciudad);

}
