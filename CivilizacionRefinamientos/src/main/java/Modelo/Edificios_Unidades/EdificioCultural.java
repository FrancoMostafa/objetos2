package Modelo.Edificios_Unidades;

import Modelo.Ciudad.Ciudad;
import Modelo.Humores.NombreHumor;

public class EdificioCultural implements Edificio {

    private Integer cultura;
    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;
    private Ciudad ciudad;

    public EdificioCultural(Integer costoDeMantenimiento, Integer costoDeConstruccion, Integer cultura) {
        this.cultura = cultura;
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMantenimiento;
        this.ciudad = null;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public Integer cultura() {
        return cultura;
    }

    public void setCultura(Integer cultura) {
        this.cultura = cultura;
    }

    public Integer getCultura() {return cultura;}

    public Integer tranquilidad() {
        if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PACIFISTA)) {
            return this.tranquilidadEnHumorPacifista();
        }
        else if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PERSEGUIDA)) {
            return this.tranquilidadEnHumorPerseguida();
        }
        else {
            return this.tranquilidadEnHumorSensible();
        }
    }

    public Integer tranquilidadEnHumorPacifista() {
        return 15;
    }

    public Integer tranquilidadEnHumorPerseguida() {
        return 1;
    }

    public Integer tranquilidadEnHumorSensible() {
        return cultura / ciudad.getImperio().getFactorTranquilidad();
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

    public void boostear() {
        this.cultura = cultura*2;
    }

}
