package Modelo.Edificios_Unidades;

import Modelo.Ciudad.*;
import Modelo.Humores.NombreHumor;

public class EdificioMilitar implements Edificio {

    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;
    private Integer potencia;
    private Ciudad ciudad;

    public EdificioMilitar(Integer costoDeMantenimiento, Integer costoDeConstruccion, Integer potencia) {
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMantenimiento;
        this.potencia = potencia;
        this.ciudad = null;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public Integer potencia() {
        return potencia;
    }

    public Integer cultura() {
        return 0;
    }

    public Integer tranquilidad() {
        if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PACIFISTA)) {
            return this.tranquilidadEnHumorPacifista();
        }
        else if (ciudad.getImperio().getHumor().nombreHumor().equals(NombreHumor.PERSEGUIDA)) {
            return this.tranquilidadEnHumorPerseguida();
        }
        else {
            return this.tranquilidadEnHumorSensible();
        }
    }

    public Integer tranquilidadEnHumorPacifista() {
        return -3;
    }

    public Integer tranquilidadEnHumorPerseguida() {
        return potencia * 3;
    }

    public Integer tranquilidadEnHumorSensible() {
        return 0;
    }

    public void crearUnidadMilitar(Ciudad ciudad) {
        UnidadMilitar unidad = new UnidadMilitar(potencia);
        ciudad.agregarUnidadMilitar(unidad);
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

    public void boostear() {
        this.potencia = potencia + 5;
    }

}
