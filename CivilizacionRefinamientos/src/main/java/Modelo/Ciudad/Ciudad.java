package Modelo.Ciudad;

import Modelo.Edificios_Unidades.*;
import Modelo.Humores.*;
import Modelo.Imperio.*;
import Modelo.Estados.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Ciudad {

    private Integer habitantes;
    private List<UnidadMilitar> unidadesMilitares;
    private List<EdificioCultural> edificiosCulturales;
    private List<EdificioEconomico> edificiosEconomicos;
    private List<EdificioMilitar> edificiosMilitares;
    private Imperio imperio;
    private Estado estado;

    public Ciudad(Integer habitantes) {
        this.habitantes = habitantes;
        this.edificiosCulturales = new ArrayList<EdificioCultural>();
        this.edificiosMilitares = new ArrayList<EdificioMilitar>();
        this.edificiosEconomicos = new ArrayList<EdificioEconomico>();
        this.unidadesMilitares = new ArrayList<UnidadMilitar>();
        this.imperio = null;
        this.estado = new Normal();
    }

    public Integer getHabitantes() {
        return habitantes;
    }

    public List<EdificioEconomico> getEdificiosEconomicos() {
        return edificiosEconomicos.stream().sorted(Comparator.comparing(EdificioEconomico::getPepinesPorTurno))
                .collect(Collectors.toList());
    }

    public List<EdificioCultural> getEdificiosCulturales() {
        return edificiosCulturales.stream().sorted(Comparator.comparing(EdificioCultural::cultura))
                .collect(Collectors.toList());
    }

    public List<EdificioMilitar> getEdificiosMilitares() {
        return edificiosMilitares;
    }

    public void setImperio(Imperio imperio) {
        this.imperio = imperio;
    }

    public Imperio getImperio() {
        return imperio;
    }

    public Integer cultura() {
        return edificiosCulturales.stream().mapToInt(e -> e.cultura()).sum()
                +
                edificiosEconomicos.stream().mapToInt(e -> e.cultura()).sum()
                +
                edificiosMilitares.stream().mapToInt(e -> e.cultura()).sum()
                +
                this.getImperio().culturaQueAportanTecnologias();
    }

    public Integer tranquilidad() {
        return edificiosCulturales.stream().mapToInt(e -> e.tranquilidad()).sum()
                +
                edificiosEconomicos.stream().mapToInt(e -> e.tranquilidad()).sum()
                +
                edificiosMilitares.stream().mapToInt(e -> e.tranquilidad()).sum();
    }

    public Integer disconformidad() {
        return this.imperio.getHumor().disconformidad(this);
    }

    public void agregarEdificioCulturar(EdificioCultural edificio) {
        edificiosCulturales.add(edificio);
        edificio.setCiudad(this);
    }

    public void agregarEdificioEconomico(EdificioEconomico edificio) {
        edificiosEconomicos.add(edificio);
        edificio.setCiudad(this);
    }

    public void agregarEdificioMilitar(EdificioMilitar edificio) {
        edificiosMilitares.add(edificio);
        edificio.setCiudad(this);
    }

    public void agregarUnidadMilitar(UnidadMilitar unidad) {
        unidadesMilitares.add(unidad);
    }

    public Integer cantidadDeUnidadesMilitares() {
        return unidadesMilitares.size();
    }

    public Integer cantidadDeEdificiosCulturales() {
        return this.edificiosMilitares.size();
    }

    public Integer cantidadDeEdificiosEconomicos() {
        return edificiosEconomicos.size();
    }

    public Integer cantidadDeEdificiosTotal() {
        return this.cantidadDeEdificiosCulturales() + this.cantidadDeEdificiosEconomicos() + this.cantidadDeUnidadesMilitares();
    }

    public EdificioCultural edificioCulturalMasValioso() {
        return this.edificiosCulturales.stream().max(Comparator.comparing(e -> e.cultura())).get();
    }

    public EdificioEconomico edificioEconomicoMasValioso() {
        return this.edificiosEconomicos.stream().max(Comparator.comparing(e -> e.getPepinesPorTurno())).get();
    }

    public EdificioMilitar edificioMilitarMasValioso() {
        return this.edificiosMilitares.stream().max(Comparator.comparing(e -> e.potencia())).get();
    }

    public Boolean esFeliz() {
        return estado.esFeliz(this);
    }

    public Integer ingresosPorTurno() {
        return edificiosEconomicos.stream().mapToInt(e -> e.getPepinesPorTurno()).sum();
    }

    public Integer egresosPorTurno() {
        return edificiosEconomicos.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum();
    }

    public Integer potenciaTotal() {
        return edificiosMilitares.stream().mapToInt(e -> e.potencia()).sum();
    }

    public void aumentarPoblacion(Integer porcentaje) {
        if (this.esFeliz()) {
            habitantes = habitantes + ((habitantes * porcentaje) / 100);
        }
    }

    public void restarCostoDeMantenimientoDeEdificiosAImperio() {
        var costoDeMantenimiento = edificiosEconomicos.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    + edificiosCulturales.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    + edificiosMilitares.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    ;
        this.imperio.restarATesoro(costoDeMantenimiento);
    }

    public void sumarGananciasAlTesoroDelImperio(Integer valor) {
        this.imperio.sumarATesoro(valor);
    }


    public void crearUnidadesMilitares() {
        this.edificiosMilitares.forEach(e -> e.crearUnidadMilitar(this));
    }

    public void boostearEdificiosCulturales(List <EdificioCultural> edificiosCulturales) {
        edificiosCulturales.forEach(e -> e.boostear());
    }

    public void boostearEdificiosEconomicos(List <EdificioEconomico> edificioEconomicos) {
        edificioEconomicos.forEach(e -> e.boostear());
    }

    public void boostearEdificioMilitar() {
        EdificioMilitar e = edificiosMilitares.stream().findAny().get();
        e.boostear();
    }

    public void boostearUnEdificioAlAzar(Integer randomNumber) {
        List<Edificio> edificios = new ArrayList<>();
        edificios.addAll(this.edificiosCulturales);
        edificios.addAll(this.edificiosEconomicos);
        edificios.addAll(this.edificiosMilitares);
        edificios.get(randomNumber).boostear();
    }

    public Integer cantidadDeEdificiosDestacados() {
        return edificiosCulturales.stream().filter(e -> e.cultura() > 100).collect(Collectors.toList()).size()
                +
                edificiosEconomicos.stream().filter(e -> e.getPepinesPorTurno() > 200).collect(Collectors.toList()).size();
    }

    public void potenciarse() {
        if (estado.nombreEstado().equals(NombreEstado.NORMAL)) {
            this.estado = new Exitada();
        }
        else if (estado.nombreEstado().equals(NombreEstado.CONVULSIONADA)) {
            this.estado = new Normal();
        }
    }

    public void complicarse() {
        if (estado.nombreEstado().equals(NombreEstado.NORMAL)) {
            this.estado = new Convulsionada();
        }
        else if (estado.nombreEstado().equals(NombreEstado.EXITADA)) {
            this.estado = new Normal();
        }
    }

    public void evolucion() {
        estado.evolucion(this);
    }

}