package Modelo.Estados;

import Modelo.Ciudad.Ciudad;
import Modelo.Edificios_Unidades.EdificioCultural;
import Modelo.Edificios_Unidades.EdificioEconomico;
import Modelo.Edificios_Unidades.EdificioMilitar;

import java.util.ArrayList;
import java.util.List;

public class Exitada implements Estado {

    public NombreEstado nombreEstado() {
        return NombreEstado.EXITADA;
    }

    public void evolucion(Ciudad ciudad) {
        ciudad.aumentarPoblacion(this.aumentoDePoblacion(ciudad));
        ciudad.sumarGananciasAlTesoroDelImperio(this.ganancias(ciudad));
        ciudad.boostearEdificiosCulturales(this.edificiosCulturalesABoostear(ciudad));
        ciudad.boostearEdificiosEconomicos(this.edificiosEconomicosABoostear(ciudad));
        ciudad.boostearEdificioMilitar();
    }

    public Integer aumentoDePoblacion(Ciudad ciudad) {
        if (ciudad.esFeliz()) {
            return 8;
        } else {
            return 5;
        }
    }

    public Integer ganancias(Ciudad ciudad) {
        return ciudad.getEdificiosEconomicos().stream().mapToInt(e -> e.getPepinesPorTurno()).sum() +
                ciudad.edificioCulturalMasValioso().cultura();
    }

    public List<EdificioCultural> edificiosCulturalesABoostear(Ciudad ciudad) {
        List<EdificioCultural> edificioCulturalesABoostear = new ArrayList<>();
        var edificioConMenosCultura1 = ciudad.getEdificiosCulturales().get(0);
        var edificioConMenosCultura2 = ciudad.getEdificiosCulturales().get(1);
        edificioCulturalesABoostear.add(edificioConMenosCultura1);
        edificioCulturalesABoostear.add(edificioConMenosCultura2);
        return edificioCulturalesABoostear;
    }

    public List<EdificioEconomico> edificiosEconomicosABoostear(Ciudad ciudad) {
        List<EdificioEconomico> edificiosEconomicosABoostear = new ArrayList<>();
        var edificioQueMenosProduce1 = ciudad.getEdificiosEconomicos().get(0);
        var edificioQueMenosProduce2 = ciudad.getEdificiosEconomicos().get(1);
        edificiosEconomicosABoostear.add(edificioQueMenosProduce1);
        edificiosEconomicosABoostear.add(edificioQueMenosProduce2);
        return edificiosEconomicosABoostear;
    }

    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.cultura() > 80;
    }

    public void informarEstado() {
        System.out.println("El estado actual de la ciudad es: Exitada");;
    }

}
