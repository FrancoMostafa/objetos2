package Modelo.Estados;

import Modelo.Ciudad.Ciudad;

public class Convulsionada extends Normal {

    public NombreEstado nombreEstado() {
        return NombreEstado.CONVULSIONADA;
    }

    public void evolucion(Ciudad ciudad) {
        ciudad.aumentarPoblacion(this.aumentoDePoblacion(ciudad));
        ciudad.restarCostoDeMantenimientoDeEdificiosAImperio();
        ciudad.crearUnidadesMilitares();
    }

    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.potenciaTotal() > 100;
    }

    public void informarEstado() {
        System.out.println("El estado actual de la ciudad es: Convulcionada");
    }

}
