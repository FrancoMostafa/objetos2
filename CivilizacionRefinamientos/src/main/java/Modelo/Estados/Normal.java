package Modelo.Estados;

import Modelo.Ciudad.Ciudad;
import Modelo.Edificios_Unidades.EdificioCultural;
import Modelo.Edificios_Unidades.EdificioEconomico;
import Modelo.Edificios_Unidades.EdificioMilitar;

import java.util.*;
import java.util.stream.Collectors;

public class Normal implements Estado {

    public NombreEstado nombreEstado() {return NombreEstado.NORMAL;}

    public void evolucion(Ciudad ciudad) {
        ciudad.aumentarPoblacion(this.aumentoDePoblacion(ciudad));
        ciudad.restarCostoDeMantenimientoDeEdificiosAImperio();
        ciudad.sumarGananciasAlTesoroDelImperio(this.ganancias(ciudad));
        ciudad.crearUnidadesMilitares();
        if (this.hayPosibilidadDeBoostearUnEdificio(ciudad)) {
            ciudad.boostearUnEdificioAlAzar(generarNumeroAlAzarDel0Al_(ciudad.cantidadDeEdificiosTotal() - 1));
        }
    }

    public Integer aumentoDePoblacion(Ciudad ciudad) {
        if (ciudad.esFeliz()) {
            return 5;
        }
        else {
            return 0;
        }
    }

    public Integer ganancias(Ciudad ciudad) {
        return ciudad.getEdificiosEconomicos().stream().mapToInt(e -> e.getPepinesPorTurno()).sum();
    }

    public Boolean hayPosibilidadDeBoostearUnEdificio(Ciudad ciudad) {
        var posibilidad = 20 + (ciudad.cantidadDeEdificiosDestacados() * 10);
        return posibilidad > 100 || posibilidad > generarNumeroAlAzarDel0Al_(99);
    }

    public static PrimitiveIterator.OfInt iterator0A_(Integer otroValor) {
        return new Random().ints(0,otroValor).iterator();
    }

    public static Integer generarNumeroAlAzarDel0Al_(Integer otroValor) {
        Integer randomNumberFor0To99 = iterator0A_(otroValor).nextInt();
        return randomNumberFor0To99;
    }

    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.tranquilidad() > ciudad.disconformidad();
    }

    public void informarEstado() {
        System.out.println("El estado actual de la ciudad es: Normal");;
    }

}