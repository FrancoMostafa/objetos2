package Modelo.Estados;
import Modelo.Ciudad.Ciudad;
import Modelo.Edificios_Unidades.EdificioCultural;
import Modelo.Edificios_Unidades.EdificioEconomico;

import java.util.List;

public interface Estado {

    NombreEstado nombreEstado();

    void evolucion(Ciudad ciudad);

    void informarEstado();

    Integer aumentoDePoblacion(Ciudad ciudad);

    Integer ganancias(Ciudad ciudad);

    Boolean esFeliz(Ciudad ciudad);

}
