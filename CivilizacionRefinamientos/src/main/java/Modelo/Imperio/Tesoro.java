package Modelo.Imperio;

public class Tesoro {

    private Integer pepines;

    public Tesoro(Integer pepines) {
        this.pepines = pepines;
    }

    public void sumar(Integer pepines) {
        this.pepines = this.pepines + pepines;
    }

    public void restar(Integer pepines) {
        this.pepines = this.pepines - pepines;
    }

    public Integer getPepines() {
        return pepines;
    }

}
