package Modelo.Imperio;

import Modelo.Ciudad.Ciudad;
import Modelo.Humores.*;
import Modelo.Tecnologias.NombreTecnologia;
import Modelo.Tecnologias.Tecnologia;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Imperio {

    private Tesoro tesoro;
    private Integer factorTranquilidad;
    private List<Ciudad> ciudades;
    private List<Tecnologia> tecnologias;
    private Humor humor;

    public Imperio(Tesoro tesoro, Integer factorTranquilidad) {
        this.tesoro = tesoro;
        this.factorTranquilidad = factorTranquilidad;
        this.ciudades = new ArrayList<Ciudad>();
        this.tecnologias = new ArrayList<Tecnologia>();
        this.humor = new Pacifista();
    }

    public Tesoro getTesoro() {
        return tesoro;
    }

    public Humor getHumor() {return humor;}

    public Integer getFactorTranquilidad() {return factorTranquilidad;}

    public Integer pepines() {
        return tesoro.getPepines();
    }

    public void agregarCiudad(Ciudad ciudad) {
        ciudades.add(ciudad);
        ciudad.setImperio(this);
    }

    public void agregarTecnologia(Tecnologia tecnologia) {
        if (puedeIncorporarTecnologia(tecnologia)) {
            tecnologias.add(tecnologia);
        }
    }

    public Boolean puedeIncorporarTecnologia(Tecnologia tecnologia) {
        return !tecnologias.contains(tecnologia) &&
                this.tecnologiasQueTiene().containsAll(this.tecnologiasRequeridasParaIncorporar(tecnologia));
    }

    public List<NombreTecnologia> tecnologiasQueTiene() {
        List<NombreTecnologia> lista = new ArrayList<>();
        tecnologias.forEach(t -> lista.add(t.nombreTecnologia()));
        return lista;
    }

    public List<NombreTecnologia> tecnologiasRequeridasParaIncorporar(Tecnologia tecnologia) {
        return tecnologia.tecnologiasRequeridas();
    }

    public List<NombreTecnologia> tecnologiasQueLeFaltanParaIncorporar(Tecnologia tecnologia) {
        return this.tecnologiasRequeridasParaIncorporar(tecnologia).stream()
                .filter(t -> !this.tecnologiasQueTiene().contains(t)).collect(Collectors.toList());
    }

    public Integer culturaQueAportanTecnologias() {
        return tecnologias.stream().mapToInt(t -> t.culturaQueAporta()).sum();
    }

    public List<Ciudad> listaDeCiudades() {
            return this.ciudades.stream()
                    .sorted(Comparator.comparing(Ciudad::cultura).reversed())
                    .collect(Collectors.toList());
    }

    public Integer ingresosPorTurno() {
        return ciudades.stream().mapToInt(c -> c.ingresosPorTurno()).sum();
    }

    public Integer egresosPorTurno() {
        return ciudades.stream().mapToInt(c -> c.egresosPorTurno()).sum();
    }

    public Integer potenciaTotal() {
        return ciudades.stream().mapToInt(c -> c.potenciaTotal()).sum();
    }

    public void sumarATesoro(Integer pepines) {
        tesoro.sumar(pepines);
    }

    public void restarATesoro(Integer pepines) {
        tesoro.restar(pepines);
    }

    public void cambiarHumorAPacifista() {
        this.humor = new Pacifista();
        humor.informarHumor();
    }

    public void cambiarHumorAPerseguida() {
        this.humor = new Perseguida();
        humor.informarHumor();
    }

    public void cambiarHumorASensible() {
        this.humor = new Sensible();
        humor.informarHumor();
    }

}
