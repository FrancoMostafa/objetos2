import Modelo.Ciudad.Ciudad;
import Modelo.Edificios_Unidades.EdificioCultural;
import Modelo.Edificios_Unidades.EdificioEconomico;
import Modelo.Edificios_Unidades.EdificioMilitar;
import Modelo.Imperio.Imperio;
import Modelo.Imperio.Tesoro;
import Modelo.Tecnologias.Ganaderia;
import Modelo.Tecnologias.Matematica;
import Modelo.Tecnologias.NombreTecnologia;
import Modelo.Tecnologias.Rueda;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class Test extends TestCase {

    public void testEvolucionCiudadExitada() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Ciudad ciudad = new Ciudad(100);
        imperio.agregarCiudad(ciudad);
        ciudad.potenciarse();
        EdificioEconomico edificioEconomico1 = new EdificioEconomico(100,100,100);
        EdificioCultural edificioCultural1 = new EdificioCultural(100,100,100);
        EdificioEconomico edificioEconomico2 = new EdificioEconomico(50,50,50);
        EdificioCultural edificioCultural2 = new EdificioCultural(50,50,50);
        EdificioMilitar edificioMilitar1 = new EdificioMilitar(80,80,80);
        ciudad.agregarEdificioEconomico(edificioEconomico1);
        ciudad.agregarEdificioEconomico(edificioEconomico2);
        ciudad.agregarEdificioCulturar(edificioCultural1);
        ciudad.agregarEdificioCulturar(edificioCultural2);
        ciudad.agregarEdificioMilitar(edificioMilitar1);
        ciudad.evolucion();
        assertEquals(Integer.valueOf(200),edificioEconomico1.getPepinesPorTurno());
        assertEquals(Integer.valueOf(100),edificioEconomico2.getPepinesPorTurno());
        assertEquals(Integer.valueOf(200),edificioCultural1.cultura());
        assertEquals(Integer.valueOf(100),edificioCultural2.cultura());
        assertEquals(Integer.valueOf(85),edificioMilitar1.potencia());
    }

    public void testFactorTranquilidad() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Ciudad ciudad = new Ciudad(100);
        imperio.agregarCiudad(ciudad);
        ciudad.potenciarse();
        EdificioCultural edificioCultural1 = new EdificioCultural(100,100,100);
        ciudad.agregarEdificioCulturar(edificioCultural1);
        imperio.cambiarHumorASensible();
        assertEquals(Integer.valueOf(2),edificioCultural1.tranquilidad());
    }

    public void testDisconformidad() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Ciudad ciudad = new Ciudad(40000);
        imperio.agregarCiudad(ciudad);
        ciudad.potenciarse();
        EdificioCultural edificioCultural1 = new EdificioCultural(100,100,100);
        ciudad.agregarEdificioCulturar(edificioCultural1);
        imperio.cambiarHumorAPerseguida();
        assertEquals(Integer.valueOf(13),ciudad.disconformidad());
    }

    public void testBoosteoAlAzar() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Ciudad ciudad = new Ciudad(100);
        imperio.agregarCiudad(ciudad);
        EdificioEconomico edificioEconomico1 = new EdificioEconomico(100,100,250);
        EdificioCultural edificioCultural1 = new EdificioCultural(100,100,200);
        EdificioEconomico edificioEconomico2 = new EdificioEconomico(50,50,250);
        EdificioCultural edificioCultural2 = new EdificioCultural(50,50,200);
        EdificioMilitar edificioMilitar1 = new EdificioMilitar(80,80,80);
        ciudad.agregarEdificioEconomico(edificioEconomico1);
        ciudad.agregarEdificioEconomico(edificioEconomico2);
        ciudad.agregarEdificioCulturar(edificioCultural1);
        ciudad.agregarEdificioCulturar(edificioCultural2);
        ciudad.agregarEdificioMilitar(edificioMilitar1);
        ciudad.boostearUnEdificioAlAzar(0);
        assertEquals(Integer.valueOf(400),edificioCultural1.cultura());
    }

    public void testTecnologiasQueLeFaltan() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Rueda rueda = new Rueda();
        Matematica matematica = new Matematica();
        Ganaderia ganaderia = new Ganaderia();
        imperio.agregarTecnologia(ganaderia);
        imperio.agregarTecnologia(rueda);
        List<NombreTecnologia> lista = new ArrayList<>();
        lista.add(NombreTecnologia.ARQUERIA);
        lista.add(NombreTecnologia.TRABAJO_DE_BRONCE);
        lista.add(NombreTecnologia.MINERIA);
        assertEquals(true,imperio.tecnologiasQueLeFaltanParaIncorporar(matematica).containsAll(lista));
    }

    public void testCulturaQueAportan() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro,50);
        Rueda rueda = new Rueda();
        Matematica matematica = new Matematica();
        Ganaderia ganaderia = new Ganaderia();
        imperio.agregarTecnologia(ganaderia);
        imperio.agregarTecnologia(rueda);
        Ciudad ciudad = new Ciudad(40000);
        imperio.agregarCiudad(ciudad);
        assertEquals(Integer.valueOf(30),ciudad.cultura());
    }

}

