package Test;

import Civilizacion.*;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class Test extends TestCase {

    public void testListaDeCiudadesDeUnImperios() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro);
        Ciudad ciudad1 = new Ciudad(30);
        Ciudad ciudad2 = new Ciudad(20);
        Ciudad ciudad3 = new Ciudad(10);
        EdificioCultural edificioCultural1 = new EdificioCultural(1000,1000,50);
        ciudad1.agregarEdificioCulturar(edificioCultural1);
        EdificioCultural edificioCultural2 = new EdificioCultural(1000,1000,40);
        ciudad2.agregarEdificioCulturar(edificioCultural2);
        EdificioCultural edificioCultural3 = new EdificioCultural(1000,1000,30);
        ciudad3.agregarEdificioCulturar(edificioCultural3);
        List<Ciudad> lista = new ArrayList<>();
        lista.add(ciudad1);
        lista.add(ciudad2);
        lista.add(ciudad3);
        imperio.agregarCiudad(ciudad1);
        imperio.agregarCiudad(ciudad2);
        imperio.agregarCiudad(ciudad3);
        assertEquals(lista,imperio.listaDeCiudades());
    }

    public void testEdificioMasValioso() {
        Ciudad ciudad1 = new Ciudad(30);
        EdificioCultural edificioCultural1 = new EdificioCultural(1000, 1000, 50);
        EdificioCultural edificioCultural2 = new EdificioCultural(1000, 1000, 40);
        EdificioEconomico edificioEconomico1 = new EdificioEconomico(1000, 1000, 100);
        EdificioEconomico edificioEconomico2 = new EdificioEconomico(1000, 1000, 200);
        EdificioMilitar edificioMilitar1 = new EdificioMilitar(1000, 1000, 200);
        EdificioMilitar edificioMilitar2 = new EdificioMilitar(1000, 1000, 300);
        ciudad1.agregarEdificioCulturar(edificioCultural1);
        ciudad1.agregarEdificioCulturar(edificioCultural2);
        ciudad1.agregarEdificioEconomico(edificioEconomico1);
        ciudad1.agregarEdificioEconomico(edificioEconomico2);
        ciudad1.agregarEdificioMilitar(edificioMilitar1);
        ciudad1.agregarEdificioMilitar(edificioMilitar2);
        assertEquals(edificioCultural1, ciudad1.edificioCulturalMasValioso());
        assertEquals(edificioEconomico2, ciudad1.edificioEconomicoMasValioso());
        assertEquals(edificioMilitar2, ciudad1.edificioMilitarMasValioso());
    }

    public void testEsFeliz() {
        Ciudad ciudad1 = new Ciudad(10);
        EdificioCultural edificioCultural1 = new EdificioCultural(1000, 1000, 500);
        ciudad1.cambiarHumorAPacifista();
        ciudad1.agregarEdificioCulturar(edificioCultural1);
        Juego juego = Juego.instance(10,new ArrayList<>());
        assertEquals(Boolean.valueOf(true),ciudad1.esFeliz());
    }

    public void testPuedeIncorporarTecnologia() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro);
        Tecnologia electricidad = new Tecnologia("Electricidad",new ArrayList<>());
        List<Tecnologia> requeridasComputadora = new ArrayList<>();
        requeridasComputadora.add(electricidad);
        Tecnologia computadora = new Tecnologia("Computadora",requeridasComputadora);
        imperio.agregarTecnologia(electricidad);
        assertEquals(Boolean.valueOf(true),imperio.puedeIncorporarTecnologia(computadora));
    }

    public void testTotalesDeImperio() {
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro);
        Ciudad ciudad1 = new Ciudad(30);
        imperio.agregarCiudad(ciudad1);
        EdificioEconomico edificioEconomico1 = new EdificioEconomico(1000, 1000, 100);
        EdificioEconomico edificioEconomico2 = new EdificioEconomico(1000, 1000, 200);
        ciudad1.agregarEdificioEconomico(edificioEconomico1);
        ciudad1.agregarEdificioEconomico(edificioEconomico2);
        assertEquals(Integer.valueOf(300),imperio.ingresosPorTurno());
        assertEquals(Integer.valueOf(2000),imperio.egresosPorTurno());
        assertEquals(Integer.valueOf(0),imperio.potenciaTotal());
    }

    public void testEvolucionPorTurnoParaCiudad() {
        Ciudad ciudad1 = new Ciudad(100);
        EdificioEconomico edificioEconomico1 = new EdificioEconomico(1000, 1000, 100);
        EdificioEconomico edificioEconomico2 = new EdificioEconomico(1000, 1000, 250);
        EdificioMilitar edificioMilitar1 = new EdificioMilitar(0, 0, 200);
        EdificioCultural edificioCultural1 = new EdificioCultural(0, 0, 110);
        ciudad1.agregarEdificioEconomico(edificioEconomico1);
        ciudad1.agregarEdificioEconomico(edificioEconomico2);
        ciudad1.agregarEdificioMilitar(edificioMilitar1);
        ciudad1.agregarEdificioCulturar(edificioCultural1);
        Tesoro tesoro = new Tesoro(50000);
        Imperio imperio = new Imperio(tesoro);
        imperio.agregarCiudad(ciudad1);
        Juego juego = Juego.instance(100, new ArrayList<>());
        ciudad1.evolucion();
        assertEquals(Integer.valueOf(200),ciudad1.potenciaTotal());
        assertEquals(Integer.valueOf(48350),imperio.pepines());
        assertEquals(Integer.valueOf(220),edificioCultural1.cultura());
        assertEquals(Integer.valueOf(200),edificioEconomico1.getPepinesPorTurno());

    }

}
