package Civilizacion;

public class EdificioCultural {

    private Integer cultura;
    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;

    public EdificioCultural(Integer costoDeMantenimiento, Integer costoDeConstruccion, Integer cultura) {
        this.cultura = cultura;
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMantenimiento;
    }


    public Integer cultura() {
        return cultura;
    }

    public void setCultura(Integer cultura) {
        this.cultura = cultura;
    }

    public Integer getCultura() {return cultura;}

    public Integer tranquilidad() {
        return cultura / Juego.getJuego().getFactorUnico();
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

}
