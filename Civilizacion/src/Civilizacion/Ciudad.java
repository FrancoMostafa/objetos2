package Civilizacion;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Ciudad {

    private Integer habitantes;
    private List<UnidadMilitar> unidadesMilitares;
    private List<EdificioCultural> edificiosCulturales;
    private List<EdificioEconomico> edificiosEconomicos;
    private List<EdificioMilitar> edificiosMilitares;
    private Humor humor;
    private Imperio imperio;

    public Ciudad(Integer habitantes) {
        this.habitantes = habitantes;
        this.edificiosCulturales = new ArrayList<EdificioCultural>();
        this.edificiosMilitares = new ArrayList<EdificioMilitar>();
        this.edificiosEconomicos = new ArrayList<EdificioEconomico>();
        this.unidadesMilitares = new ArrayList<UnidadMilitar>();
        this.humor = new Pacifista();
        this.imperio = null;
    }

    public Integer getHabitantes() {
        return habitantes;
    }

    public Humor getHumor() {
        return humor;
    }

    public void cambiarHumorAPacifista() {
        this.humor = new Pacifista();
    }

    public void cambiarHumorAPerseguida() {
        this.humor = new Perseguida();
    }

    public void cambiarHumorASensible() {
        this.humor = new Sensible();
    }

    public Integer cultura() {
        return edificiosCulturales.stream().mapToInt(e -> e.cultura()).sum()
                +
                edificiosEconomicos.stream().mapToInt(e -> e.cultura()).sum()
                +
                edificiosMilitares.stream().mapToInt(e -> e.cultura()).sum();
    }

    public Integer tranquilidad() {
        return edificiosCulturales.stream().mapToInt(e -> e.tranquilidad()).sum()
                +
                edificiosEconomicos.stream().mapToInt(e -> e.tranquilidad()).sum()
                +
                edificiosMilitares.stream().mapToInt(e -> e.tranquilidad()).sum();
    }

    public Integer disconformidad() {
        return humor.disconformidad(this);
    }

    public void agregarEdificioCulturar(EdificioCultural edificio) {
        edificiosCulturales.add(edificio);
    }

    public void agregarEdificioEconomico(EdificioEconomico edificio) {
        edificiosEconomicos.add(edificio);
    }

    public void agregarEdificioMilitar(EdificioMilitar edificio) {
        edificiosMilitares.add(edificio);
    }

    public void agregarUnidadMilitar(UnidadMilitar unidad) {
        unidadesMilitares.add(unidad);
    }

    public Integer cantidadDeUnidadesMilitares() {
        return unidadesMilitares.size();
    }

    public Integer cantidadDeEdificiosCulturales() {
        return this.edificiosMilitares.size();
    }

    public void setImperio(Imperio imperio) {
        this.imperio = imperio;
    }

    public EdificioCultural edificioCulturalMasValioso() {
        return this.edificiosCulturales.stream().max(Comparator.comparing(e -> e.cultura())).get();
    }

    public EdificioEconomico edificioEconomicoMasValioso() {
        return this.edificiosEconomicos.stream().max(Comparator.comparing(e -> e.getPepinesPorTurno())).get();
    }

    public EdificioMilitar edificioMilitarMasValioso() {
        return this.edificiosMilitares.stream().max(Comparator.comparing(e -> e.potencia())).get();
    }

    public Boolean esFeliz() {
        return this.tranquilidad() > this.disconformidad();
    }

    public Integer ingresosPorTurno() {
        return edificiosEconomicos.stream().mapToInt(e -> e.getPepinesPorTurno()).sum();
    }

    public Integer egresosPorTurno() {
        return edificiosEconomicos.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum();
    }

    public Integer potenciaTotal() {
        return edificiosMilitares.stream().mapToInt(e -> e.potencia()).sum();
    }

    public void evolucion() {
        this.aumentarPoblacion();
        this.restarCostoDeMantenimientoDeEdificiosAImperio();
        this.sumarGananciasAlTesoroDelImperio();
        this.crearUnidadesMilitares();
        this.boostearEdificioCultural();
        this.boostearEdificioEconomico();
    }

    public void aumentarPoblacion() {
        if (this.esFeliz()) {
            habitantes = habitantes + ((habitantes * 5) / 100);
        }
    }

    public void restarCostoDeMantenimientoDeEdificiosAImperio() {
        var costoDeMantenimiento = edificiosEconomicos.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    + edificiosCulturales.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    + edificiosMilitares.stream().mapToInt(e -> e.getCostoDeMantenimiento()).sum()
                                    ;
        this.imperio.restarATesoro(costoDeMantenimiento);
    }

    public void sumarGananciasAlTesoroDelImperio() {
        var ganancias = edificiosEconomicos.stream().mapToInt(e -> e.getPepinesPorTurno()).sum();
        this.imperio.sumarATesoro(ganancias);
    }

    public void crearUnidadesMilitares() {
        this.edificiosMilitares.forEach(e -> e.crearUnidadMilitar(this));
    }

    public void boostearEdificioCultural() {
        if (this.edificiosCulturales.stream().filter(e -> e.cultura() > 100).collect(Collectors.toList()).size() >= 1) {
            var edificioConMenosCultura = this.edificiosCulturales.stream().min(Comparator.comparing(e -> e.cultura())).get();
            edificioConMenosCultura.setCultura(edificioConMenosCultura.getCultura() * 2);
        }
    }

    public void boostearEdificioEconomico() {
        if (this.edificiosEconomicos.stream().filter(e -> e.getPepinesPorTurno() > 200).collect(Collectors.toList()).size() >= 1) {
            var edificioQueMenosProduce = this.edificiosEconomicos.stream().min(Comparator.comparing(e -> e.getPepinesPorTurno())).get();
            edificioQueMenosProduce.setPepinesPorTurno(edificioQueMenosProduce.getPepinesPorTurno() * 2);
        }
    }

}