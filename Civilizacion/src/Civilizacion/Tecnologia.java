package Civilizacion;

import java.util.List;

public class Tecnologia {

    private String nombre;
    private List<Tecnologia> tecnologiasRequeridas;

    public Tecnologia(String nombre, List<Tecnologia> tecnologiasRequeridas) {
        this.nombre = nombre;
        this.tecnologiasRequeridas = tecnologiasRequeridas;
    }

    public String getNombre() {
        return nombre;
    }

    public List<Tecnologia> getTecnologiasRequeridas() {
        return tecnologiasRequeridas;
    }

}
