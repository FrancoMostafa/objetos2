package Civilizacion;

public class EdificioMilitar {

    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;
    private Integer potencia;

    public EdificioMilitar(Integer costoDeMantenimiento, Integer costoDeConstruccion, Integer potencia) {
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMantenimiento;
        this.potencia = potencia;
    }

    public Integer potencia() {
        return potencia;
    }

    public Integer cultura() {
        return 0;
    }

    public Integer tranquilidad() {
        return 1;
    }

    public void crearUnidadMilitar(Ciudad ciudad) {
        UnidadMilitar unidad = new UnidadMilitar(potencia);
        ciudad.agregarUnidadMilitar(unidad);
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

}
