package Civilizacion;

public abstract class Humor {

    protected Integer disconformidad(Ciudad ciudad) {
        return this.disconformidadPorHabitantes(ciudad) +
                this.disconformidadPorUnidadesMilitares(ciudad);
    }

    protected abstract Integer disconformidadPorHabitantes(Ciudad ciudad);

    protected abstract Integer disconformidadPorUnidadesMilitares(Ciudad ciudad);

}
