package Civilizacion;

public class Perseguida extends Humor {

    public Perseguida() {
        super();
    }

    public Integer disconformidadPorHabitantes(Ciudad ciudad) {
        if ((Math.round(ciudad.getHabitantes() / 40000)) > 3) {
            return Math.round(ciudad.getHabitantes() / 40000);
        }
        else {
            return 3;
        }
    }

    public Integer disconformidadPorUnidadesMilitares(Ciudad ciudad) {
        if (ciudad.cantidadDeUnidadesMilitares() == 0) {
            return 10;
        }
        else if (ciudad.cantidadDeUnidadesMilitares() <= 3) {
            return 5;
        }
        else {
            return 0;
        }
    }

}
