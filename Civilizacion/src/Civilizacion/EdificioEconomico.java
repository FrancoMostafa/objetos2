package Civilizacion;

public class EdificioEconomico {

    private Integer costoDeMantenimiento;
    private Integer costoDeConstruccion;
    private Integer pepinesPorTurno;

    public EdificioEconomico(Integer costoDeMatenimiento, Integer costoDeConstruccion, Integer pepinesPorTurno) {
        this.costoDeConstruccion = costoDeConstruccion;
        this.costoDeMantenimiento = costoDeMatenimiento;
        this.pepinesPorTurno = pepinesPorTurno;
    }

    public Integer cultura() {
        if (pepinesPorTurno > 500) {
            return 3;
        }
        else {
            return 2;
        }
    }

    public Integer tranquilidad() {return 0;}

    public Integer getPepinesPorTurno() {
        return pepinesPorTurno;
    }

    public void setPepinesPorTurno(Integer pepines) {
        pepinesPorTurno = pepines;
    }

    public Integer getCostoDeMantenimiento() {
        return costoDeMantenimiento;
    }

    public Integer getCostoDeConstruccion() {
        return costoDeConstruccion;
    }

}
