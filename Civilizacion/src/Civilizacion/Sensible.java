package Civilizacion;

public class Sensible extends Humor {

    public Sensible() {
        super();
    }

    public Integer disconformidadPorHabitantes(Ciudad ciudad) {
        var valor = 0;
        if (ciudad.getHabitantes() < 200000) {
            valor = Math.round(ciudad.getHabitantes() / 20000);
        }
        else if (ciudad.getHabitantes() > 200000) {
            valor = 10 + Math.round((ciudad.getHabitantes() - 200000) / 40000);
        }
        return valor - ciudad.cantidadDeEdificiosCulturales();
    }

    public Integer disconformidadPorUnidadesMilitares(Ciudad ciudad) {
        return 0;
    }

}
