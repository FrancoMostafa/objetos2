package Civilizacion;

import java.util.*;
import java.util.stream.Collectors;

public class Imperio {

    private Tesoro tesoro;
    private List<Ciudad> ciudades;
    private List<Tecnologia> tecnologias;

    public Imperio(Tesoro tesoro) {
        this.tesoro = tesoro;
        this.ciudades = new ArrayList<Ciudad>();
        this.tecnologias = new ArrayList<Tecnologia>();
    }

    public Tesoro getTesoro() {
        return tesoro;
    }

    public Integer pepines() {
        return tesoro.getPepines();
    }

    public void agregarCiudad(Ciudad ciudad) {
        ciudades.add(ciudad);
        ciudad.setImperio(this);
    }

    public Boolean puedeIncorporarTecnologia(Tecnologia tecnologia) {
        return !tecnologias.contains(tecnologia) &&
                tecnologias.containsAll(tecnologia.getTecnologiasRequeridas());
    }

    public void agregarTecnologia(Tecnologia tecnologia) {
        if (puedeIncorporarTecnologia(tecnologia)) {
            tecnologias.add(tecnologia);
        }
    }

    public List<Ciudad> listaDeCiudades() {
            return this.ciudades.stream()
                    .sorted(Comparator.comparing(Ciudad::cultura).reversed())
                    .collect(Collectors.toList());
    }

    public Integer ingresosPorTurno() {
        return ciudades.stream().mapToInt(c -> c.ingresosPorTurno()).sum();
    }

    public Integer egresosPorTurno() {
        return ciudades.stream().mapToInt(c -> c.egresosPorTurno()).sum();
    }

    public Integer potenciaTotal() {
        return ciudades.stream().mapToInt(c -> c.potenciaTotal()).sum();
    }

    public void sumarATesoro(Integer pepines) {
        tesoro.sumar(pepines);
    }

    public void restarATesoro(Integer pepines) {
        tesoro.restar(pepines);
    }

}
