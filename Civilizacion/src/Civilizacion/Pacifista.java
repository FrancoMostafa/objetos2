package Civilizacion;

public class Pacifista extends Humor {

    public Pacifista() {
        super();
    }

    public Integer disconformidadPorHabitantes(Ciudad ciudad) {
        return Math.round(ciudad.getHabitantes() / 40000);
    }

    public Integer disconformidadPorUnidadesMilitares(Ciudad ciudad) {
        return ciudad.cantidadDeUnidadesMilitares();
    }

}
