package ToxiTaxi.ManejoDePedidos.Auto;

public class Auto {

    private String marca;

    private String modelo;

    private String patente;

    public Auto(String marca, String modelo, String patente) {
        this.marca = marca;
        this.modelo = modelo;
        this.patente = patente;
    }

}
