package ToxiTaxi.ManejoDePedidos.Pedidos;

import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import ToxiTaxi.ManejoDePedidos.Chofer.Chofer;

import java.util.Date;

public class Pedido {

    private String nombreDeCliente;

    private Integer telefonoDeCliente;

    private Direccion direccionDeComienzo;

    private Date fechaDePedido;

    private Integer numeroCorrelativo;

    private EstadoPedido estadoPedido;

    private Chofer choferAsignado;

    public Pedido(String nombreDeCliente, Integer telefonoDeCliente, Direccion direccionDeComienzo, String calleDeComienzo, Integer numeroDeComienzo, Date fechaDePedido) {
        this.nombreDeCliente = nombreDeCliente;
        this.telefonoDeCliente = telefonoDeCliente;
        this.direccionDeComienzo = direccionDeComienzo;
        this.fechaDePedido = fechaDePedido;
        this.estadoPedido = null;
        this.choferAsignado = null;
    }

    public void setNumeroCorrelativo(Integer numeroCorrelativo) {
        this.numeroCorrelativo = numeroCorrelativo;
    }

    public EstadoPedido getEstadoPedido() {
        return estadoPedido;
    }

    public Integer getNumeroCorrelativo() {
        return numeroCorrelativo;
    }

    public Date getFechaDePedido() {
        return fechaDePedido;
    }

    public Chofer getChoferAsignado() {
        return choferAsignado;
    }

    public void setChoferAsignado(Chofer chofer) {
        this.choferAsignado = chofer;
    }

    public void cambiarAEstadoPendiente() {
        this.estadoPedido = new EstadoPendiente();
    }

    public void cambiarAEstadoTomado() {
        this.estadoPedido = new EstadoTomado();
    }

    public void cambiarAEstadoDescartado() {
        this.estadoPedido = new EstadoDescartado();
    }

    public void cambiarAEstadoResuelto() {this.estadoPedido = new EstadoResuelto();}


}
