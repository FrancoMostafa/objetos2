package ToxiTaxi.ManejoDePedidos.Chofer;

import ART.ARTDeChofer.ARTDeChofer;
import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import ToxiTaxi.ManejoDePedidos.Auto.Auto;
import ToxiTaxi.ManejoDePedidos.Pedidos.Pedido;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Chofer {

    private String nombreYApellido;

    private Integer telefono;

    private EstadoChofer estadoChofer;

    private Auto auto;

    private Integer pedidosMaximosQuePuedeRecibirEnUnDia;

    private List<Pedido> pedidosEnTramite;

    private Integer montoRecaudado;

    // Aclaracion: pedidosRecibidosEnUnDia y fechaEnLaQueEstaTrabajando se modifican cuando se le asigna un pedido.
    // Se tiene en cuenta la logica de que cuando llega un pedido de fecha distinta cambio el dia.

    public Chofer(String nombreYApellido, Integer telefono, Auto auto, Integer pedidosMaximosQuePuedeRecibirEnUnDia) {
        this.nombreYApellido = nombreYApellido;
        this.telefono = telefono;
        this.auto = auto;
        this.estadoChofer = new EstadoLibre();
        this.pedidosEnTramite = new ArrayList<>();
        this.montoRecaudado = 0;
        this.pedidosMaximosQuePuedeRecibirEnUnDia = pedidosMaximosQuePuedeRecibirEnUnDia;
    }

    public Integer getMontoRecaudado() {
        return montoRecaudado;
    }

    public String getNombreYApellido() { return nombreYApellido; }

    public Integer getTelefono() { return telefono; }

    public void avisarQueEstaActivo() {
        try {
            if(this.estadoChofer.getClass().equals(EstadoInactivo.class)) {
                this.cambiarAEstadoLibre();
                System.out.println("El chofer " + this.nombreYApellido + " esta activo");
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("El chofer " + this.nombreYApellido + " esta inactivo");
        }
    }

    public void avisarQueDejaDeEstarActivo() {
        try {
            if(this.estadoChofer.getClass().equals(EstadoLibre.class) || this.estadoChofer.getClass().equals(EstadoOcupado.class)) {
                this.cambiarAEstadoInactivo();
                System.out.println("El chofer " + this.nombreYApellido + " ahora esta inactivo");
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("El chofer " + this.nombreYApellido + " no estaba activo");
        }
    }

    public void cambiarAEstadoLibre() { this.estadoChofer = new EstadoLibre(); }

    public void cambiarAEstadoInactivo() { this.estadoChofer = new EstadoInactivo();}

    public void cambiarAEstadoOcupado() {
        this.estadoChofer = new EstadoOcupado();
    }

    public List<Pedido> getPedidosEnTramite() {
        return pedidosEnTramite;
    }

    public void agregarPedidoEnTramite(Pedido pedido) {
        pedidosEnTramite.add(pedido);
    }

    public void informarQueSeLlegoADestino(Pedido pedido, Direccion direccion, Integer precio) {
        try {
            if (this.pedidosEnTramite.contains(pedido)) {
                pedido.cambiarAEstadoResuelto();
                montoRecaudado =+ precio;
                if(this.estadoChofer.getClass().equals(EstadoInactivo.class)) {
                    this.cambiarAEstadoLibre();
                }
                System.out.println("El chofer " + this.nombreYApellido + " termino el pedido " + pedido.getNumeroCorrelativo()
                        + " en la calle " + direccion.getCalle() + " numero " + direccion.getNumero().toString() + " con el cobro de " + precio.toString());
            } else {
                throw new Exception();
            }
        }
        catch(Exception e) {
            System.out.println("El chofer " + this.nombreYApellido + " no esta realizando este pedido");
        }
    }

    public Boolean puedeRecibirUnPedido() {
        return estadoChofer.getClass().equals(EstadoLibre.class) || estadoChofer.getClass().equals(EstadoOcupado.class)
                || !this.excedioElLimiteDePedidos();
    }

    public Boolean excedioElLimiteDePedidos() {
        return pedidosEnTramite.stream().allMatch(p -> Collections.frequency(pedidosEnTramite,p) <= pedidosMaximosQuePuedeRecibirEnUnDia);
    }

}