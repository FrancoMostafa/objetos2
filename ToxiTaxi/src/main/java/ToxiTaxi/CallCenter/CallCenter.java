package ToxiTaxi.CallCenter;

import ToxiTaxi.ManejoDePedidos.Chofer.Chofer;
import ToxiTaxi.ManejoDePedidos.Pedidos.EstadoPendiente;
import ToxiTaxi.ManejoDePedidos.Pedidos.EstadoResuelto;
import ToxiTaxi.ManejoDePedidos.Pedidos.EstadoTomado;
import ToxiTaxi.ManejoDePedidos.Pedidos.Pedido;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CallCenter {

    private static CallCenter instance;

    private List<Pedido> pedidos;

    private List<Chofer> choferes;

    private CallCenter() {
        this.pedidos = new ArrayList<>();
        this.choferes = new ArrayList<>();
    }

    public static CallCenter instance() {
        if (instance == null) {
            instance = new CallCenter();
        }
        return instance;
    }

    public static CallCenter getInstance() {
        return instance;
    }

    public List<Chofer> getChoferes() {
        return choferes;
    }

    public void entrarPedido(Pedido pedido, Integer numeroCorrelativo) {
        pedidos.add(pedido);
        pedido.setNumeroCorrelativo(numeroCorrelativo);
        pedido.cambiarAEstadoPendiente();
    }

    public void asignarPedidoAChofer(Pedido pedido, Chofer chofer, Integer tiempoDeArribo) {
        try {
            if (this.pedidos.contains(pedido) && pedido.getEstadoPedido().getClass().equals(EstadoPendiente.class) && !chofer.getPedidosEnTramite().contains(pedido)) {
                pedido.cambiarAEstadoTomado();
                chofer.cambiarAEstadoOcupado();
                chofer.agregarPedidoEnTramite(pedido);
                pedido.setChoferAsignado(chofer);
                System.out.println("El tiempo de arribo del pedido " + pedido.getNumeroCorrelativo() + " es de " + tiempoDeArribo.toString() + "minutos");
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("El pedido ya esta tomado o el pedido no esta en el call center o el chofer ya esta realizando este pedido");
        }
    }

    public void informarQueElPedidoQuedaDescartado(Pedido pedido) {
        try {
            if (this.pedidos.contains(pedido)) {
                pedido.cambiarAEstadoDescartado();
                System.out.println("El pedido " + pedido.getNumeroCorrelativo() + " quedo descartado");
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("El pedido ya esta tomado o el pedido no esta en el call center");
        }
    }

    public void listaDePedidosPendientes() {
        var lista = this.pedidos.stream().filter(p -> p.getEstadoPedido().getClass().equals(EstadoPendiente.class));
        System.out.println("CALL CENTER - PEDIDOS PENDIENTES:");
        lista.forEach(p -> System.out.println(p.getNumeroCorrelativo()));
    }

    public Integer cantidadDePedidosResueltosEnElDia(Date fecha) {
        return (int) pedidos.stream().filter(p -> p.getFechaDePedido().equals(fecha)).collect(Collectors.toList()).size();
    }

    public Integer cantidadDePedidosEnViaje() {
        return (int) this.pedidos.stream().filter(p -> p.getEstadoPedido().getClass().equals(EstadoTomado.class)).collect(Collectors.toList()).size();
    }

    public Integer importeRecaudadoDeChofer(Chofer chofer) {
        try {
            if (choferes.contains(chofer)) {
                return chofer.getMontoRecaudado();
            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("ERROR: El chofer no esta registrado en el call center");
            return 0;
        }
    }

    public Integer cantidadDePedidosAtendidosPorElChoferEnElDia(Chofer chofer, Date dia) {
        return (int) pedidos.stream().filter(p -> p.getChoferAsignado().equals(chofer) &&
                p.getFechaDePedido().equals(dia)).collect(Collectors.toList()).size();
    }

    public List<Chofer> choferesQuePuedenRecibirPedidos() {
        return choferes.stream().filter(Chofer::puedeRecibirUnPedido).collect(Collectors.toList());
    }

}