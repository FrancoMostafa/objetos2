package RequerimientosJudiciales.DependenciaJudicial;

import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import RequerimientosJudiciales.Casa.Casa;
import org.javatuples.Pair;
import org.javatuples.Tuple;

import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DependenciaJudicial {

    private List<Casa> casasVigiladas;

    private List<Pair<Date,Direccion>> direccionesALasQueLlegaron;

    private List<Pair<Date,Direccion>> direccionesEnLasQueSalieron;

    public static List<DependenciaJudicial> instances;

    public DependenciaJudicial() {
        this.casasVigiladas = new ArrayList<>();
        this.direccionesALasQueLlegaron = new ArrayList<>();
        this.direccionesEnLasQueSalieron = new ArrayList<>();
        if(instances == null) {
            instances = new ArrayList<>();
        }
        instances.add(this);
    }

    public List<Casa> getCasasVigiladas() {
        return casasVigiladas;
    }

    public void vigilarCasa(Casa casa) {
        if(!casasVigiladas.contains(casa)) {
            casasVigiladas.add(casa);
        }
    }

    public List<Direccion> direccionesVigiladas() {
        return casasVigiladas.stream().map(Casa::getDireccion).collect(Collectors.toList());
    }

    public void registrarQueLLegaronALaDireccionEnLaFecha(Direccion direccion, Date fecha) {
        try {
            if(this.direccionesVigiladas().contains(direccion)) {
                direccionesALasQueLlegaron.add(new Pair(direccion,fecha));
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La direccion " + direccion.toString() +  " no esta siendo vigilada por esta dependencia judicial");
        }
    }

    public void registrarQueSalieronDeLaDireccionEnLaFecha(Direccion direccion, Date fecha) {
        try {
            if(this.direccionesVigiladas().contains(direccion)) {
                direccionesEnLasQueSalieron.add(new Pair(direccion,fecha));
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("La direccion " + direccion.toString() +  " no esta siendo vigilada por esta dependencia judicial");
        }
    }

    public Integer cuantasVecesLlegaronALaDireccionEnLaFecha(Direccion direccion, Date fecha) {
        return (int) direccionesALasQueLlegaron.stream().filter(d -> d.getValue0().equals(direccion)
        && d.getValue1().equals(fecha)).collect(Collectors.toList()).size();
    }

    public Integer cuantasVecesSalieronDeLaDireccionEnLaFecha(Direccion direccion, Date fecha) {
        return (int) direccionesEnLasQueSalieron.stream().filter(d -> d.getValue0().equals(direccion)
                && d.getValue1().equals(fecha)).count();
    }

}
