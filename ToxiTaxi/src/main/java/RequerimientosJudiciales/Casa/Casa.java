package RequerimientosJudiciales.Casa;

import EmpresasConEmpleadosViajeros.Viaje.Direccion;

public class Casa {

    private Direccion direccion;

    public Casa(Direccion direccion) {
        this.direccion = direccion;
    }

    public Direccion getDireccion() {
        return direccion;
    }
}
