package EmpresasConEmpleadosViajeros.Empresa;

import EmpresasConEmpleadosViajeros.Empleado.Empleado;
import EmpresasConEmpleadosViajeros.Viaje.Direccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Empresa {

    private String nombreDeLaEmpresa;

    private List<Empleado> empleados;

    public static List<Empresa> instances;

    public Empresa(String nombreDeLaEmpresa) {
        this.nombreDeLaEmpresa = nombreDeLaEmpresa;
        this.empleados = new ArrayList<>();
        if(instances == null) {
            instances = new ArrayList<>();
        }
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void agregarEmpleado(Empleado empleado) {
        if(!this.empleados.contains(empleado)) {
            empleados.add(empleado);
        }
    }

    public Empleado empleadoConElNumero(Integer numero) {
        var empleado = empleados.stream().filter(e -> e.getTelefono().equals(numero)).collect(Collectors.toList());
        try {
            if (empleado.size() == 0) {
                throw new Exception();
            } else {
                return empleado.get(0);
            }
        }
        catch (Exception e) {
            System.out.println("No hay ningun empleado con el numero " + numero.toString());
            return new Empleado("",0);
        }
    }

    public List<Empleado> empleadosEnViaje() {
        return empleados.stream().filter(Empleado::estaEnViaje).collect(Collectors.toList());
    }

    public Boolean hayUnEmpleadoEnLaDireccion(Direccion direccion) {
        return empleados.stream().filter(e -> e.direccionActual().equals(direccion)).collect(Collectors.toList()).size() == 0;
    }

}
