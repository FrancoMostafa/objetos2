package EmpresasConEmpleadosViajeros.Empleado;

import EmpresasConEmpleadosViajeros.Empresa.Empresa;
import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import EmpresasConEmpleadosViajeros.Viaje.Viaje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Empleado {

    private String nombreYApellido;

    private Integer telefono;

    private Boolean estaEnViaje;

    private Viaje viajeActual;

    private Direccion direccionActual;

    public static List<Empleado> instances;

    public Empleado(String nombreYApellido, Integer telefono) {
        this.nombreYApellido = nombreYApellido;
        this.telefono = telefono;
        this.estaEnViaje = false;
        this.direccionActual = null;
        if(instances == null) {
            instances = new ArrayList<>();
        }
        instances.add(this);
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void iniciarViaje(Direccion direccion) {
        try {
            if(!estaEnViaje) {
                viajeActual = new Viaje(direccion);
                estaEnViaje = true;
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("El empleado ya esta en viaje");
        }
    }

    public void finalizarViaje() {
        try {
            if (estaEnViaje) {
                this.direccionActual = viajeActual.getDireccion();
                this.estaEnViaje = false;
            } else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Este empleado ya esta en viaje");
        }
    }

    public Boolean estaEnViaje() {
        return estaEnViaje;
    }

    public String direccionActual() {
        if (this.estaEnViaje()) {
            return "El empleado " + this.nombreYApellido + " esta en viaje";
        }
        else if (direccionActual == null) {
            return "El empleado " + this.nombreYApellido + "no empezo ningun viaje todavia";
        }
        else {
            return direccionActual.toString();
        }
    }

}
