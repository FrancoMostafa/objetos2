package EmpresasConEmpleadosViajeros.Viaje;

public class Viaje {

    private Direccion direccion;

    public Viaje(Direccion direccion) {
        this.direccion = direccion;
    }

    public Direccion getDireccion() {
        return direccion;
    }

}
