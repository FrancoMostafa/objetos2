package ART.ChoferConART;

import ToxiTaxi.ManejoDePedidos.Chofer.Chofer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChoferConART {

    private String nombreYApellidoChoferAsociado;

    private Integer telefonoChoferAsociado; // Usado para identificar chofer

    private Integer dniDeChofer;

    private Date fechaDeNacimientoDeChofer;

    private Integer viajesRealizados;

    public static List<ChoferConART> instances;

    public ChoferConART(String nombreYApellidoChoferAsociado, Integer telefonoChoferAsociado, Integer dniDeChofer, Date fechaDeNacimientoDeChofer) {
        this.nombreYApellidoChoferAsociado = nombreYApellidoChoferAsociado;
        this.telefonoChoferAsociado = telefonoChoferAsociado;
        this.dniDeChofer = dniDeChofer;
        this.fechaDeNacimientoDeChofer = fechaDeNacimientoDeChofer;
        this.viajesRealizados = 0;
        if(instances == null) {
            instances = new ArrayList<>();
        }
        instances.add(this);
    }

    public Integer getViajesRealizados() {
        return viajesRealizados;
    }

    public void setViajesRealizados(Integer viajes) {
        viajesRealizados += viajes;
    }

    public Integer getTelefonoChoferAsociado() {
        return telefonoChoferAsociado;
    }

    public void sumarViaje() {
        viajesRealizados += 1;
    }

}
