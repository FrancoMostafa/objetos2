package ART.ARTDeChofer;

import java.util.Date;

public class ARTLaPrimera extends ARTDeChofer {

    private static ARTLaPrimera instance;

    private ARTLaPrimera() {
        super();
        this.cantidadDeViajesParaRequerirChequeoMedico = 250;
    }

    public ARTLaPrimera instance() {
        if(instance == null) {
            instance = new ARTLaPrimera();
        }
        return instance;
    }

}
