package ART.ARTDeChofer;

import ART.ChoferConART.ChoferConART;
import ToxiTaxi.CallCenter.CallCenter;
import ToxiTaxi.ManejoDePedidos.Chofer.Chofer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public abstract class ARTDeChofer {

    protected List<ChoferConART> choferesRegistrados;

    protected Integer cantidadDeViajesParaRequerirChequeoMedico;

    public static List<ARTDeChofer> instances;

    public ARTDeChofer() {
        this.choferesRegistrados = new ArrayList<>();
        if(instances == null) {
            instances = new ArrayList<>();
        }
        instances.add(this);
    }

    public void registrarChofer(String nombreYApellidoChoferAsociado, Integer telefonoChoferAsociado, Integer dniDeChofer, Date fechaDeNacimientoDeChofer) {
        choferesRegistrados.add(new ChoferConART(nombreYApellidoChoferAsociado, telefonoChoferAsociado, dniDeChofer, fechaDeNacimientoDeChofer));
    }

    public List<ChoferConART> getChoferesRegistrados() {
        return choferesRegistrados;
    }

    public List<ChoferConART> choferesQueRequierenChequeoMedico() {
        return choferesRegistrados.stream().filter(c -> c.getViajesRealizados() > cantidadDeViajesParaRequerirChequeoMedico).collect(Collectors.toList());
    }

    public void realizarChequeoMedicoAChofer(ChoferConART choferConART) {
        try {
            if (this.choferesQueRequierenChequeoMedico().contains(choferConART)) {
                choferConART.setViajesRealizados(choferConART.getViajesRealizados() - cantidadDeViajesParaRequerirChequeoMedico);
            }
            else {
                throw new Exception();
            }
        }
        catch (Exception e) {
            System.out.println("Este chofer no requiere chequeo medico o este chofer no esta asociado a esta ART");
        }
    }

}
