package ART.ARTDeChofer;

import java.util.Date;

public class ARTLaSagaz extends ARTDeChofer {

    private static ARTLaSagaz instance;

    private ARTLaSagaz() {
        super();
        this.cantidadDeViajesParaRequerirChequeoMedico = 250;
    }

    public static ARTLaSagaz instance() {
        if(instance == null) {
            instance = new ARTLaSagaz();
        }
        return instance;
    }

}
