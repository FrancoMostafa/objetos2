package Eventos;

import ART.ARTDeChofer.ARTDeChofer;
import ART.ChoferConART.ChoferConART;
import EmpresasConEmpleadosViajeros.Empleado.Empleado;
import EmpresasConEmpleadosViajeros.Empresa.Empresa;
import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import RequerimientosJudiciales.DependenciaJudicial.DependenciaJudicial;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Evento {

    private Integer telefonoDeChofer;

    private Integer telefonoDeCliente;

    private Date fechaDeEvento;

    private Direccion direccionDeComienzoDeEvento;

    private Direccion direccionDeFinDeEvento;

    public Evento(Integer telefonoDeChofer, Integer telefonoDeCliente, Date fechaDeEvento, Direccion direccionDeComienzoDeEvento, Direccion direccionDeFinDeEvento) {
        this.telefonoDeChofer = telefonoDeChofer;
        this.telefonoDeCliente = telefonoDeCliente;
        this.fechaDeEvento = fechaDeEvento;
        this.direccionDeComienzoDeEvento = direccionDeComienzoDeEvento;
        this.direccionDeFinDeEvento = direccionDeFinDeEvento;
    }

    public void eventoCompleto() { // ASIGNACION Y FINALIZACION DE VIAJE
        asignarViaje();
        finalizarViaje();
    }

    public void asignarViaje() {
        this.ponerEmpleadoEnViaje(); // EMPRESA CON EMPLEADOS VIAJEROS
        this.registrarDireccionDeSalidaDeViaje(); // REQUERIMIENTOS JUDICIALES
    }

    public void finalizarViaje() {
        this.registrarDireccionDeLlegadaDeViaje(); // EMPRESA CON EMPLEADOS VIAJEROS
        this.registrarFinalDeViaje(); // REQUERIMIENTOS JUDICIALES
        this.registrarViajeEnART(); // ART
    }

    public void ponerEmpleadoEnViaje() {
        if(this.esteClienteEsUnEmpleado()) {
            this.empleadoConEsteNumero().iniciarViaje(direccionDeComienzoDeEvento);
        }
    }

    public void registrarFinalDeViaje() {
        if(this.esteClienteEsUnEmpleado()) {
            this.empleadoConEsteNumero().finalizarViaje();
        }
    }

    public Boolean esteClienteEsUnEmpleado() {
        return Empleado.instances.stream().map(Empleado::getTelefono).collect(Collectors.toList()).contains(this.telefonoDeCliente);
    }

    public Empleado empleadoConEsteNumero() {
        return Empleado.instances.stream().filter(e -> e.getTelefono().equals(telefonoDeCliente)).collect(Collectors.toList()).get(0);
    }

    public void registrarDireccionDeSalidaDeViaje() {
        if(estaDireccionEstaSiendoVigilada(direccionDeComienzoDeEvento)) {
            this.todasLasDependenciasJudicialesConEstaDireccionVigilada().forEach(d -> d.registrarQueSalieronDeLaDireccionEnLaFecha(direccionDeComienzoDeEvento,fechaDeEvento));
        }
    }

    public void registrarDireccionDeLlegadaDeViaje() {
        if(estaDireccionEstaSiendoVigilada(direccionDeComienzoDeEvento)) {
            this.todasLasDependenciasJudicialesConEstaDireccionVigilada().forEach(d -> d.registrarQueLLegaronALaDireccionEnLaFecha(direccionDeFinDeEvento,fechaDeEvento));
        }
    }

    public Boolean estaDireccionEstaSiendoVigilada(Direccion direccion) {
        return DependenciaJudicial.instances.stream().map(DependenciaJudicial::direccionesVigiladas).collect(Collectors.toList()).contains(direccion);
    }

    public List<DependenciaJudicial> todasLasDependenciasJudicialesConEstaDireccionVigilada() {
        return DependenciaJudicial.instances.stream().filter(d -> d.direccionesVigiladas().contains(direccionDeComienzoDeEvento)).collect(Collectors.toList());
    }

    public void registrarViajeEnART() {
        if(esteChoferTieneART()) {
            this.getChoferConART().sumarViaje();
        }
    }

    public Boolean esteChoferTieneART() {
        return this.todosLosChoferesConART().stream().map(ChoferConART::getTelefonoChoferAsociado).collect(Collectors.toList()).contains(telefonoDeChofer);
    }

    public List<ChoferConART> todosLosChoferesConART() {
        var listasDeChoferesConART = ARTDeChofer.instances.stream().map(ARTDeChofer::getChoferesRegistrados).collect(Collectors.toList());
        List<ChoferConART> choferesConART = new ArrayList<>();
        listasDeChoferesConART.forEach(choferesConART::addAll);
        return choferesConART;
    }

    public ChoferConART getChoferConART() {
        return ChoferConART.instances.stream().filter(c -> c.getTelefonoChoferAsociado().equals(telefonoDeChofer)).collect(Collectors.toList()).get(0);
    }

}
