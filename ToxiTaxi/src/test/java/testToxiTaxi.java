// TEST DE EVENTO COMPLETO DE TOXI TAXI

import ART.ARTDeChofer.ARTDeChofer;
import ART.ARTDeChofer.ARTLaSagaz;
import ART.ChoferConART.ChoferConART;
import EmpresasConEmpleadosViajeros.Empleado.Empleado;
import EmpresasConEmpleadosViajeros.Empresa.Empresa;
import EmpresasConEmpleadosViajeros.Viaje.Direccion;
import Eventos.Evento;
import RequerimientosJudiciales.Casa.Casa;
import RequerimientosJudiciales.DependenciaJudicial.DependenciaJudicial;
import ToxiTaxi.CallCenter.CallCenter;
import ToxiTaxi.ManejoDePedidos.Auto.Auto;
import ToxiTaxi.ManejoDePedidos.Chofer.Chofer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;

public class testToxiTaxi {

    @DisplayName("Test Toxi Taxi Evento")
    @Test
    void testToxiTaxiEvento() {

        // CREACION DE CLASES

        CallCenter callCenter = CallCenter.instance();
        Chofer chofer = new Chofer("Juan Perez", (int)2241605727L, new Auto("Chvrolet","80","AB216057"),10);
        Empresa empresa = new Empresa("La mejor empresa");
        Empleado empleado = new Empleado("Carlos Perez", (int)2241675020L);
        empresa.agregarEmpleado(empleado);
        Direccion direccionDeSalida = new Direccion("Falsa",123);
        Direccion direccionDeLlegada = new Direccion("verdadera",123);
        Casa casa1 = new Casa(direccionDeSalida);
        Casa casa2 = new Casa(direccionDeLlegada);
        DependenciaJudicial dependenciaJudicial = new DependenciaJudicial();
        dependenciaJudicial.vigilarCasa(casa1);
        dependenciaJudicial.vigilarCasa(casa2);
        ARTLaSagaz artLaSagaz = ARTLaSagaz.instance();
        artLaSagaz.registrarChofer("Juan Perez",(int)2241605727L,(int)41675755L,new Date(24/5/99));
        ChoferConART choferConART = artLaSagaz.getChoferesRegistrados().get(0);

        // EVENTO: ASIGNAR VIAJE

        Evento evento = new Evento((int)2241605727L,(int)2241675020L,new Date(12/7/21),direccionDeSalida,direccionDeLlegada);
        evento.asignarViaje();

        // EVALUAR RESULTADOS

        Assertions.assertEquals(empresa.hayUnEmpleadoEnLaDireccion(direccionDeSalida),true);
        Assertions.assertTrue(dependenciaJudicial.direccionesVigiladas().contains(direccionDeSalida));
        Assertions.assertTrue(dependenciaJudicial.direccionesVigiladas().contains(direccionDeLlegada));
        Assertions.assertEquals(artLaSagaz.choferesQueRequierenChequeoMedico().size(), 0);
        Assertions.assertEquals(choferConART.getViajesRealizados(),0);

        // EVENTO: FINALIZAR VIAJE

        evento.finalizarViaje();

        // EVALUAR RESULTADOS
        Assertions.assertEquals(empresa.hayUnEmpleadoEnLaDireccion(direccionDeLlegada),true);
        Assertions.assertEquals(choferConART.getViajesRealizados(),1);
    }

}
