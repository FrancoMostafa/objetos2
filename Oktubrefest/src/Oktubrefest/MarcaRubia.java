package Oktubrefest;

public class MarcaRubia extends Marca {

    private Double graduacion;

    public MarcaRubia(String marcaDeCerveza,Integer lupulo,String pais, Double graduacion) {
        super(marcaDeCerveza,lupulo,pais);
        this.graduacion = graduacion;
    }

    @Override
    public Double graduacion() {
        return graduacion;
    }

}
