package Oktubrefest.Test;

import Oktubrefest.*;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


public class Test extends TestCase {

    public void testPrueba() {
        Aleman pepe = new Aleman("Pepe","Carlos",23, true,10.00);
        Aleman juan = new Aleman("Juan","Carlos",20, true,5.00);
        Collection<Persona> alemanes = new ArrayList<>();
        alemanes.add(pepe);
        alemanes.add(juan);
        Integer sumaPeso = alemanes.stream().mapToInt(persona -> persona.getPeso()).sum();
        assertEquals(Integer.valueOf(43),sumaPeso);
    }

    public void test1() {
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        Jarra jarra = new Jarra(1,carpa);
        assertEquals(Double.valueOf(0.1),jarra.getAlcoholDeJarra());
    }

    public void test2() {
        Belga pepe = new Belga("Pepe","Carlos",23, true,10.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        MarcaRubia heineken = new MarcaRubia("Heineken",7,"Argentina",8.00);
        Carpa carpa2 = new Carpa(4,true,heineken);
        Jarra jarraDeQuilmes = new Jarra(1,carpa);
        Jarra jarraDeHeineken = new Jarra(1,carpa2);
        pepe.comprarJarra(jarraDeHeineken);
        pepe.comprarJarra(jarraDeQuilmes);
        assertEquals(Double.valueOf(0.18), pepe.cantidadDeAlcoholEnSangre());
    }

    public void test3() {
        Belga pepe = new Belga("Pepe","Carlos",23, true,1.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        MarcaRubia heineken = new MarcaRubia("Heineken",7,"Argentina",8.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        Carpa carpa2 = new Carpa(4,true,heineken);
        Jarra jarraDeQuilmes = new Jarra(1,carpa);
        Jarra jarraDeHeineken = new Jarra(1,carpa2);
        pepe.comprarJarra(jarraDeHeineken);
        pepe.comprarJarra(jarraDeQuilmes);
        assertEquals(Boolean.valueOf(true), pepe.estaEbria());
    }

    public void test4() {
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        Belga carlos = new Belga("Carlos", "Roque", 60, true, 1.00);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        assertEquals(Boolean.valueOf(true),marcos.quiereEntrar(carpa));
    }

    public void test5() {
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        Belga carlos = new Belga("Carlos", "Roque", 60, true, 1.00);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        assertEquals(Boolean.valueOf(true),carpa.dejaEntrar(marcos));
    }

    public void test6() {
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        Belga carlos = new Belga("Carlos", "Roque", 60, true, 1.00);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        assertEquals(Boolean.valueOf(true),marcos.puedeEntrar(carpa));
    }

    public void test7() {
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        Belga carlos = new Belga("Carlos", "Roque", 60, true, 1.00);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",10.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        marcos.entrar(carpa);
        pepe.entrar(carpa);
        carlos.entrar(carpa);
        assertEquals(Integer.valueOf(3),carpa.getCantidadDePersonas());
    }

    public void test8() {
        Belga pepe = new Belga("Pepe","Carlos",23, true,3.00);
        MarcaRubia quilmes = new MarcaRubia("Quilmes",10,"Argentina",1.00);
        Carpa carpa = new Carpa(4,true,quilmes);
        Jarra jarraDeQuilmes = new Jarra(1,carpa);
        pepe.comprarJarra(jarraDeQuilmes);
        pepe.entrar(carpa);
        assertEquals(Integer.valueOf(1),carpa.ebriosEmpedernidos());
    }

    public void test9() {
        MarcaRubia krombacher = new MarcaRubia("Krombacher",10,"Alemania",2.0);
        Carpa carpa = new Carpa(4,true,krombacher);
        Jarra jarraDeKrombacher1 = new Jarra(1,carpa);
        Jarra jarraDeKrombacher2 = new Jarra(1,carpa);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        marcos.comprarJarra(jarraDeKrombacher1);
        marcos.comprarJarra(jarraDeKrombacher2);
        assertEquals(Boolean.valueOf(true),marcos.esPatriota());
    }

    public void test10() {
        MarcaRubia krombacher = new MarcaRubia("Krombacher",10,"Alemania",2.0);
        MarcaRoja quilmesRed = new MarcaRoja("Quilmes",10,"Argentina");
        Carpa carpaAlemana = new Carpa(5,true,krombacher);
        Carpa carpaArgentina = new Carpa(5,true,quilmesRed);
        Jarra jarraDeKrombacher = new Jarra(1,carpaAlemana);
        Jarra jarraDeQuilmesRed = new Jarra(1,carpaArgentina);
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        pepe.comprarJarra(jarraDeKrombacher);
        pepe.comprarJarra(jarraDeQuilmesRed);
        List<Carpa> carpas = new ArrayList<Carpa>();
        carpas.add(carpaAlemana);
        carpas.add(carpaArgentina);
        assertTrue(pepe.carpasEnLasQueCompro().size() == carpas.size() &&
                pepe.carpasEnLasQueCompro().containsAll(carpas) && carpas.containsAll(pepe.carpasEnLasQueCompro()));
    }

    public void test11() {
        MarcaRubia krombacher = new MarcaRubia("Krombacher",10,"Alemania",2.0);
        MarcaRoja quilmesRed = new MarcaRoja("Quilmes",10,"Argentina");
        Carpa carpaAlemana = new Carpa(5,true,krombacher);
        Carpa carpaArgentina = new Carpa(5,true,quilmesRed);
        Jarra jarraDeKrombacher = new Jarra(1,carpaAlemana);
        Jarra jarraDeQuilmesRed = new Jarra(1,carpaArgentina);
        Belga pepe = new Belga("Pepe", "Carlos", 23, true, 1.00);
        pepe.comprarJarra(jarraDeKrombacher);
        pepe.comprarJarra(jarraDeQuilmesRed);
        Aleman marcos = new Aleman("Marcos","Juarez",60,true,2.00);
        marcos.comprarJarra(jarraDeKrombacher);
        marcos.comprarJarra(jarraDeQuilmesRed);
        assertTrue(marcos.fueCon(pepe));
    }

}
