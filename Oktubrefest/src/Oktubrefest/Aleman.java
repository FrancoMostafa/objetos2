package Oktubrefest;

import javax.swing.text.StyledEditorKit;
import java.util.List;

public class Aleman extends Persona {

    public Aleman(String nombre, String apellido, Integer peso, Boolean musica, Double aguante) {
        super(nombre, apellido,peso, musica, aguante);
    }

    @Override
    public Boolean quiereEntrar(Carpa unaCarpa) {
        return this.leGustaLaMarcaDeCerveza(unaCarpa.getMarcaQueVende())
                &&
                this.leGustaLaMusicaTradicional.equals(unaCarpa.getTieneBandaTradicional())
                &&
                (unaCarpa.getCantidadDePersonas()%2) == 0;
    }

    @Override
    public Boolean esPatriota() {
        return this.jarrasCompradas.stream().anyMatch(j -> j.getCarpa().getMarcaQueVende().getPais().equals("Alemania"));
    }

}
