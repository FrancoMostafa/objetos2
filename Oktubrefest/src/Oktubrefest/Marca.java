package Oktubrefest;

public class Marca {

    protected String marcaDeCerveza;
    protected Integer lupulo;
    protected String pais;
    protected Carpa carpa = null;

    public Marca(String marcaDeCerveza, Integer lupulo, String pais) {
        this.marcaDeCerveza = marcaDeCerveza;
        this.lupulo = lupulo;
        this.pais = pais;
    }

    public String getMarcaDeCerveza() {
        return marcaDeCerveza;
    }

    public void setMarcaDeCerveza(String marcaDeCerveza) {
        this.marcaDeCerveza = marcaDeCerveza;
    }

    public Integer getLupulo() {
        return lupulo;
    }

    public void setLupulo(Integer lupulo) {
        this.lupulo = lupulo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Double graduacion() {
        return 0.0;
    }

    public void setCarpa(Carpa unaCarpa) {
        this.carpa = unaCarpa;
    }

    public Carpa getCarpa() {return this.carpa;}

}
