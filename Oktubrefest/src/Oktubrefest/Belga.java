package Oktubrefest;

import java.util.List;

public class Belga extends Persona {

    public Belga(String nombre, String apellido, Integer peso, Boolean musica, Double aguante) {
        super(nombre, apellido, peso, musica, aguante);
    }

    @Override
    public Boolean leGustaLaMarcaDeCerveza(Marca unaMarca) {
        return unaMarca.getLupulo() > 4;
    }

    @Override
    public Boolean esPatriota() {
        return this.jarrasCompradas.stream().anyMatch(j -> j.getCarpa().getMarcaQueVende().getPais().equals("Belgica"));
    }
}
