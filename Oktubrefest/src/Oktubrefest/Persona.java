package Oktubrefest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Persona {

    protected String nombre;
    protected String apellido;
    protected Integer peso;
    protected Boolean leGustaLaMusicaTradicional;
    protected List<Jarra> jarrasCompradas;
    protected Double aguante;

    public Persona(String nombre, String apellido, Integer peso, Boolean musica, Double aguante) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.peso = peso;
        this.leGustaLaMusicaTradicional = musica;
        this.aguante = aguante;
        this.jarrasCompradas = new ArrayList<Jarra>();
    }

    public void comprarJarra(Jarra unaJarra) {
        jarrasCompradas.add(unaJarra);
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public List<Jarra> getJarrasCompradas() {
        return jarrasCompradas;
    }

    public void setJarrasCompradas(List<Jarra> jarrasCompradas) {
        this.jarrasCompradas = jarrasCompradas;
    }

    public Double getAguante() {
        return aguante;
    }

    public void setAguante(Double aguante) {
        this.aguante = aguante;
    }

    public Double cantidadDeAlcoholEnSangre() {
         return this.jarrasCompradas.stream().mapToDouble(jarra -> jarra.getAlcoholDeJarra()).sum();
    }

    public Boolean estaEbria() {
        return this.cantidadDeAlcoholEnSangre() * this.getPeso() > this.getAguante();
    }

    public Boolean leGustaLaMarcaDeCerveza(Marca unaMarca) {
        return true;
    }

    public void entrarACarpa(Carpa unaCarpa) {
        if (this.quiereEntrar(unaCarpa) && unaCarpa.dejaEntrar(this)) {
            unaCarpa.dejarEntrar(this);
        }
    }

    public Boolean quiereEntrar(Carpa unaCarpa) {
        return this.leGustaLaMarcaDeCerveza(unaCarpa.getMarcaQueVende())
                &&
                this.leGustaLaMusicaTradicional.equals(unaCarpa.getTieneBandaTradicional());
    }

    public Boolean puedeEntrar(Carpa unaCarpa) {
        return this.quiereEntrar(unaCarpa) && unaCarpa.dejaEntrar(this);
    }

    public void entrar(Carpa unaCarpa) {
        unaCarpa.dejarEntrar(this);
    }

    public Boolean susJarrasSonDeMasDeUnLitro() {
        return this.jarrasCompradas.stream().allMatch(jarra -> jarra.getCapacidadEnLitros() >= 1);

    }

    public Boolean esPatriota() {
        return true;
    }

    public List<Carpa> carpasEnLasQueCompro() {
        return this.jarrasCompradas.stream().map(j -> j.getCarpa()).distinct().collect(Collectors.toList());
    }


    public Boolean fueCon(Persona otraPersona) {
        return (this.carpasEnLasQueCompro().size() == otraPersona.carpasEnLasQueCompro().size() &&
                this.carpasEnLasQueCompro().containsAll(otraPersona.carpasEnLasQueCompro()) &&
                otraPersona.carpasEnLasQueCompro().containsAll(this.carpasEnLasQueCompro()));
    }

}
