package Oktubrefest;

public class Jarra {

    private Integer capacidadEnLitros;

    private Carpa carpa;

    public Jarra(Integer capacidadEnLitros, Carpa carpa) {
        this.capacidadEnLitros = capacidadEnLitros;
        this.carpa = carpa;
    }

    public Integer getCapacidadEnLitros() {
        return capacidadEnLitros;
    }

    public void setCapacidadEnLitros(Integer capacidadEnLitros) {
        this.capacidadEnLitros = capacidadEnLitros;
    }

    public Double getAlcoholDeJarra() {
        return (this.carpa.getMarcaQueVende().graduacion() * this.getCapacidadEnLitros()) / 100;
    }

    public Carpa getCarpa() {
        return carpa;
    }

    public void setCarpa(Carpa carpa) {
        this.carpa = carpa;
    }

}
