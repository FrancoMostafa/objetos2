package Oktubrefest;

public class MarcaNegra extends Marca  {

    public MarcaNegra(String marcaDeCerveza, Integer lupulo, String pais) {
        super(marcaDeCerveza,lupulo,pais);
    }

    protected static Double graduacionReglamentariaMundial = 5.00;

    public static Double getGraduacionReglamentariaMundial() {
        return graduacionReglamentariaMundial;
    }

    public static void setGraduacionReglamentariaMundial(Double graduacionReglamentariaMundial) {
        MarcaNegra.graduacionReglamentariaMundial = graduacionReglamentariaMundial;
    }

    @Override
    public Double graduacion() {
      return Double.valueOf(Math.min(getGraduacionReglamentariaMundial(),this.getLupulo() * 2));
    }
}
