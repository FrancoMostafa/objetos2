package Oktubrefest;

public class MarcaRoja extends MarcaNegra {

    public MarcaRoja(String marcaDeCerveza, Integer lupulo, String pais) {
        super(marcaDeCerveza, lupulo, pais);
    }

    @Override
    public Double graduacion() {
        return super.graduacion() * 1.25;
    }

}
