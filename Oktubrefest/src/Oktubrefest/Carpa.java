package Oktubrefest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Carpa {

    private Integer capacidadPermitida;
    private Integer cantidadDePersonas = 0;
    private ArrayList<Persona> personasActuales;
    private Boolean tieneBandaTradicional;
    private Marca marcaQueVende;

    public Carpa(Integer capacidadPermitida, Boolean tieneBandaTradicional, Marca marcaQueVende) {
        this.capacidadPermitida = capacidadPermitida;
        this.tieneBandaTradicional = tieneBandaTradicional;
        this.marcaQueVende = marcaQueVende;
        this.personasActuales = new ArrayList<Persona>();
        marcaQueVende.setCarpa(this);
    }

    public Integer getCapacidadPermitida() {
        return capacidadPermitida;
    }

    public void setCapacidadPermitida(Integer capacidadPermitida) {
        this.capacidadPermitida = capacidadPermitida;
    }

    public Integer getCantidadDePersonas() {
        return cantidadDePersonas;
    }

    public void setCantidadDePersonas(Integer cantidadDePersonas) {
        this.cantidadDePersonas = cantidadDePersonas;
    }

    public ArrayList<Persona> getPersonasActuales() {
        return personasActuales;
    }

    public void setPersonasActuales(ArrayList<Persona> personasActuales) {
        this.personasActuales = personasActuales;
    }

    public Boolean getTieneBandaTradicional() {
        return tieneBandaTradicional;
    }

    public void setTieneBandaTradicional(Boolean tieneBandaTradicional) {
        this.tieneBandaTradicional = tieneBandaTradicional;
    }

    public Marca getMarcaQueVende() {
        return marcaQueVende;
    }

    public void setMarcaQueVende(Marca marcaQueVende) {
        this.marcaQueVende = marcaQueVende;
    }

    public Boolean dejaEntrar(Persona unaPersona) {
        return (!unaPersona.estaEbria() && ((this.cantidadDePersonas + 1) <= this.capacidadPermitida));
    }

    public void dejarEntrar(Persona unaPersona) {
        if (this.dejaEntrar(unaPersona) && unaPersona.quiereEntrar(this)) {
            this.cantidadDePersonas = this.getCantidadDePersonas() + 1;
            this.personasActuales.add(unaPersona);
        }
    }

    public Integer ebriosEmpedernidos() {
        return this.personasActuales.stream().filter(ebrio ->
                ebrio.susJarrasSonDeMasDeUnLitro()).collect(Collectors.toList()).size();
    }

}
