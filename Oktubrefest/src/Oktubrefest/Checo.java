package Oktubrefest;

import java.util.List;

public class Checo extends Persona {

    public Checo(String nombre, String apellido, Integer peso, Boolean musica, Double aguante) {
        super(nombre, apellido, peso, musica, aguante);
    }

    @Override
    public Boolean leGustaLaMarcaDeCerveza(Marca unaMarca) {
        return unaMarca.graduacion() > 8;
    }

    @Override
    public Boolean esPatriota() {
        return this.jarrasCompradas.stream().anyMatch(j -> j.getCarpa().getMarcaQueVende().getPais().equals("República Checa"));
    }
}
